**Quick links**

  - [wiki] with tutorial to get started quickly
  - [API][API01] documentation to learn about details (version 0.1.1)
  - [API][API02] documentation to learn about details (version 0.2-M1)
  - [Trello board] to get in touch
  - [Twitter] account to stay updated

**Frequently Asked Questions**

[TOC]

# What's new?

**04.08.2014**

We just released first bugfix release for SpecGine 0.1. It should be source
compatible. See [release notes][Notes011] for details.

We recommend trying our first milestone release of SpecGine 0.2. It features
lot of improvements and new features. See [release notes][Notes02M1] for details.

# What is SpecGine?

SpecGine is a cross-platform game engine made by [SpecDevs]. SpecGine is
written in Scala and is utilizing [LibGDX] to support multiple platforms,
including:

  - Windows
  - Linux
  - MacOS X
  - Android
  - iOS

SpecGine was created as an in-house solution, but we decided to release some
parts of our technology stack to wider audience. Engine is currently in alpha
stage and lots of functionality is still missing, but parts that are already
present were carefully selected, and should be relatively stable.

All released code and documentation is licensed under 2-clause BSD license.
See [LICENSE] file for full text.

# What are most important SpecGine features?

At hearth of SpecGine is advanced state management system. It features:

  - States grouped into tree-structure hierarchy.
  - State stack and transitions between states.
  - Automatic and customizable loading screen and progress bar.
  - Transparent handling of persistent configuration, data exchange between
    states and assets using single, simple syntax.
  - Ability to access state just under active one on stack for easy
    state overlays.
  - Automatic state registration for input events when needed with support
    for easy extension for custom state types.
  - Separate constant length simulation steps and variable length rendering
    steps with optional interpolation support.
  - Ability to guess resource types based on file extensions with customizable
    default parameters and extension lists.

This state management system is accompanied by three already implemented
states:

  - Game state which implements static, type-safe and reflection-free entity
    system with ready systems templates to process entities or their pairs.
  - Menu state with easy integration of various menu buttons with handles for
    custom background rendering.
  - Slide-show state to maximally simplify creation of non-interactive linear
    content, like intros, credits or cutscenes.

More features will come soon, but what is already here, paired with excellent
LibGDX framework we depend on, allows you to make even advanced games.

# What are hardware or software requirements to develop using SpecGine?

A relatively modern desktop computer running Java 7 or newer, equipped with
graphics card capable to use OpenGL 2.1 is recommended. To get best results,
you should use latest Java 7 directly from [Oracle].
Computer with Java 6 should also work, but we do not test our system for
compatibility with developing on Java 6. Java version 5 or older are
unsupported.

To develop for Android you will need Android SDK with Android Platform
Tools, Android Build Tools and platform image for Android version you want
to develop for.

Finally, if you want to develop for iOS platform you will need to use
MacOS X and XCode, and acquire developer license from Apple (we would like
to mention, that we are by no means associated with Apple, we do not
resell licenses, nor provide any support to owners of developer license).

In all cases you will need the Simple Build Tool, [SBT]. Currently we
recommend using version 0.12.4 for 0.1 branch and 0.13.5 for 0.2 branch.

**Note**:
There is [known issue](https://code.google.com/p/android/issues/detail?id=62583)
with Android Build Tools version 19. It is fixed in versions 19.0.1 and higher,
so we recommend updating to latest release of Android Build Tools.

# What are hardware or sofrware requirements to play games made with SpecGine?

Requirements depend on platform you deploy to:

 - Any relatively modern desktop computer running Java 6 or newer
   equipped with graphics card supporting OpenGL 2.1.
 - Android devices with operating system in version 2.3.3 or higher and
   OpenGL ES 2.0 support.
 - iPad and iPhone devices, operating system in version 5 or higher and
   OpenGL ES 2.0 support.

# How do I start using it?

**Note**: SpecGine 0.1.1 is compiled against Scala 2.10 and LibGDX 0.9.9.
SpecGine 0.2-M1 is compiled against Scala 2.11 and LibGDX 1.2.0.
You should use matching version of dependencies in your project.

To learn about SpecGine we recommend checking tutorial on our [wiki] and
[API] docs for currently released version. You will also need to get the
SpecGine itself. Here is how you can do it.

## Getting from Central Repository

We publish to Central Repository. Add following lines to your main
project `libraryDependencies` to depend on SpecGine. Notice, that
macros package is only a provided dependency.

```
#!scala
libraryDependencies ++= Seq(
  "com.specdevs" %% "specgine" % "0.1.1",
  "com.specdevs" %% "specgine-macros" % "0.1.1" % "provided"
)
```
or for milestone release:
```
#!scala
libraryDependencies ++= Seq(
  "com.specdevs" %% "specgine" % "0.2-M1",
  "com.specdevs" %% "specgine-macros" % "0.2-M1" % "provided"
)
```

If you are using SBT [android-plugin] and experience problems running Android
application, you can try using following `libraryDependencies`, which
seem to work when above fails.

```
#!scala
libraryDependencies ++= Seq(
  "com.specdevs" %% "specgine" % "0.1.1",
  "com.specdevs" %% "specgine-macros" % "0.1.1" exclude("org.scala-lang", "scala-reflect")
)
```

## Creating locally published version

You can also build SpecGine and publish it into your local repository.

First, you need to get source code by cloning git repository:

```
#!bash
git clone https://bitbucket.org/specdevs/specgine.git
cd specgine
git checkout v0.1.1 # or v0.2-M1
```

Then start `sbt` command (Simple Build Tool, a built tool popular for Scala
projects). We recommend checking [SBT] documentation on how to get and run it
on your platform. When you get working SBT, just issue command:

```
#!scala
publish-local
```

It will compile, package and publish SpecGine into your local repository,
where projects on your machine will be able to find it. To depend on
locally published SpecGine version, use same `libraryDependencies`
as for Central Repository.

## Getting JAR files manually

You can also download JAR files for engine and documentation manually and
place them where your compiler can find them. We publish them here on
bitbucket:

  - [specgine_2.10-0.1.1.jar] - main engine code (0.1.1)
  - [specgine-macros_2.10-0.1.1.jar] - macros package (0.1.1)
  - [specgine-all_2.10-0.1.1-javadoc.jar] - compiled documentation for offline viewing (0.1.1)

  - [specgine_2.11-0.2-M1.jar] - main engine code (0.2-M1)
  - [specgine-macros_2.11-0.2-M1.jar] - macros package (0.2-M1)
  - [specgine-all_2.11-0.2-M1-javadoc.jar] - compiled documentation for offline viewing (0.2-M1)

## Do you publish snapshots of future releases?

Currently no, we do not publish snapshots of SpecGine, but it might change
in future. For now, you are free to use procedure described in
**Creating locally published version** above, just checkout current development
branch instead of release tag.

# I found a bug, what should I do?

If you find some bugs, mistakes in the code, on the page or documentation, we
will be more than happy to accept a pull request from you. If you do not know
how to fix the issue, get in touch with us. We recommend using Bugs section
on our public [Trello board]. When we confirm the issue we will create a ticket
for it on our bitbucket issue tracking system.

# I miss a certain feature, what should I do?

Just ask! We also miss lots of features we plan to add in future versions.
There is chance we are already working on similar feature and we can discuss
our needs and ideas for it. It is best to write it on our public [Trello board]
in Ideas section. If we decide to implement it, we will create ticket for it
on our bitbucket issue tracking system.

# I want to help, what can I do?

Of course if you asked yourself one of two earlier questions, and followed
their answers, you were already of great help to us! But what else you can do?

First of all, help us spread the word! If you like our work, do not forget to
tell about it to your friends. Even better if you run technology blog. If you
already made some games or prototypes using SpecGine, we would be happy
to hear about it, and if you agree, to put it on engine web page as
a kind of showcase.

Finally, if you are interested in helping with development, we would like to
ask you to get in touch with us, so we can discuss your contribution. Most
likely we will include your changes to code, but as previously stated,
SpecGine is part of our in-house technology stack, and we do not want it to
diverge too much from original design, unless benefit will be viable or
changes to our own code maintainable.

# How do I find out news about developments in SpecGine?

You can star or follow this repository and our [Trello board], or you can follow
us on our [Twitter] account.

# I do not like contacting by Trello, any other ways to get in touch?

Of course. While we prefer Trello to contact, because it allows us to get
a glimpse at board and know what we have to do today, you can still use
more traditional ways of contacting us. You can send us e-mail at address
contact(at)specdevs.com or directly to team members, using scheme
givenname.familyname(at)specdevs.com. You can also get in touch by Tweeting
to us.

[SpecDevs]: http://specdevs.com/ "SpecDevs group"
[LibGDX]: http://libgdx.badlogicgames.com/ "LibGDX framework"
[Trello board]: https://trello.com/b/eaIixUAi/specgine "SpecGine Trello board"
[Twitter]: https://twitter.com/specdevs "SpecDevs Twitter account"
[wiki]: https://bitbucket.org/specdevs/specgine/wiki/Home "SpecGine wiki"
[API01]: http://specdevs.bitbucket.org/specgine/api/0.1.1/ "SpecGine API 0.1.1"
[API02]: http://specdevs.bitbucket.org/specgine/api/0.2-M1/ "SpecGine API 0.2-M1"
[SBT]: http://www.scala-sbt.org/ "Simple Build Tool web page"
[specgine_2.10-0.1.1.jar]: https://bitbucket.org/specdevs/specgine/downloads/specgine_2.10-0.1.1.jar "Main engine code (0.1.1)"
[specgine-macros_2.10-0.1.1.jar]: https://bitbucket.org/specdevs/specgine/downloads/specgine-macros_2.10-0.1.1.jar "Macros package (0.1.1)"
[specgine-all_2.10-0.1.1-javadoc.jar]: https://bitbucket.org/specdevs/specgine/downloads/specgine-all_2.10-0.1.1-javadoc.jar "Compiled documentation for offline viewing (0.1.1)"
[specgine_2.11-0.2-M1.jar]: https://bitbucket.org/specdevs/specgine/downloads/specgine_2.11-0.2-M1.jar "Main engine code (0.2-M1)"
[specgine-macros_2.11-0.2-M1.jar]: https://bitbucket.org/specdevs/specgine/downloads/specgine-macros_2.11-0.2-M1.jar "Macros package (0.2-M1)"
[specgine-all_2.11-0.2-M1-javadoc.jar]: https://bitbucket.org/specdevs/specgine/downloads/specgine-all_2.11-0.2-M1-javadoc.jar "Compiled documentation for offline viewing (0.2-M1)"
[Oracle]: http://java.com/ "Java Home Page"
[android-plugin]: https://github.com/jberkel/android-plugin "SBT Android Plugin"
[LICENSE]: https://bitbucket.org/specdevs/specgine/src/HEAD/LICENSE?at=master "2-clause BSD license"
[Notes011]: https://bitbucket.org/specdevs/specgine/src/HEAD/notes/0.1.1.markdown "Release notes of 0.1.1"
[Notes02M1]: https://bitbucket.org/specdevs/specgine/src/v0.2-M1/notes/0.2.markdown "Release notes of 0.2-M1"
