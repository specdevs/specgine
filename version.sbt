version := "0.2-SNAPSHOT"

scalaVersion := "2.11.5"

libgdxVersion := "1.5.2"

sprayJsonVersion := "1.3.1"

scalatestVersion := "2.2.1"
