package com.specdevs.specgine.macros.states

import com.specdevs.specgine.states._

import scala.annotation.Annotation

/** Root of all possible rule annotations used by `withManager` macro. */
sealed trait ComponentsRule extends Annotation

/** Specifies that [[com.specdevs.specgine.states.Component `Component`]] should be avoided.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is associated with given [[com.specdevs.specgine.states.Component `Component`]].
 *
 * @constructor Creates new rule for unwanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class No[A <: Component]
  extends ComponentsRule

/** Specifies that two [[com.specdevs.specgine.states.Component `Component`]]s should be avoided.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is associated with any of two given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for unwanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class NoneOf2[A <: Component, B <: Component]
  extends ComponentsRule

/** Specifies that three [[com.specdevs.specgine.states.Component `Component`]]s should be avoided.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is associated with any of three given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for unwanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class NoneOf3[A <: Component, B <: Component, C <: Component]
  extends ComponentsRule

/** Specifies that four [[com.specdevs.specgine.states.Component `Component`]]s should be avoided.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is associated with any of four given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for unwanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class NoneOf4[A <: Component, B <: Component, C <: Component, D <: Component]
  extends ComponentsRule

/** Specifies that five [[com.specdevs.specgine.states.Component `Component`]]s should be avoided.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is associated with any of five given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for unwanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam E Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class NoneOf5[A <: Component, B <: Component, C <: Component, D <: Component, E <: Component]
  extends ComponentsRule

/** Specifies that six [[com.specdevs.specgine.states.Component `Component`]]s should be avoided.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is associated with any of six given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for unwanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam E Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam F Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class NoneOf6[A <: Component, B <: Component, C <: Component, D <: Component, E <: Component, F <: Component]
  extends ComponentsRule

/** Specifies that seven [[com.specdevs.specgine.states.Component `Component`]]s should be avoided.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is associated with any of seven given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for unwanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam E Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam F Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam G Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class NoneOf7[A <: Component, B <: Component, C <: Component, D <: Component, E <: Component, F <: Component,
  G <: Component] extends ComponentsRule

/** Specifies that eight [[com.specdevs.specgine.states.Component `Component`]]s should be avoided.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is associated with any of eight given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for unwanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam E Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam F Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam G Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam H Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class NoneOf8[A <: Component, B <: Component, C <: Component, D <: Component, E <: Component, F <: Component,
  G <: Component, H <: Component] extends ComponentsRule

/** Specifies that nine [[com.specdevs.specgine.states.Component `Component`]]s should be avoided.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is associated with any of nine given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for unwanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam E Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam F Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam G Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam H Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam I Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class NoneOf9[A <: Component, B <: Component, C <: Component, D <: Component, E <: Component, F <: Component,
  G <: Component, H <: Component, I <: Component] extends ComponentsRule

/** Specifies that the [[com.specdevs.specgine.states.Component `Component`]] is needed.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is not associated with given
 * [[com.specdevs.specgine.states.Component `Component`]].
 *
 * @constructor Creates new rule for wanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class The[A <: Component]
  extends ComponentsRule

/** Specifies that at least one of two [[com.specdevs.specgine.states.Component `Component`]]s is needed.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is not associated with at least one of two given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for wanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class OneOf2[A <: Component, B <: Component]
  extends ComponentsRule

/** Specifies that at least one of three [[com.specdevs.specgine.states.Component `Component`]]s is needed.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is not associated with at least one of three given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for wanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class OneOf3[A <: Component, B <: Component, C <: Component]
  extends ComponentsRule

/** Specifies that at least one of four [[com.specdevs.specgine.states.Component `Component`]]s is needed.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is not associated with at least one of four given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for wanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class OneOf4[A <: Component, B <: Component, C <: Component, D <: Component]
  extends ComponentsRule

/** Specifies that at least one of five [[com.specdevs.specgine.states.Component `Component`]]s is needed.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is not associated with at least one of five given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for wanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam E Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class OneOf5[A <: Component, B <: Component, C <: Component, D <: Component, E <: Component]
  extends ComponentsRule

/** Specifies that at least one of six [[com.specdevs.specgine.states.Component `Component`]]s is needed.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is not associated with at least one of six given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for wanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam E Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam F Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class OneOf6[A <: Component, B <: Component, C <: Component, D <: Component, E <: Component, F <: Component]
  extends ComponentsRule

/** Specifies that at least one of seven [[com.specdevs.specgine.states.Component `Component`]]s is needed.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is not associated with at least one of seven given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for wanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam E Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam F Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam G Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class OneOf7[A <: Component, B <: Component, C <: Component, D <: Component, E <: Component, F <: Component,
  G <: Component] extends ComponentsRule

/** Specifies that at least one of eight [[com.specdevs.specgine.states.Component `Component`]]s is needed.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is not associated with at least one of eight given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for wanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam E Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam F Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam G Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam H Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class OneOf8[A <: Component, B <: Component, C <: Component, D <: Component, E <: Component, F <: Component,
  G <: Component, H <: Component] extends ComponentsRule

/** Specifies that at least one of nine [[com.specdevs.specgine.states.Component `Component`]]s is needed.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is not associated with at least one of nine given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for wanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam E Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam F Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam G Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam H Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam I Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class OneOf9[A <: Component, B <: Component, C <: Component, D <: Component, E <: Component, F <: Component,
  G <: Component, H <: Component, I <: Component] extends ComponentsRule

/** Specifies that two [[com.specdevs.specgine.states.Component `Component`]]s are needed.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is not associated with all of two given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for wanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class AllOf2[A <: Component, B <: Component]
  extends ComponentsRule

/** Specifies that three [[com.specdevs.specgine.states.Component `Component`]]s are needed.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is not associated with all of three given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for wanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class AllOf3[A <: Component, B <: Component, C <: Component]
  extends ComponentsRule

/** Specifies that four [[com.specdevs.specgine.states.Component `Component`]]s are needed.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is not associated with all of four given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for wanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class AllOf4[A <: Component, B <: Component, C <: Component, D <: Component]
  extends ComponentsRule

/** Specifies that five [[com.specdevs.specgine.states.Component `Component`]]s are needed.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is not associated with all of five given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for wanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam E Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class AllOf5[A <: Component, B <: Component, C <: Component, D <: Component, E <: Component]
  extends ComponentsRule

/** Specifies that six [[com.specdevs.specgine.states.Component `Component`]]s are needed.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is not associated with all of six given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for wanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam E Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam F Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class AllOf6[A <: Component, B <: Component, C <: Component, D <: Component, E <: Component, F <: Component]
  extends ComponentsRule

/** Specifies that seven [[com.specdevs.specgine.states.Component `Component`]]s are needed.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is not associated with all of seven given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for wanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam E Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam F Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam G Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class AllOf7[A <: Component, B <: Component, C <: Component, D <: Component, E <: Component, F <: Component,
  G <: Component] extends ComponentsRule

/** Specifies that eight [[com.specdevs.specgine.states.Component `Component`]]s are needed.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is not associated with all of eight given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for wanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam E Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam F Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam G Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam H Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class AllOf8[A <: Component, B <: Component, C <: Component, D <: Component, E <: Component, F <: Component,
  G <: Component, H <: Component] extends ComponentsRule

/** Specifies that nine [[com.specdevs.specgine.states.Component `Component`]]s are needed.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will not accept [[com.specdevs.specgine.states.Entity `Entity`]]
 * if it is not associated with all of nine given
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * @constructor Creates new rule for wanted types of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam A Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam B Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam D Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam E Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam F Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam G Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam H Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam I Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class AllOf9[A <: Component, B <: Component, C <: Component, D <: Component, E <: Component, F <: Component,
  G <: Component, H <: Component, I <: Component] extends ComponentsRule
