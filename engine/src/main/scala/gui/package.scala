package com.specdevs.specgine

/** Package providing GUI functionality in SpecGine.
 *
 * Currently there is only LibGDX implementation of GUI, residing in
 * [[com.specdevs.specgine.gui.gdx `gdx`]] subpackage.
 */
package object gui
