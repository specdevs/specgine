package com.specdevs.specgine

/** Package providing assets handling in SpecGine.
 *
 * Classes in this package are responsible for assets handling. All assets in
 * system should be subtypes of [[com.specdevs.specgine.assets.Asset `Asset`]]
 * and loaded with asset manager interface in
 * [[com.specdevs.specgine.assets.AbstractAssetManager `AbstractAssetManager`]].
 *
 * Functionality to load and unload resources is present in
 * [[com.specdevs.specgine.assets.AssetManagerUser `AssetManagerUser`]],
 * that can be mixed-in into other classes. It is important, that classes
 * mixing it in, call its `disposeAssets` method.
 *
 * Assets types are determined by
 * [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]], each
 * of its subclasses provide one type of resources. Currently all assets are
 * framework dependent, thus only one one asset descriptor is provided by
 * [[com.specdevs.specgine.assets]] package:
 * [[com.specdevs.specgine.assets.GuessAsset `GuessAsset`]], used to determine
 * asset type using its file extension.
 */
package object assets
