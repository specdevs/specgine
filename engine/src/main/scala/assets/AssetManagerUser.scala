package com.specdevs.specgine.assets

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.{global => globalExecutionContext}

/** Trait mixed-in into classes that use
 * [[com.specdevs.specgine.assets.AbstractAssetManager `AbstractAssetManager`]].
 *
 * Assets loaded trough `AssetManagerUser` are automatically managed, and will
 * be unloaded or removed when no longer needed. This is recommended way to
 * use [[com.specdevs.specgine.assets.Asset `Asset`]]s in SpecGine.
 *
 * @note You should not mix loading or registering
 *       [[com.specdevs.specgine.assets.Asset `Asset`]]s directly trough
 *       [[com.specdevs.specgine.assets.AbstractAssetManager `AbstractAssetManager`]]
 *       and trough `AssetManagerUser`, unless you know exactly what you are doing.
 */
trait AssetManagerUser {
  private var loaded: List[String] = Nil

  private var added: List[String] = Nil

  private var theAssetManager: Option[AbstractAssetManager] = None

  /** Implicit execution context for working with future assets. */
  implicit val executionContext = globalExecutionContext

  /** Getter for instance of
   * [[com.specdevs.specgine.assets.AbstractAssetManager `AbstractAssetManager`]].
   *
   * @return Configuration manager instance.
   */
  implicit def assetManager: AbstractAssetManager = {
    if (theAssetManager.isEmpty) {
      throw new IllegalStateException("You haven't set assetManager.")
    } else {
      theAssetManager.get
    }
  }

  /** Setter for instance of
   * [[com.specdevs.specgine.assets.AbstractAssetManager `AbstractAssetManager`]].
   *
   * @param manager Configuration manager instance.
   */
  def assetManager_=(manager: AbstractAssetManager): Unit = {
    theAssetManager = Some(manager)
  }

  /** Method checking if asset is loaded.
   *
   * @param name Name of asset to check.
   * @return `true` if asset is loaded.
   */
  def isAssetLoaded(name: String): Boolean = {
    assetManager.isLoaded(name)
  }

  /** Method to request loading of asset.
   *
   * @param name Name of asset to load.
   * @param descriptor [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]]
   *        associated with type of asset to load.
   * @note Assets are reference-counted, thus even if multiple copies are requested,
   *       only one instance of file will be loaded to memory.
   * @return Future asset, completed when loading is finished.
   */
  def loadAsset(name: String, descriptor: AssetDescriptor = GuessAsset): Future[Asset] = {
    val future = assetManager.load(name, descriptor)
    loaded = name :: loaded
    future
  }

  /** Method used to get asset from manager.
   *
   * @param name Name of asset to get.
   * @return Value associated with key, or `None` if not found.
   */
  def getAsset(name: String): Option[Asset] = {
    assetManager.get(name)
  }

  /** Method used to locally store asset in manager.
   *
   * @param name Name of asset to set.
   * @param value Asset to set.
   * @note Asset will be automatically disposed when class you set it in is disposed.
   */
  def setAsset(name: String, value: Asset): Unit = {
    assetManager.set(name, value)
    added = name :: added
  }

  /** Method used to get future asset from manager.
   *
   * @param name Name of asset to get.
   * @return Future value associated with key.
   */
  def getFutureAsset(name: String): Future[Asset] = {
    assetManager.getFuture(name)
  }

  /** Method used to locally store future asset in manager.
   *
   * @param name Name of asset to set.
   * @param value Future asset to set.
   * @note Asset will be automatically disposed when class you set it in is disposed.
   */
  def setFutureAsset(name: String, value: Future[Asset]): Unit = {
    assetManager.setFuture(name, value)
    added = name :: added
  }

  /** Method adding new future task to loading process.
   *
   * Tasks added with this method will be executed as part of loading
   * process, after all assets requested with `load` method are already
   * loaded. Results of this process will not be stored inside asset manager.
   * Future tasks are performed in background, inside default future execution
   * context.
   *
   * @param task Task to add to loading process.
   */
  def addTask[T](task: Future[T]): Unit = {
    assetManager.addTask(task)
  }

  /** Method adding new task to loading process.
   *
   * Tasks added with this method will be executed as part of loading
   * process, after all assets requested with `load` method are already
   * loaded and background tasks processed. Results of this process will not
   * be stored inside asset manager. Those tasks will block main thread, so
   * they should not take long time to finish.
   *
   * @param task Task to add to loading process.
   */
  def addTask(task: => Unit): Unit = {
    assetManager.addTask(task)
  }

  /** Method unloading or removing all assets added trough `AssetManagerUser`. */
  def disposeAssets(): Unit = {
    added foreach (name => {
      assetManager.get(name) foreach (_.dispose())
      assetManager.removeLocal(name)
    })
    added = Nil
    loaded foreach assetManager.unload
    loaded = Nil
  }
}
