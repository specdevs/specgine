package com.specdevs.specgine.assets

import com.specdevs.specgine.core.{Getter,GetterFuture,Setter,SetterFuture}

import scala.concurrent.Future

/** Trait representing all implementations of asset managers.
 *
 * Implementation should allow to `load` and `unload` assets, and check if
 * asset `isLoaded`. When asset is requested to be loaded, it should report
 * that it `needsLoading`. When method `update` is called, it should load some
 * part of resources and return nearly immediately to not block main thread.
 * `getPercentage` method can be used to check if loading is done.
 *
 * Asset manager can also keep assets loaded by user, registered by calling
 * `set`. Both loaded and registered resources can be obtained using `get`.
 * To remove registered asset method `removeLocal` is required.
 */
trait AbstractAssetManager extends Getter[Asset] with Setter[Asset] with GetterFuture[Asset] with SetterFuture[Asset] {
  /** Method checking if asset is loaded.
   *
   * @param name Name of asset to check.
   * @return `true` if asset is loaded.
   */
  def isLoaded(name: String): Boolean

  /** Method to request loading of asset.
   *
   * @param name Name of asset to load.
   * @param descriptor [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]]
   *        associated with type of asset to load.
   * @note Assets are reference-counted, thus even if multiple copies are requested,
   *       only one instance of file will be loaded to memory.
   * @return Future asset, completed when loading is finished.
   */
  def load(name: String, descriptor: AssetDescriptor = GuessAsset): Future[Asset]

  /** Method to request unloading of asset.
   *
   * @param name Name of asset to unload.
   * @note Unloading should not be called more times than loading of asset.
   */
  def unload(name: String): Unit

  /** Method removing locally added resource.
   *
   * Removes assets added with `set` method.
   *
   * @param name Name of asset to remove.
   */
  def removeLocal(name: String): Unit

  /** Method checking if loading is in progress.
   *
   * @return `true` if some assets were requested to load, but loading is not finished yet.
   */
  def needsLoading: Boolean

  /** Advance loading in async manner.
   *
   * @return `true` if all loading is finished.
   */
  def update(): Boolean

  /** Get percentage of loading.
   *
   * @return A number in range (0..1).
   */
  def getPercentage: Float

  /** Method adding new future task to loading process.
   *
   * Tasks added with this method will be executed as part of loading
   * process, after all assets requested with `load` method are already
   * loaded. Results of this process will not be stored inside asset manager.
   * Future tasks are performed in background, inside default future execution
   * context.
   *
   * @param task Task to add to loading process.
   */
  def addTask[T](task: Future[T]): Unit

  /** Method adding new task to loading process.
   *
   * Tasks added with this method will be executed as part of loading
   * process, after all assets requested with `load` method are already
   * loaded and background tasks processed. Results of this process will not
   * be stored inside asset manager. Those tasks will block main thread, so
   * they should not take long time to finish.
   *
   * @param task Task to add to loading process.
   */
  def addTask(task: => Unit): Unit

  /** Method registering new [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]].
   *
   * Only registered descriptors are considered for guessing of asset types.
   * New descriptors with custom options or extensions can be added, to change
   * default handling of [[com.specdevs.specgine.assets.Asset `Asset`]]s.
   * [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]]s added
   * first are checked last.
   *
   * @param added [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]]s to register.
   */
  def registerDescriptor(added: AssetDescriptor*): Unit

  /** Dispose asset manager releasing all resources. */
  def dispose(): Unit
}
