package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.graphics.Pixmap.{Format => GdxFormat}
import com.badlogic.gdx.graphics.{Texture => GdxTexture}
import com.badlogic.gdx.graphics.{TextureData => GdxTextureData}
import com.badlogic.gdx.graphics.Texture.{TextureFilter => GdxTextureFilter}
import com.badlogic.gdx.graphics.Texture.{TextureWrap => GdxTextureWrap}
import com.badlogic.gdx.assets.loaders.TextureLoader.{TextureParameter => GdxTextureParameter}

/** [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]] for
 * [[com.specdevs.specgine.assets.gdx.TextureAsset `TextureAsset`]].
 *
 * @constructor New instance of `TextureDescriptor`.
 * @param format Format of the final texture.
 * @param genMipMaps Whether to generate mipmaps.
 * @param texture Texture to put data in.
 * @param textureData Texture data for textures created on the fly.
 * @param minFilter Filter used for zooming font texture.
 * @param magFilter Filter used for zooming font texture.
 * @param wrapU How to wrap texture in U coordinate.
 * @param wrapV How to wrap texture in V coordinate.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/Pixmap.Format.html ''
 *      LibGDX API'']], for `Format` description.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/Texture.html ''LibGDX API'']],
 *      for `Texture` description.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/TextureData.html ''
 *      LibGDX API'']], for `TextureData` description.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/Texture.TextureFilter.html ''
 *      LibGDX API'']], for `TextureFilter` description.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/Texture.TextureWrap.html ''
 *      LibGDX API'']], for `TextureWrap` description.
 */
case class TextureDescriptor(
    format: Option[GdxFormat] = None,
    genMipMaps: Boolean = false,
    texture: Option[GdxTexture] = None,
    textureData: Option[GdxTextureData] = None,
    minFilter: GdxTextureFilter = GdxTextureFilter.Nearest,
    magFilter: GdxTextureFilter = GdxTextureFilter.Nearest,
    wrapU: GdxTextureWrap = GdxTextureWrap.ClampToEdge,
    wrapV: GdxTextureWrap = GdxTextureWrap.ClampToEdge) extends AssetDescriptorGdx {
  type T = GdxTexture

  type P = GdxTextureParameter

  val extensions: List[String] = List("png", "jpg", "jpeg", "etc1", "cim")

  val assetClass: Class[GdxTexture] = classOf[GdxTexture]

  def loaderParameters: GdxTextureParameter = {
    val params = new GdxTextureParameter()
    if (format.isDefined) params.format = format.get
    params.genMipMaps = genMipMaps
    if (texture.isDefined) params.texture = texture.get
    if (textureData.isDefined) params.textureData = textureData.get
    params.minFilter = minFilter
    params.magFilter = magFilter
    params.wrapU = wrapU
    params.wrapV = wrapV
    params
  }

  def packAsset(asset: Any): Option[Asset] = asset match {
    case (gdxAsset: GdxTexture) => Some(TextureAsset(gdxAsset))
    case _ => None
  }
}
