package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.assets.loaders.ModelLoader.{ModelParameters => GdxModelParameters}
import com.badlogic.gdx.graphics.g3d.{Model => GdxModel}

/** [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]] for
 * [[com.specdevs.specgine.assets.gdx.ModelAsset `ModelAsset`]] in G3d format.
 *
 * @constructor New instance of `G3dModelDescriptor`.
 */
case class G3dModelDescriptor() extends AssetDescriptorGdx {
  type T = GdxModel

  type P = GdxModelParameters

  val extensions: List[String] = List("g3dj", "g3db")

  val assetClass: Class[GdxModel] = classOf[GdxModel]

  def loaderParameters: GdxModelParameters = new GdxModelParameters

  def packAsset(asset: Any): Option[Asset] = asset match {
    case (gdxAsset: GdxModel) => Some(ModelAsset(gdxAsset))
    case _ => None
  }
}
