package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.assets.{AssetLoaderParameters => GdxAssetLoaderParameters}
import com.badlogic.gdx.assets.AssetLoaderParameters.{LoadedCallback => GdxLoadedCallback}
import com.badlogic.gdx.assets.{AssetManager => GdxAssetManager}
import com.badlogic.gdx.assets.loaders.{AssetLoader => GdxAssetLoader}

import scala.concurrent.Promise
import scala.util.Try

/** [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]] for LibGDX assets.
 *
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/assets/AssetLoaderParameters.html ''
 *      LibGDX API'']], for `AssetLoaderParameters` description.
 */
trait AssetDescriptorGdx extends AssetDescriptor {
  /** Type representing LibGDX asset. */
  type T

  /** Type representing loader's parameters for LibGDX asset. */
  type P <: GdxAssetLoaderParameters[T]

  /** Class of asset. */
  val assetClass: Class[T]

  /** Method returning LibGDX loader parameters.
   *
   * @return Instance of `AssetLoaderParameters[T]` for LibGDX.
   */
  def loaderParameters: P

  /** Method returning callback completing promise when loading finishes.
   *
   * @param promise Promise of future asset.
   * @return Callback completing promise with just loaded asset.
   */
  def promiseCallback(promise: Promise[Asset]): GdxLoadedCallback = {
    new GdxLoadedCallback {
      def finishedLoading(manager: GdxAssetManager, name: String, what: Class[_]): Unit = {
        promise.complete(Try(packAsset(manager.get(name, what)).get))
        ()
      }
    }
  }

  /** Method packing LibGDX asset into SpecGine asset.
   *
   * @param asset LibGDX asset.
   * @return SpecGine asset or `None` if asset types are incompatible.
   */
  def packAsset(asset: Any): Option[Asset]

  /** Method returning optional asset loader.
   *
   * @return AssetLoader or `None` if asset does not have optional loader.
   */
  def getLoader: Option[GdxAssetLoader[T, P]] = None
}
