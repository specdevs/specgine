package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.graphics.g3d.{Model => GdxModel}

/** `Model` implementation for [[com.specdevs.specgine.assets.Asset `Asset`]].
 *
 * @param self LibGDX `Model` value.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/g3d/Model.html ''
 *      LibGDX API'']], for `Model` description.
 */
case class ModelAsset(val self: GdxModel) extends AnyVal with Asset {
  def dispose(): Unit = {
    self.dispose()
  }
}
