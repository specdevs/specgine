package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.scenes.scene2d.ui.{Skin => GdxSkin}
import com.badlogic.gdx.assets.loaders.SkinLoader.{SkinParameter => GdxSkinParameter}

/** [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]] for
 * [[com.specdevs.specgine.assets.gdx.SkinAsset `SkinAsset`]].
 *
 * @constructor New instance of `SkinDescriptor`.
 * @param textureAtlasPath Path to texture atlas, `None` if default.
 */
case class SkinDescriptor(textureAtlasPath: Option[String] = None) extends AssetDescriptorGdx {
  type T = GdxSkin

  type P = GdxSkinParameter

  val extensions: List[String] = List("json")

  val assetClass: Class[GdxSkin] = classOf[GdxSkin]

  def loaderParameters: GdxSkinParameter = textureAtlasPath match {
    case Some(path) => new GdxSkinParameter(path)
    case None => new GdxSkinParameter()
  }

  def packAsset(asset: Any): Option[Asset] = asset match {
    case (gdxAsset: GdxSkin) => Some(SkinAsset(gdxAsset))
    case _ => None
  }
}
