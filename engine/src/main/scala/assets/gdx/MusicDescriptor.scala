package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.assets.loaders.MusicLoader.{MusicParameter => GdxMusicParameter}
import com.badlogic.gdx.audio.{Music => GdxMusic}

/** [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]] for
 * [[com.specdevs.specgine.assets.gdx.MusicAsset `MusicAsset`]].
 *
 * @constructor New instance of `MusicDescriptor`.
 */
case class MusicDescriptor() extends AssetDescriptorGdx {
  type T = GdxMusic

  type P = GdxMusicParameter

  val extensions: List[String] = List("mp3", "ogg")

  val assetClass: Class[GdxMusic] = classOf[GdxMusic]

  def loaderParameters: GdxMusicParameter = new GdxMusicParameter

  def packAsset(asset: Any): Option[Asset] = asset match {
    case (gdxAsset: GdxMusic) => Some(MusicAsset(gdxAsset))
    case _ => None
  }
}
