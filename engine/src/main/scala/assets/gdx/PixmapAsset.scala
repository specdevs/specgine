package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.graphics.{Pixmap => GdxPixmap}

/** `Pixmap` implementation for [[com.specdevs.specgine.assets.Asset `Asset`]].
 *
 * @param self LibGDX `Pixmap` value.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/Pixmap.html ''LibGDX API'']],
 *      for `Pixmap` description.
 */
case class PixmapAsset(val self: GdxPixmap) extends AnyVal with Asset {
  def dispose(): Unit = {
    self.dispose()
  }
}
