package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.assets.loaders.{AssetLoader => GdxAssetLoader}

/** [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]] for
 * [[com.specdevs.specgine.assets.gdx.StringListAsset `StringListAsset`]].
 *
 * @constructor New instance of `StringListDescriptor`.
 * @param encoding Asset will be read into a string using the specified encoding.
 */
case class StringListDescriptor(encoding: Option[String] = None) extends AssetDescriptorGdx {
  type T = String

  type P = StringListLoader.StringListLoaderParameters

  val extensions: List[String] = List("txt")

  val assetClass: Class[String] = classOf[String]

  def loaderParameters: StringListLoader.StringListLoaderParameters = {
    StringListLoader.StringListLoaderParameters(encoding)
  }

  override def getLoader: Option[GdxAssetLoader[String, StringListLoader.StringListLoaderParameters]] = {
    Some(new StringListLoader)
  }

  def packAsset(asset: Any): Option[Asset] = asset match {
    case (gdxAsset: String) => Some(StringListAsset(gdxAsset.split("""\r\n?|\n""").toList))
    case _ => None
  }
}
