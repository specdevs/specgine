package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.assets.loaders.{AssetLoader => GdxAssetLoader}

import spray.json.JsValue

/** [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]] for
 * [[com.specdevs.specgine.assets.gdx.JsonAsset `JsonAsset`]].
 *
 * @constructor New instance of `JsonAssetDescriptor`.
 */
case class JsonAssetDescriptor() extends AssetDescriptorGdx {
  type T = JsValue

  type P = JsonLoader.JsonLoaderParameters

  val extensions: List[String] = List("json")

  val assetClass: Class[JsValue] = classOf[JsValue]

  def loaderParameters: JsonLoader.JsonLoaderParameters = {
    new JsonLoader.JsonLoaderParameters
  }

  override def getLoader: Option[GdxAssetLoader[JsValue, JsonLoader.JsonLoaderParameters]] = {
    Some(new JsonLoader)
  }

  def packAsset(asset: Any): Option[Asset] = asset match {
    case (gdxAsset: JsValue) => Some(JsonAsset(gdxAsset))
    case _ => None
  }
}
