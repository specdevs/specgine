package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.assets.{AssetManager => GdxAssetManager}
import com.badlogic.gdx.assets.{AssetLoaderParameters => GdxAssetLoaderParameters}
import com.badlogic.gdx.assets.loaders.{AssetLoader => GdxAssetLoader}

import scala.collection.mutable.{HashMap,HashSet,Queue}
import scala.concurrent.{Future,Promise}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure,Success}

/** Asset manager class based on LibGDX.
 *
 * All asset types handled by LibGDX are automatically handled and registered
 * in `AssetManager`. If you want to add custom resource types, you have to:
 *   - Add [[com.specdevs.specgine.assets.gdx.AssetDescriptorGdx `AssetDescriptorGdx`]]
 *     for new asset type.
 *   - Prepare LibGDX `AssetLoader` and return it's instance from
 *     [[com.specdevs.specgine.assets.gdx.AssetDescriptorGdx `AssetDescriptorGdx's`]] `getLoader` method.
 *   - Register descriptor using `registerDescriptor` if you want to allow
 *     SpecGine `AssetManager` to guess asset type based on its extension.
 *     Firstly added `AssetLoader` for a given asset class is registered as default loader. Any other
 *     `AssetLoader` for the same asset class will be registered only for specified extensions.
 *   - Create implicit object that subtypes
 *     [[com.specdevs.specgine.core.Provider `Provider`]] and make it visible
 *     if you want to use [[com.specdevs.specgine.core.GetSetUser `GetSetUser`]]
 *     and its universal `get` and `set` family of methods.
 *
 * @constructor Creates instance of [[com.specdevs.specgine.assets.AbstractAssetManager `AbstractAssetManager`]]
 *              that wraps LibGDX `AssetManager`.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/assets/loaders/AssetLoader.html ''
 *      LibGDX API'']], for `AssetLoader` description.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/assets/AssetLoaderParameters.html ''
 *      LibGDX API'']], for `AssetLoaderParameters` description.
 */
class AssetManager extends AbstractAssetManager {
  private lazy val assetManager = new GdxAssetManager(new AssetResolver)

  private val loaded = new HashMap[String, (AssetDescriptorGdx, Future[Asset], Promise[Asset])]

  private val assetsClasses = new HashSet[Class[_]]

  private var loadingCount = 0

  private var tasksCount = 0

  private val pendingAssets = new HashMap[String, Future[Asset]]

  private val futureTasks = new Queue[Future[Any]]

  private val finalizingTasks = new Queue[() => Unit]

  private val local = new HashMap[String, Asset]

  private var descriptors: List[AssetDescriptor] = Nil

  registerDescriptor(getDefaultDescriptors: _*)

  private def identifyAsset(name: String): AssetDescriptorGdx = {
    def canBe(a: AssetDescriptor): Boolean = {
      a.extensions exists (extension => name endsWith extension)
    }
    def check(as: List[AssetDescriptor]): AssetDescriptorGdx = as match {
      case (a: AssetDescriptorGdx)::_ if canBe(a) => a
      case _:: rest => check(rest)
      case Nil => throw new IllegalArgumentException(s"Cannot guess asset type from extension for file $name.")
    }
    check(descriptors)
  }

  def isLoaded(name: String): Boolean = {
    assetManager.isLoaded(name)
  }

  private def loadGdx(name: String, descriptor: AssetDescriptorGdx): Future[Asset] = {
    val promise = Promise[Asset]()
    if (isLoaded(name)) {
      descriptor.promiseCallback(promise).finishedLoading(assetManager, name, descriptor.assetClass)
    } else {
      val params = descriptor.loaderParameters
      params.loadedCallback = descriptor.promiseCallback(promise)
      assetManager.load(name, descriptor.assetClass, params)
    }
    val future = promise.future
    if (!loaded.isDefinedAt(name)) {
      loadingCount += 1
    }
    loaded += name -> ((descriptor, future, promise))
    future
  }

  def load(name: String, descriptor: AssetDescriptor = GuessAsset): Future[Asset] = {
    if (loaded.isDefinedAt(name)) {
      val (_, future, _) = loaded(name)
      future
    } else {
      val descriptorGdx = descriptor match {
        case descriptorGdx: AssetDescriptorGdx => descriptorGdx
        case _ => identifyAsset(name)
      }
      loadGdx(name, descriptorGdx)
    }
  }

  def unload(name: String): Unit = {
    if (isLoaded(name)) {
      val dependencies = Option(assetManager.getDependencies(name))
      assetManager.unload(name)
      if (loaded.isDefinedAt(name) && !isLoaded(name)) {
        loaded -= name
        if (needsLoading) {
          loadingCount -= 1
        }
        if (dependencies.nonEmpty) {
          dependencies.get.items foreach { name =>
            if (loaded.isDefinedAt(name) && !isLoaded(name)) {
              loaded -= name
              if (needsLoading) {
                loadingCount -= 1
              }
            }
          }
        }
      }
    }
  }

  def removeLocal(name: String): Unit = {
    if (local.isDefinedAt(name)) {
      local -= name
      ()
    }
  }

  def registerDescriptor(added: AssetDescriptor*): Unit = {
    for (assetDescriptor <- added) {
      assetDescriptor match {
        case (assetDescriptorGdx: AssetDescriptorGdx) if (assetDescriptorGdx.getLoader.isDefined) => {
          val assetClass = assetDescriptorGdx.assetClass
          val loader = assetDescriptorGdx.getLoader.get
          if (assetsClasses.contains(assetClass)) {
            for (extension <- assetDescriptorGdx.extensions) {
              assetManager.setLoader(assetClass, extension, loader)
            }
          } else {
            assetManager.setLoader(assetClass, loader)
            assetsClasses += assetClass
          }
        }
        case _ => ()
      }
    }
    descriptors = added ++: descriptors
  }

  private def getDefaultDescriptors: List[AssetDescriptorGdx] = List(
    BitmapFontDescriptor(),
    G3dModelDescriptor(),
    ObjModelDescriptor(),
    ParticleEffectDescriptor(),
    PixmapDescriptor(),
    SkinDescriptor(),
    TextureAtlasDescriptor(),
    TextureDescriptor(),
    MusicDescriptor(),
    SoundDescriptor(),
    StringListDescriptor()
  )

  def needsLoading: Boolean = getPercentage < 1

  private def updateLoadedAndPending(): Unit = {
    loaded retain {
      (name, values) => {
        val (_, future, promised) = values
        val completed = future.isCompleted
        if (!completed) {
          promised failure (new IllegalStateException)
        }
        completed
      }
    }
    pendingAssets retain {
      (name, future) => {
        val completed = future.isCompleted
        if (completed) {
          future.value match {
            case Some(Success(asset)) => set(name, asset)
            case Some(Failure(e)) => throw e
            case _ => ()
          }
        }
        !completed
      }
    }
    ()
  }

  def update(): Boolean = {
    val gdxLoadingDone = assetManager.update()
    var finished = false
    if (gdxLoadingDone) {
      updateLoadedAndPending()
      if (pendingAssets.isEmpty) {
        futureTasks filter {
          future => !future.isCompleted
        }
        if (futureTasks.isEmpty) {
          if (!finalizingTasks.isEmpty) {
            finalizingTasks.dequeue.apply()
          }
          if (finalizingTasks.isEmpty) {
            loadingCount = 0
            tasksCount = 0
            finished = true
          }
        }
      }
    }
    finished
  }

  def getPercentage: Float = {
    val gdxProgress = assetManager.getProgress
    if (loadingCount + tasksCount == 0) {
      1
    } else {
      val tasksDone = tasksCount - pendingAssets.size - futureTasks.size - finalizingTasks.size
      (gdxProgress * loadingCount + tasksDone) / (loadingCount + tasksCount)
    }
  }

  def dispose(): Unit = {
    assetManager.dispose()
    loaded.clear()
    local.clear()
    pendingAssets.clear()
    futureTasks.clear()
    finalizingTasks.clear()
    loadingCount = 0
    tasksCount = 0
  }

  def get(name: String): Option[Asset] = {
    if (local.isDefinedAt(name)) {
      local.get(name)
    } else if (assetManager.isLoaded(name)) {
      val (descriptor, _, _) = loaded(name)
      descriptor.packAsset(assetManager.get(name, descriptor.assetClass))
    } else {
      None
    }
  }

  def set(name: String, value: Asset): Unit = {
    local += name -> value
    ()
  }

  def getFuture(name: String): Future[Asset] = {
    if (loaded.isDefinedAt(name)) {
      val (_, future, _) = loaded(name)
      future
    } else if (local.isDefinedAt(name)) {
      Future.successful[Asset](local(name))
    } else if (pendingAssets.isDefinedAt(name)) {
      pendingAssets(name)
    } else {
      Future.failed[Asset](new IllegalStateException)
    }
  }

  def setFuture(name: String, value: Future[Asset]): Unit = {
    if (!pendingAssets.isDefinedAt(name)) {
      tasksCount += 1
    }
    pendingAssets += name -> value
    ()
  }

  def addTask[T](task: Future[T]): Unit = {
    tasksCount += 1
    futureTasks.enqueue(task)
  }

  def addTask(task: => Unit): Unit = {
    tasksCount += 1
    finalizingTasks.enqueue(() => task)
  }
}
