package com.specdevs.specgine.assets.gdx

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.loaders.{FileHandleResolver => GdxFileHandleResolver}
import com.badlogic.gdx.files.{FileHandle => GdxFileHandle}

private[gdx] class AssetResolver extends GdxFileHandleResolver {
  override def resolve(filename: String): GdxFileHandle = {
    if (!(filename startsWith ":")) {
      val file = Gdx.files.internal(filename)
      if (file.exists()) {
        file
      } else {
        Gdx.files.classpath(filename)
      }
    } else if (filename startsWith ":internal:") {
      Gdx.files.internal(filename drop 10)
    } else if (filename startsWith ":external:") {
      Gdx.files.external(filename drop 10)
    } else if (filename startsWith ":local:") {
      Gdx.files.local(filename drop 7)
    } else if (filename startsWith ":classpath:") {
      Gdx.files.classpath(filename drop 11)
    } else if (filename startsWith ":absolute:") {
      Gdx.files.absolute(filename drop 10)
    } else {
      throw new IllegalArgumentException(s"Unknown asset location in $filename")
    }
  }
}
