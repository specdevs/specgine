package com.specdevs.specgine.assets

import com.specdevs.specgine.core.Provider

import com.badlogic.gdx.audio.{Music => GdxMusic}
import com.badlogic.gdx.audio.{Sound => GdxSound}
import com.badlogic.gdx.graphics.{Pixmap => GdxPixmap}
import com.badlogic.gdx.graphics.{Texture => GdxTexture}
import com.badlogic.gdx.graphics.g2d.{BitmapFont => GdxBitmapFont}
import com.badlogic.gdx.graphics.g2d.{ParticleEffect => GdxParticleEffect}
import com.badlogic.gdx.graphics.g2d.{TextureAtlas => GdxTextureAtlas}
import com.badlogic.gdx.graphics.g3d.{Model => GdxModel}
import com.badlogic.gdx.scenes.scene2d.ui.{Skin => GdxSkin}

/** Package implementing asset classes using LibGDX framework. */
package object gdx {
  /** Implicit object translating between `BitmapFont` and [[com.specdevs.specgine.assets.Asset `Asset`]].
   *
   * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/g2d/BitmapFont.html ''
   *      LibGDX API]], for `BitmapFont` description.
   */
  implicit object ImplicitBitmapFontAsset extends Provider[Asset,GdxBitmapFont] {
    def unpack(value: Option[Asset]): Option[GdxBitmapFont] = {
      value match {
        case Some(BitmapFontAsset(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: GdxBitmapFont): Asset = BitmapFontAsset(value)
  }

  /** Implicit object translating between `Model` and [[com.specdevs.specgine.assets.Asset `Asset`]].
   *
   * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/g3d/Model.html ''
   *      LibGDX API'']], for `Model` description.
   */
  implicit object ImplicitModelAsset extends Provider[Asset,GdxModel] {
    def unpack(value: Option[Asset]): Option[GdxModel] = {
      value match {
        case Some(ModelAsset(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: GdxModel): Asset = ModelAsset(value)
  }

  /** Implicit object translating between `Music` and [[com.specdevs.specgine.assets.Asset `Asset`]].
   *
   * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/audio/Music.html ''LibGDX API'']],
   *      for `Music` description.
   */
  implicit object ImplicitMusicAsset extends Provider[Asset,GdxMusic] {
    def unpack(value: Option[Asset]): Option[GdxMusic] = {
      value match {
        case Some(MusicAsset(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: GdxMusic): Asset = MusicAsset(value)
  }

  /** Implicit object translating between `ParticleEffect` and [[com.specdevs.specgine.assets.Asset `Asset`]].
   *
   * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/g2d/ParticleEffect.html ''
   *      LibGDX API]], for `ParticleEffect` description.
   */
  implicit object ImplicitParticleEffectAsset extends Provider[Asset,GdxParticleEffect] {
    def unpack(value: Option[Asset]): Option[GdxParticleEffect] = {
      value match {
        case Some(ParticleEffectAsset(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: GdxParticleEffect): Asset = ParticleEffectAsset(value)
  }

  /** Implicit object translating between `Pixmap` and [[com.specdevs.specgine.assets.Asset `Asset`]].
   *
   * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/Pixmap.html ''
   *      LibGDX API'']], for `Pixmap` description.
   */
  implicit object ImplicitPixmapAsset extends Provider[Asset,GdxPixmap] {
    def unpack(value: Option[Asset]): Option[GdxPixmap] = {
      value match {
        case Some(PixmapAsset(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: GdxPixmap): Asset = PixmapAsset(value)
  }

  /** Implicit object translating between `Skin` and [[com.specdevs.specgine.assets.Asset `Asset`]].
   *
   * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/scenes/scene2d/ui/Skin.html ''
   *      LibGDX API'']], for `Skin` description.
   */
  implicit object ImplicitSkinAsset extends Provider[Asset,GdxSkin] {
    def unpack(value: Option[Asset]): Option[GdxSkin] = {
      value match {
        case Some(SkinAsset(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: GdxSkin): Asset = SkinAsset(value)
  }

  /** Implicit object translating between `Sound` and [[com.specdevs.specgine.assets.Asset `Asset`]].
   *
   * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/audio/Sound.html ''LibGDX API'']],
   *      for `Sound` description.
   */
  implicit object ImplicitSoundAsset extends Provider[Asset,GdxSound] {
    def unpack(value: Option[Asset]): Option[GdxSound] = {
      value match {
        case Some(SoundAsset(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: GdxSound): Asset = SoundAsset(value)
  }

  /** Implicit object translating between `Texture` and [[com.specdevs.specgine.assets.Asset `Asset`]].
   *
   * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/Texture.html ''
   *      LibGDX API'']], for `Texture` description.
   */
  implicit object ImplicitTextureAsset extends Provider[Asset,GdxTexture] {
    def unpack(value: Option[Asset]): Option[GdxTexture] = {
      value match {
        case Some(TextureAsset(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: GdxTexture): Asset = TextureAsset(value)
  }

  /** Implicit object translating between `TextureAtlas` and [[com.specdevs.specgine.assets.Asset `Asset`]].
   *
   * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/g2d/TextureAtlas.html ''
   *      LibGDX API'']], for `TextureAtlas` description.
   */
  implicit object ImplicitTextureAtlasAsset extends Provider[Asset,GdxTextureAtlas] {
    def unpack(value: Option[Asset]): Option[GdxTextureAtlas] = {
      value match {
        case Some(TextureAtlasAsset(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: GdxTextureAtlas): Asset = TextureAtlasAsset(value)
  }
}
