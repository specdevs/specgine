package com.specdevs.specgine.assets.gdx

import com.badlogic.gdx.assets.{AssetDescriptor => GdxAssetDescriptor,AssetManager => GdxAssetManager}
import com.badlogic.gdx.assets.{AssetLoaderParameters => GdxAssetLoaderParameters}
import com.badlogic.gdx.assets.loaders.{AsynchronousAssetLoader => GdxAsynchronousAssetLoader}
import com.badlogic.gdx.files.{FileHandle => GdxFileHandle}
import com.badlogic.gdx.utils.{Array => GdxArray}

/** AssetLoader to load [[com.specdevs.specgine.assets.gdx.StringListAsset `StringListAsset`]].
 *
 * Passed to [[com.specdevs.specgine.assets.gdx.StringListDescriptor `StringListDescriptor`]].
 */
class StringListLoader
  extends GdxAsynchronousAssetLoader[String, StringListLoader.StringListLoaderParameters](new AssetResolver) {
  private var string: String = ""

  /** Method for asynchronous loading of [[com.specdevs.specgine.assets.gdx.StringListAsset `StringListAsset`]].
   *
   * @param manager LibGDX Asset manager.
   * @param fileName Name of file to be loaded.
   * @param fileHandle Handle to file to be loaded.
   * @param parameter Parameters used in loading asset.
   */
  def loadAsync(
    manager: GdxAssetManager,
    fileName: String,
    fileHandle: GdxFileHandle,
    parameter: StringListLoader.StringListLoaderParameters
  ): Unit = {
    string = parameter.encoding match {
      case Some(encoding) => fileHandle.readString(encoding)
      case None    => fileHandle.readString()
    }
  }

  /** Method for loading of [[com.specdevs.specgine.assets.gdx.StringListAsset `StringListAsset`]].
   *
   * @param manager LibGDX Asset manager.
   * @param fileName Name of file to be loaded.
   * @param fileHandle Handle to file to be loaded.
   * @param parameter Parameters used in loading asset.
   */
  def loadSync(
    manager: GdxAssetManager,
    fileName: String,
    fileHandle: GdxFileHandle,
    parameter: StringListLoader.StringListLoaderParameters
  ): String = {
    string
  }

  /** Returns dependencies of loading [[com.specdevs.specgine.assets.gdx.StringListAsset `StringListAsset`]].
   *
   * @param fileName Name of file to be loaded.
   * @param fileHandle Handle to file to be loaded.
   * @param parameter Parameters used in loading asset.
   */
  def getDependencies(
    fileName: String,
    fileHandle: GdxFileHandle,
    parameter: StringListLoader.StringListLoaderParameters
  ): GdxArray[GdxAssetDescriptor[_]] = {
    new GdxArray[GdxAssetDescriptor[_]]
  }
}

/** Companion object for [[com.specdevs.specgine.assets.gdx.StringListLoader `StringListLoader`]] class. */
object StringListLoader {
  /** Parameter class to be passed to [[com.specdevs.specgine.assets.gdx.StringListDescriptor `StringListDescriptor`]]
   * if additional configuration is necessary.
   *
   * @param encoding Encoding used for file reading.
   */
  case class StringListLoaderParameters(encoding: Option[String] = None) extends GdxAssetLoaderParameters[String]
}
