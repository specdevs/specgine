package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.graphics.g3d.{Model => GdxModel}
import com.badlogic.gdx.graphics.g3d.loader.ObjLoader.{ObjLoaderParameters => GdxObjLoaderParameters}

/** [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]] for
 * [[com.specdevs.specgine.assets.gdx.ModelAsset `ModelAsset`]] in Obj format.
 *
 * @constructor New instance of `ObjModelDescriptor`.
 * @param flipV If `true` model will be vertically flipped.
 */
case class ObjModelDescriptor(flipV: Boolean = false) extends AssetDescriptorGdx {
  type T = GdxModel

  type P = GdxObjLoaderParameters

  val extensions: List[String] = List("obj")

  val assetClass: Class[GdxModel] = classOf[GdxModel]

  def loaderParameters: GdxObjLoaderParameters = {
    val params = new GdxObjLoaderParameters()
    params.flipV = flipV
    params
  }

  def packAsset(asset: Any): Option[Asset] = asset match {
    case (gdxAsset: GdxModel) => Some(ModelAsset(gdxAsset))
    case _ => None
  }
}
