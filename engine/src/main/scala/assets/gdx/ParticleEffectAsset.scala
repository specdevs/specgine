package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.graphics.g2d.{ParticleEffect => GdxParticleEffect}

/** `ParticleEffect` implementation for [[com.specdevs.specgine.assets.Asset `Asset`]].
 *
 * @param self LibGDX `ParticleEffect` value.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/g2d/ParticleEffect.html ''
 *      LibGDX API'']], for `ParticleEffect` description.
 */
case class ParticleEffectAsset(val self: GdxParticleEffect) extends AnyVal with Asset {
  def dispose(): Unit = {
    self.dispose()
  }
}
