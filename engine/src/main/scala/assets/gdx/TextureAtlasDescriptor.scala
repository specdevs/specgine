package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.graphics.g2d.{TextureAtlas => GdxTextureAtlas}
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader.{TextureAtlasParameter => GdxTextureAtlasParameter}

/** [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]] for
 * [[com.specdevs.specgine.assets.gdx.TextureAtlasAsset `TextureAtlasAsset`]].
 *
 * @constructor New instance of `TextureAtlasDescriptor`.
 * @param flip If `true` texture atlas will be vertically flipped.
 */
case class TextureAtlasDescriptor(flip: Boolean = false) extends AssetDescriptorGdx {
  type T = GdxTextureAtlas

  type P = GdxTextureAtlasParameter

  val extensions: List[String] = List("atlas")

  val assetClass: Class[GdxTextureAtlas] = classOf[GdxTextureAtlas]

  def loaderParameters: GdxTextureAtlasParameter = {
    val params = new GdxTextureAtlasParameter()
    params.flip = flip
    params
  }

  def packAsset(asset: Any): Option[Asset] = asset match {
    case (gdxAsset: GdxTextureAtlas) => Some(TextureAtlasAsset(gdxAsset))
    case _ => None
  }
}
