package com.specdevs.specgine.states

import com.specdevs.specgine.assets.AssetManagerUser
import com.specdevs.specgine.core.{StateManagerUser,ConfigManagerUser}
import com.specdevs.specgine.input.{AbstractInputManager,InputReceiver,InputProxy}
import com.specdevs.specgine.animation.TweenManagerUser

import scala.collection.mutable.{LongMap,ListBuffer}
import scala.concurrent.Future

/** Core class for [[com.specdevs.specgine.states.Entity `Entity`]] management.
 *
 * It contains methods to create and modify instances of
 * [[com.specdevs.specgine.states.Entity `Entity`]], and also keeps
 * sets of [[com.specdevs.specgine.states.Component `Component`]]s
 * associated with them.
 *
 * `World` class takes care for notifying
 * [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]s about
 * any changes to [[com.specdevs.specgine.states.Component `Component`]]s
 * structure, but not their values. It also distributes processing and rendering
 * to systems with [[com.specdevs.specgine.states.Processor `Processor`]]
 * or [[com.specdevs.specgine.states.Renderer `Renderer`]] mixed-in.
 *
 * [[com.specdevs.specgine.states.Entity `Entity`]] management methods
 * from `World` class can be accessed trough
 * [[com.specdevs.specgine.states.WorldUser `WorldUser`]] trait.
 *
 * @constructor Creates new `World` instance.
 */
class World extends AssetManagerUser with ConfigManagerUser with StateManagerUser with TweenManagerUser
  with InputProxy{
  private var current = Entity(0)

  private val systems = new EntitySystemQueue[EntitySystem]

  private val processable = new EntitySystemQueue[EntitySystem with Processor](_.processPriority)

  private val renderable = new EntitySystemQueue[EntitySystem with Renderer](_.renderPriority)

  private val receivers = new ListBuffer[Receiver]

  private val responders = new ListBuffer[Responder]

  private val added = new LongMap[Set[Component]]

  /** Method adding new [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]] to `World`.
   *
   * This method adds new [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
   * and ensures, that it knows about all earlier registered instances of
   * [[com.specdevs.specgine.states.Entity `Entity`]], and all future ones.
   *
   * @param system [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]] to add.
   */
  def addSystem(system: EntitySystem): Unit = {
    system.assetManager = assetManager
    system.configManager = configManager
    system.tweenManager = tweenManager
    system.stateManager = stateManager
    system.world = this
    for ((e, cs) <- added) {
      system.addEntityToSystem(Entity(e), cs)
    }
    system match {
      case processor: Processor => processable += processor
      case _ => ()
    }
    system match {
      case renderer: Renderer => renderable += renderer
      case _ => ()
    }
    system match {
      case receiver: Receiver => receivers += receiver
      case _ => ()
    }
    system match {
      case responder: Responder => responders += responder
      case _ => ()
    }
    systems += system
  }

  /** Method used to create [[com.specdevs.specgine.states.Entity `Entity`]].
   *
   * @param components Set of [[com.specdevs.specgine.states.Component `Component`]]s for new
   *        [[com.specdevs.specgine.states.Entity `Entity`]].
   * @return [[com.specdevs.specgine.states.Entity `Entity`]] associated with
   *        given set of [[com.specdevs.specgine.states.Component `Component`]]s.
   */
  def createEntity(components: Component*): Entity = {
    current = current.next
    val cs = components.toSet
    for (system <- systems) {
      system.addEntityToSystem(current, cs)
    }
    added += current.id -> cs
    current
  }

  /** Method used to add new [[com.specdevs.specgine.states.Component `Component`]]s
   * to [[com.specdevs.specgine.states.Entity `Entity`]].
   *
   * @param e [[com.specdevs.specgine.states.Entity `Entity`]] to which components will be added.
   * @param components Set of [[com.specdevs.specgine.states.Component `Component`]]s to add.
   */
  def addComponents(e: Entity, components: Component*): Unit = {
    if (!added.isDefinedAt(e.id)) {
      throw new IllegalArgumentException(s"Entity $e doesn't exist.")
    }
    added(e.id) ++= components
    val cs = added(e.id)
    for (system <- systems) {
      if (system systemWantsEntity cs) {
        system.addEntityToSystem(e, cs)
      } else if (system systemHasEntity e) {
        system.removeEntityFromSystem(e)
      }
    }
  }

  /** Method used to filter out [[com.specdevs.specgine.states.Component `Component`]]s
   * from [[com.specdevs.specgine.states.Entity `Entity`]].
   *
   * @param e [[com.specdevs.specgine.states.Entity `Entity`]] to from which components will be filtered out.
   * @param rule A predicate saying if component should be removed.
   */
  def filterComponents(e: Entity, rule: Component => Boolean): Unit = {
    if (!added.isDefinedAt(e.id)) {
      throw new IllegalArgumentException(s"Entity $e doesn't exist.")
    }
    added(e.id) = added(e.id).filter(rule)
    val cs = added(e.id)
    for (system <- systems) {
      if (system systemWantsEntity cs) {
        system.addEntityToSystem(e, cs)
      } else if (system systemHasEntity e) {
        system.removeEntityFromSystem(e)
      }
    }
  }

  /** Method used to remove [[com.specdevs.specgine.states.Entity `Entity`]].
   *
   * @param e [[com.specdevs.specgine.states.Entity `Entity`]] to remove.
   */
  def removeEntity(e: Entity): Unit = {
    if (!added.isDefinedAt(e.id)) {
      throw new IllegalArgumentException(s"Entity $e doesn't exist.")
    }
    for (system <- systems) {
      system.removeEntityFromSystem(e)
    }
    added -= e.id
    ()
  }

  /** Executes `processAll` method on all systems mixning-in
   * [[com.specdevs.specgine.states.Processor `Processor`]] trait.
   *
   * @param dt Time of simulation step.
   */
  def process(dt: Float): Unit = {
    for (system <- processable) {
      system.processAll(dt)
    }
  }

  /** Executes `renderAll` method on all systems mixning-in [[com.specdevs.specgine.states.Renderer `Renderer`]] trait.
   *
   * @param alpha Render interpolation between two last simulation steps (0..1).
   */
  def render(alpha: Float): Unit = {
    for (system <- renderable) {
      system.renderAll(alpha)
    }
  }

  /** Helper method correctly calling `initialize`, used by
   * [[com.specdevs.specgine.states.GameState `GameState`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doInitialize(): Unit = {
    for (system <- systems) {
      system.doInitialize()
    }
  }

  /** Helper method correctly calling `create`, used by
   * [[com.specdevs.specgine.states.GameState `GameState`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doCreate(): Unit = {
    for (system <- systems) {
      system.doCreate()
    }
  }

  /** Helper method correctly calling `enter`, used by
   * [[com.specdevs.specgine.states.GameState `GameState`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doEnter(): Unit = {
    for (system <- systems) {
      system.doEnter()
    }
  }

  /** Helper method correctly calling `leave`, used by
   * [[com.specdevs.specgine.states.GameState `GameState`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doLeave(): Unit = {
    for (system <- systems) {
      system.doLeave()
    }
  }

  /** Helper method correctly calling `dispose`, used by
   * [[com.specdevs.specgine.states.GameState `GameState`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doDispose(): Unit = {
    for (system <- systems) {
      system.doDispose()
    }
    current = Entity(0)
    systems.clear()
    added.clear()
    processable.clear()
    renderable.clear()
    disposeAssets()
    disposeTweens()
  }

  /** Executes `resize` method on all systems known to World.
   *
   * @param x Screen width.
   * @param y Screen heigth.
   */
  def resize(x: Int, y: Int): Unit = {
    for (system <- systems) {
      system.resize(x,y)
    }
  }

  /** Executes `resizeFrozen` method on all systems known to World.
   *
   * @param x Screen width.
   * @param y Screen heigth.
   */
  def resizeFrozen(x: Int, y: Int): Unit = {
    for (system <- systems) {
      system.resizeFrozen(x, y)
    }
  }

  def addReceivers(manager: AbstractInputManager): Unit = {
    for (system <- systems) {
      system match {
        case x: InputReceiver => manager.addReceiver(x, system.inputPriority)
        case _ => ()
      }
      system match {
        case x: InputProxy => x.addReceivers(manager)
        case _ => ()
      }
    }
  }

  /** Method used to send a message to receivers.
   *
   * @param message A message to be sent.
   */
  def send(message: Message): Unit = {
    receivers foreach (_.receive(message))
  }

  /** Method used to find replies for a given message.
   *
   * @param message A message to be replied to.
   * @return all replies for a given message.
   */
  def query(message: Message): Stream[Reply] = {
    def respondersToStream(responders: List[Responder]): Stream[Reply] = responders match {
      case Nil => Stream.empty
      case head::tail => head.respond(message) match {
        case None => respondersToStream(tail)
        case Some(reply) => reply #:: respondersToStream(tail)
      }
    }
    respondersToStream(responders.toList)
  }

  /** Method used to find replies for a given message asynchronolously.
   *
   * @param message A message to be replied to.
   * @return all replies for a given message.
   */
  def queryAsync(message: Message): Stream[Future[Reply]] = {
    def respondersToStreamAsync(responders: List[Responder]): Stream[Future[Reply]] = responders match {
      case Nil => Stream.empty
      case head::tail => Future {
        head.respond(message) match {
          case Some(reply) => reply
          case None => throw new NoReplyAvailable
        }
      } #:: respondersToStreamAsync(tail)
    }
    respondersToStreamAsync(responders.toList)
  }

  /** Method used to find first reply for a given message.
   *
   * @param message A message to be replied to.
   * @return first reply for a given message.
   */
  def queryFirst(message: Message): Option[Reply] = {
    query(message).headOption
  }

  /** Method used to find first reply for a given message asynchronolously.
   *
   * @param message A message to be replied to.
   * @return first reply for a given message.
   */
  def queryFirstAsync(message: Message): Future[Reply] = Future {
    queryFirst(message) match {
      case Some(reply) => reply
      case None => throw new NoReplyAvailable
    }
  }
}
