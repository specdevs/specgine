package com.specdevs.specgine.states

/** Trait for components specification inside entity container.
 *
 * This trait is accompanied by
 * [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]].
 * Normal use case include subclassing the `ComponentsSpec`,
 * adding fields with maps from [[com.specdevs.specgine.states.Entity `Entity`]]
 * to some container of [[com.specdevs.specgine.states.Component `Component`]]s,
 * and them mixing-in
 * [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * implementing functionality to operate on all those fields at once.
 * It means, that well defined entity container is of type
 * "`ComponentsSpec with ComponentsManager`".
 *
 * `ComponentsSpec` also allows to group entities using arbitrary rules.
 * It can be used to group them to display in layered system with
 * custom drawing order, or to limit number of possible collisions
 * by grouping search space.
 *
 * We provide a way to automatically generate `ComponentsManager`
 * based on [[com.specdevs.specgine.states.ComponentsSpec `ComponentsSpec`]]
 * using `withManager` macro from [[com.specdevs.specgine.macros.states]]
 * package. It is recommended way to create `ComponentsManager`s,
 * but not only way to do so.
 */
trait ComponentsSpec {
  /** Group [[com.specdevs.specgine.states.Entity `Entity`]] belongs to.
   *
   * @param e [[com.specdevs.specgine.states.Entity `Entity`]] to be checked.
   * @return Group number or `None` if it does not belong to any group.
   */
  def group(e: Entity): Option[Int] = None
}
