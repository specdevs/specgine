package com.specdevs.specgine.states.gdx

import com.specdevs.specgine.assets.{AssetDescriptor,GuessAsset}
import com.specdevs.specgine.assets.gdx.{StringListAsset}
import com.specdevs.specgine.states._

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.{GL20 => GdxGL20, Color => GdxColor}
import com.badlogic.gdx.graphics.g2d.{SpriteBatch => GdxSpriteBatch}
import com.badlogic.gdx.graphics.g2d.{BitmapFont => GdxBitmapFont}
import com.badlogic.gdx.math.{Matrix4 => GdxMatrix4}

import scala.math.min

/** Text Slide based on LibGDX.
 *
 * Slide displays loaded text for
 * `fadeIn` + `time` + `fadeOut` seconds, starting from black screen.
 * Then it will linearly transform to the chosen font's color. It will be rendered
 * like that and then fade out (linearly) to black screen.
 *
 * Size of rendered text can be customized by textWidth and textHeight parameters,
 * describing maximal portion of screen width and height respectively. Defaults are 0.9.
 *
 * @constructor Creates new [[com.specdevs.specgine.states.Slide `Slide`]] to display text.
 * @param text List of Strings to be loaded. Each element of a list will be presented in a new line.
 * @param font Desired font - an optional parameter. If not given, the default font (Arial 15) will be used.
 * @param time Amount of time the text is fully visible.
 * @param isBreakable Declares whether slide can be stopped before it ends.
 * @param textWidth Maximal portion of screen width used for setting up text width (0..1).
 * @param textHeight Maximal portion of screen height used for setting up text height (0..1).
 * @param fadeIn Amount of time the text is fading in.
 * @param fadeOut Amount of time the text is fading out.
 */
class TextSlide(
    text: List[String],
    font: Option[GdxBitmapFont] = None,
    alignement: GdxBitmapFont.HAlignment = GdxBitmapFont.HAlignment.LEFT,
    time: Float = 3f,
    isBreakable: Boolean = true,
    textWidth: Float = 0.9f,
    textHeight: Float = 0.9f,
    fadeIn: Float = 1f,
    fadeOut: Float =  1f) extends Slide {

  private var batch: Option[GdxSpriteBatch] = None

  private val viewMatrix: GdxMatrix4 = new GdxMatrix4

  private var slideFont: Option[GdxBitmapFont] = None

  private var needsLoading = false

  private var assetsName = ""

  private var assetDescriptor: Option[AssetDescriptor] = None

  private var textAsset: List[String] = Nil

  private var screenWidth = 0f

  private var screenHeight = 0f

  private var t = 0f

  private var oldt = 0f

  private var withLines = ""

  private var maxWidth = 0f

  private var maxHeight = 0f

  private var baseWidthRatio = 1f

  private var baseHeightRatio = 1f

  private var baseScaleX = 1f

  private var baseScaleY = 1f

  private var baseColor = new GdxColor(1f, 1f, 1f, 1f)

  def isFinished: Boolean = t > duration

  def breakable: Boolean = isBreakable

  def duration: Float = time + fadeIn + fadeOut

  def initialize(): Unit = {
    batch = Some(new GdxSpriteBatch)
    if (needsLoading) {
      loadAsset(assetsName, assetDescriptor.get)
    }
    slideFont = Some(font getOrElse new GdxBitmapFont)
  }

  def create(): Unit = {
    Gdx.gl20.glClearColor(0f, 0f, 0f, 0f)

    textAsset = if (needsLoading) {
      getAsset(assetsName) match {
        case Some(StringListAsset(text)) => text
        case _ => throw new IllegalArgumentException("Unsupported resource type.")
      }
    } else {
      text
    }

    withLines = textAsset mkString "\n"

    baseScaleX = slideFont.get.getScaleX
    baseScaleY = slideFont.get.getScaleY
    baseColor = slideFont.get.getColor

    val bounds = slideFont.get.getMultiLineBounds(withLines)
    maxWidth = bounds.width
    maxHeight = bounds.height
  }

  def process(dt: Float): Unit = {
    oldt = t
    t += dt
  }

  def render(alpha: Float): Unit = {
    val now = t*alpha + oldt*(1-alpha)
    Gdx.gl20.glClear(GdxGL20.GL_COLOR_BUFFER_BIT)
    batch.get.begin()
    val currentColor = new GdxColor(baseColor)
    if (now < fadeIn) {
      currentColor.a = currentColor.a*now/fadeIn
      slideFont.get.setColor(currentColor)
    }
    if (now > fadeIn + time) {
      currentColor.a = currentColor.a*(fadeOut-(now-time-fadeIn))/fadeOut
      slideFont.get.setColor(currentColor)
    }

    val x = (1f-textWidth)/2*screenWidth
    val y = (1f+textHeight)/2*screenHeight
    slideFont.get.drawMultiLine(batch.get, withLines, x, y, textWidth*screenWidth, alignement)

    batch.get.end()
  }

  def dispose(): Unit = {
    if (font.isEmpty) {
      slideFont.get.dispose()
    } else {
      font.get.setColor(baseColor)
      font.get.setScale(baseScaleX, baseScaleY)
    }
    batch.get.dispose()
  }

  def resize(x: Int, y: Int): Unit = {
    if (screenWidth==0f) {
      baseWidthRatio = min(maxWidth/(textWidth*x.toFloat), 1f)
      baseHeightRatio = min(maxHeight/(textHeight*y.toFloat), 1f)
    }

    screenWidth = x.toFloat
    screenHeight = y.toFloat

    val scale = min(baseWidthRatio*textWidth*screenWidth/maxWidth, baseHeightRatio*textHeight*screenHeight/maxHeight)
    slideFont.get.setScale(baseScaleX*scale, baseScaleY*scale)

    viewMatrix.setToOrtho2D(0f, 0f, screenWidth, screenHeight)
    batch.get.setProjectionMatrix(viewMatrix)
  }
}
/** Companion object for [[com.specdevs.specgine.states.gdx.TextSlide `TextSlide`]] class. */
object TextSlide {
  /** Creates new [[com.specdevs.specgine.states.Slide `Slide`]] to display text loaded from file.
   *
   * @param textFileName Name of asset to be loaded, pointing to text file.
   * @param font Desired font - an optional parameter. If not given, the default font (Arial 15) will be used.
   * @param time Amount of time the text is fully visible.
   * @param isBreakable Declares whether slide can be stopped before it ends.
   * @param textWidth Maximal portion of screen width used for setting up text width (0..1).
   * @param textHeight Maximal portion of screen height used for setting up text height (0..1).
   * @param fadeIn Amount of time the text is fading in.
   * @param fadeOut Amount of time the text is fading out.
   * @param descriptor Asset descriptor used during loading, by default it is
   *        guessed using [[com.specdevs.specgine.assets.GuessAsset `GuessAsset`]].
   */
  def fromFile(
    textFileName: String,
    font: Option[GdxBitmapFont] = None,
    alignement: GdxBitmapFont.HAlignment = GdxBitmapFont.HAlignment.LEFT,
    time: Float = 3f,
    isBreakable: Boolean = true,
    textWidth: Float = 0.9f,
    textHeight: Float = 0.9f,
    fadeIn: Float = 1f,
    fadeOut: Float =  1f,
    descriptor: AssetDescriptor = GuessAsset): TextSlide = {
    val slide = new TextSlide(
      Nil,
      font,
      alignement,
      time,
      isBreakable,
      textWidth,
      textHeight,
      fadeIn,
      fadeOut
    )
    slide.needsLoading = true
    slide.assetsName = textFileName
    slide.assetDescriptor = Some(descriptor)
    slide
  }
}
