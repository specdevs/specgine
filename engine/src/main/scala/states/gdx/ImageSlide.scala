package com.specdevs.specgine.states.gdx

import com.specdevs.specgine.states._

import com.specdevs.specgine.assets.{AssetDescriptor,GuessAsset}
import com.specdevs.specgine.assets.gdx.{PixmapAsset,TextureAsset}

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.{GL20 => GdxGL20}
import com.badlogic.gdx.graphics.g2d.{SpriteBatch => GdxSpriteBatch}
import com.badlogic.gdx.graphics.{Texture => GdxTexture}
import com.badlogic.gdx.math.{Matrix4 => GdxMatrix4}

import scala.math.max

/** Image Slide based on LibGDX.
 *
 * Slide loads LibGDX texture or pixmap. Loaded image will be displayed for
 * `fadeIn` + `time` + `fadeOut` seconds, starting from black screen.
 * Then it will linearly transform to fully opaque. It will be rendered
 * like that and then fade out (linearly) to black screen.
 *
 * Size of rendered image can be customized by width and height parameters,
 * describing portion of screen width and height respectively. If only one
 * size is specified, then the other one is calculated in such way, that
 * image aspect ratio is kept. If both width and height are specified,
 * image can be stretched to fill given portion of screen. Not specifying
 * sizes results in image that does not scale unless it does not fit screen,
 * then it is squeezed just enough to fit and keep aspect ratio. Images are
 * always centred both horizontally and vertically.
 *
 * @constructor Creates new [[com.specdevs.specgine.states.Slide `Slide`]] to display images using LibGDX framework.
 * @param imageName Name of asset to be loaded, pointing to either pixmap or texture.
 * @param time Amount of time the image is fully visible.
 * @param isBreakable Declares whether slide can be stopped before it ends.
 * @param width Portion of screen width used for setting up image width (0..1).
 * @param height Portion of screen height used for setting up image height (0..1).
 * @param fadeIn Amount of time the image is fading in.
 * @param fadeOut Amount of time the image is fading out.
 * @param descriptor Asset descriptor used during loading, by default it is
 *        guessed using [[com.specdevs.specgine.assets.GuessAsset `GuessAsset`]].
 */
class ImageSlide(
    imageName: String,
    time: Float = 3f,
    isBreakable: Boolean = true,
    width: Option[Float] = None,
    height: Option[Float] = None,
    fadeIn: Float = 1f,
    fadeOut: Float =  1f,
    descriptor: AssetDescriptor = GuessAsset) extends Slide {
  private var image: Option[GdxTexture] = None

  private var batch: Option[GdxSpriteBatch] = None

  private val viewMatrix: GdxMatrix4 = new GdxMatrix4

  private var screenWidth = 0f

  private var screenHeight = 0f

  private var imageWidth = 0f

  private var imageHeight = 0f

  private var isLoaded = false

  private var t = 0f

  private var oldt = 0f

  def isFinished: Boolean = {
    t > duration
  }

  def breakable: Boolean = isBreakable

  def duration: Float = time+fadeIn+fadeOut

  def initialize(): Unit = {
    loadAsset(imageName, descriptor)
    batch = Some(new GdxSpriteBatch)
  }

  def create(): Unit = {
    Gdx.gl20.glClearColor(0f, 0f, 0f, 0f)
    image = getAsset(imageName) match {
      case Some(TextureAsset(texture)) => Some(texture)
      case Some(PixmapAsset(pixmap)) => {
        isLoaded = true
        Some(new GdxTexture(pixmap))
      }
      case _ =>
        throw new IllegalArgumentException(
          "Unsupported resource type, you should create your own Slide implementation."
        )
    }
    imageWidth = image.get.getWidth().toFloat
    imageHeight = image.get.getHeight().toFloat
  }

  def process(dt: Float): Unit = {
    oldt = t
    t += dt
  }

  def render(alpha: Float): Unit = {
    val now = t*alpha + oldt*(1-alpha)
    Gdx.gl20.glClear(GdxGL20.GL_COLOR_BUFFER_BIT)
    batch.get.begin()
    if (now < fadeIn) {
      batch.get.setColor(1f, 1f, 1f, now/fadeIn)
    }
    if (now > fadeIn + time) {
      batch.get.setColor(1f, 1f, 1f, 1f-(now-time-fadeIn)/fadeOut)
    }
    batch.get.draw(image.get, (screenWidth-imageWidth)/2, (screenHeight-imageHeight)/2, imageWidth, imageHeight)
    batch.get.end()
  }

  def dispose(): Unit = {
    if (isLoaded) {
      image.get.dispose()
      image = None
      isLoaded = false
    }
    batch.get.dispose()
  }

  def resize(x: Int, y: Int): Unit = {
    screenWidth = x.toFloat
    screenHeight = y.toFloat
    val imW = image.get.getWidth()
    val imH = image.get.getHeight()
    val size = (width, height) match {
      case (Some(w), Some(h)) => {
        (w*x, h*y)
      }
      case (None, None) => {
        val scale = max(max(imW/screenWidth, imH/screenHeight), 1f)
        (imW/scale, imH/scale)
      }
      case (Some(w), None) => {
        val h = w*x*imH/imW
        val scale = max(h/y, 1f)
        (w*x/scale, h/scale)
      }
      case (None, Some(h)) => {
        val w = h*y*imW/imH
        val scale = max(w/x, 1f)
        (w/scale, h*y/scale)
      }
    }
    imageWidth = size._1
    imageHeight = size._2
    viewMatrix.setToOrtho2D(0f, 0f, screenWidth, screenHeight)
    batch.get.setProjectionMatrix(viewMatrix)
  }
}
