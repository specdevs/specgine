package com.specdevs.specgine.states.gdx

import com.specdevs.specgine.states._

import com.specdevs.specgine.gui.gdx.Canvas

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.{GL20 => GdxGL20}

/** Default menu screen implementation based on
 * [[com.specdevs.specgine.gui.gdx.Canvas `Canvas`]].
 *
 * Simple implementation of menu screen based on
 * [[com.specdevs.specgine.gui.gdx.Canvas `Canvas`]] and `Scene2d.ui`
 * library from LibGDX framework.
 *
 * Because creating GUI already requires, that default `skin` is
 * present, most operations will be done in `create`. It guarantees,
 * that resource loading has already finished.
 *
 * In below example we create button named "Resume", that when
 * clicked will `popState`. It can be implementation of "Pause" menu.
 * {{{
 * // assumes that skin "defaultSkin" is loaded or created during initialize
 * def create(): Unit = {
 *   val skin = get[Asset,GdxSkin]("defaultSkin")
 *   val button = new TextButton("Resume", skin)
 *   table.add(button).width(120).pad(10)
 *   button.addListener(new ChangeListener {
 *     def changed(event: ChangeEvent, actor: Actor): Unit = {
 *       popState()
 *     }
 *   })
 * }
 * }}}
 *
 * Optional `width` and `height` parameters describe [[com.specdevs.specgine.gui.gdx.Canvas `Canvas`]]
 * design size and are automatically set in it using `canvasWidth` and `canvasHeight`.
 *
 * @see [[https://github.com/libgdx/libgdx/wiki/Scene2d.ui ''LibGDX wiki'']],
 *      for `scene2d.ui` manual and information on how to create menus.
 * @constructor Creates new instance of [[com.specdevs.specgine.states.AbstractMenuScreen `AbstractMenuScreen`]].
 * @param width Optional width of content in pixels, `None` if no scaling should be applied.
 * @param height Optional height of content in pixels, `None` if no scaling should be applied.
 * @note Currently one has to use LibGDX syntax to build GUI, but this
 *       might change in near future, with introduction of GUI-building DSL.
 */
abstract class MenuScreen(
    width: Option[Float] = None,
    height: Option[Float] = None)
    extends AbstractMenuScreen with Canvas {
  override def doInitialize(): Unit = {
    super.doInitialize()
    canvasWidth = width
    canvasHeight = height
    initializeCanvas()
  }

  def enter(): Unit = {
    Gdx.gl20.glClearColor(0f, 0f, 0f, 0f)
  }

  def leave(): Unit = {}

  override def doDispose(): Unit = {
    disposeCanvas()
    super.doDispose()
  }

  def processBackground(dt: Float): Unit = {}

  def process(dt: Float): Unit = {
    processBackground(dt)
    processCanvas(dt)
  }

  def resize(x: Int, y: Int): Unit = {
    resizeCanvas(x, y)
  }

  def renderBackground(alpha: Float): Unit = {
    Gdx.gl20.glClear(GdxGL20.GL_COLOR_BUFFER_BIT)
  }

  def render(alpha: Float): Unit = {
    renderBackground(alpha)
    renderCanvas(alpha)
  }
}
