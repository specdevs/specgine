package com.specdevs.specgine.states.gdx

import com.specdevs.specgine.states._

import com.specdevs.specgine.gui.gdx.Canvas

/** Default Graphical User Interface system implementation based on
 * [[com.specdevs.specgine.gui.gdx.Canvas `Canvas`]].
 *
 * Simple implementation of [[com.specdevs.specgine.states.VoidEntitySystem `VoidEntitySystem`]]
 * based on [[com.specdevs.specgine.gui.gdx.Canvas `Canvas`]] and `Scene2d.ui`
 * library from LibGDX framework.
 *
 * Because creating GUI already requires, that default `skin` is
 * present, most operations will be done in `create`. It guarantees,
 * that resource loading has already finished.
 *
 * `GUISystem` sets default input and process priorities to `-100`,
 * and rendering priority to `100`, to process before other systems
 * and render on top of other systems. If you need different values,
 * you can override default priorities in concrete class.
 *
 * @see [[https://github.com/libgdx/libgdx/wiki/Scene2d.ui ''LibGDX wiki'']],
 *      for `scene2d.ui` manual and information on how to create menus.
 * @constructor Creates new instance of [[com.specdevs.specgine.states.VoidEntitySystem `VoidEntitySystem`]]
 *              with [[com.specdevs.specgine.gui.gdx.Canvas `Canvas`]].
 * @param width Optional width of content in pixels, `None` if no scaling should be applied.
 * @param height Optional height of content in pixels, `None` if no scaling should be applied.
 * @note Currently one has to use LibGDX syntax to build GUI, but this
 *       might change in near future, with introduction of GUI-building DSL.
 */
abstract class GUISystem extends VoidEntitySystem(-100, -100, 100) with Canvas with Processor with Renderer {
  override def doInitialize(): Unit = {
    super.doInitialize()
    initializeCanvas()
  }

  def enter(): Unit = {}

  def leave(): Unit = {}

  override def doDispose(): Unit = {
    disposeCanvas()
    super.doDispose()
  }

  override def process(dt: Float): Unit = {
    processCanvas(dt)
  }

  override def render(alpha: Float): Unit = {
    renderCanvas(alpha)
  }

  override def resize(x: Int, y: Int): Unit = {
    resizeCanvas(x, y)
  }
}
