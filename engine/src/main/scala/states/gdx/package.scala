package com.specdevs.specgine.states

/** Package implementing state classes using LibGDX framework. */
package object gdx
