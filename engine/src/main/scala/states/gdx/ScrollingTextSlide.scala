package com.specdevs.specgine.states.gdx

import com.specdevs.specgine.assets.{AssetDescriptor,GuessAsset}
import com.specdevs.specgine.assets.gdx.{StringListAsset,StringListDescriptor}
import com.specdevs.specgine.states._

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.{GL20 => GdxGL20, Color => GdxColor}
import com.badlogic.gdx.graphics.g2d.{SpriteBatch => GdxSpriteBatch}
import com.badlogic.gdx.graphics.g2d.{BitmapFont => GdxBitmapFont}
import com.badlogic.gdx.graphics.g2d.BitmapFont.{TextBounds=>GdxTextBounds}
import com.badlogic.gdx.math.{Matrix4 => GdxMatrix4}

import scala.math.min

/** Scrolling text slide based on LibGDX.
 *
 * ScrollingTextSlide displays loaded text for `time` seconds by moving it
 * either from bottom to top or from top to bottom.
 *
 * Width of rendered text can be customized by textWidth parameter,
 * describing maximal portion of screen width. Default is 0.9.
 *
 * @constructor Creates new [[com.specdevs.specgine.states.Slide `Slide`]] to display text.
 * @param text List of Strings to be loaded. Each element of a list will be presented in a new line.
 * @param font Desired font - an optional parameter. If not given, the default font (Arial 15) will be used.
 * @param alignemetn Text alignement.
 * @param flipY Declares whether the text should be reversed and emerge from the top.
 * @param time Amount of time the text is visible.
 * @param displacement Displacement of moving text (time dependent function).
 * @param isBreakable Declares whether slide can be stopped before it ends.
 * @param textWidth Maxinmal portion of screen width used for setting up text width (0..1).
 * @param isWrapped Determines whether the text should be wrapped or squeezed into a line.
 */
class ScrollingTextSlide(
    text: List[String],
    font: Option[GdxBitmapFont] = None,
    alignement: GdxBitmapFont.HAlignment = GdxBitmapFont.HAlignment.LEFT,
    flipY: Boolean = false,
    time: Float = 3f,
    displacement: (Float => Float) = _*20f,
    isBreakable: Boolean = true,
    textWidth: Float = 0.9f,
    isWrapped: Boolean = false) extends Slide {

  private var batch: Option[GdxSpriteBatch] = None

  private val viewMatrix: GdxMatrix4 = new GdxMatrix4

  private var slideFont: Option[GdxBitmapFont] = None

  private var needsLoading = false

  private var assetsName = ""

  private var assetDescriptor: Option[AssetDescriptor] = None

  private var textAsset: List[String] = Nil

  private var screenWidth = 0f

  private var screenHeight = 0f

  private var t = 0f

  private var oldt = 0f

  private var withLines = ""

  private var maxWidth = 0f

  private var maxHeight = 0f

  private var baseWidthRatio = 1f

  private var startY = 0f

  private val direction = if (flipY) -1f else 1f

  private var scale = 1f

  private type DrawFun = (GdxSpriteBatch, CharSequence, Float, Float, Float, GdxBitmapFont.HAlignment) => GdxTextBounds

  private var baseScaleX = 1f

  private var baseScaleY = 1f

  private var baseColor = new GdxColor(1f, 1f, 1f, 1f)

  def isFinished: Boolean = t > duration

  def breakable: Boolean = isBreakable

  def duration: Float = time

  def initialize(): Unit = {
    batch = Some(new GdxSpriteBatch)
    if (needsLoading) {
      loadAsset(assetsName, assetDescriptor.get)
    }
    slideFont = Some(font getOrElse new GdxBitmapFont)
  }

  def create(): Unit = {
    Gdx.gl20.glClearColor(0f, 0f, 0f, 0f)

    textAsset = if (needsLoading) {
      getAsset(assetsName) match {
        case Some(StringListAsset(text)) => text
        case _ => throw new IllegalArgumentException("Unsupported resource type.")
      }
    } else {
      text
    }

    baseScaleX = slideFont.get.getScaleX
    baseScaleY = slideFont.get.getScaleY
    baseColor = slideFont.get.getColor

    withLines = (if (flipY) textAsset.reverse else textAsset) mkString "\n"

    val bounds = slideFont.get.getMultiLineBounds(withLines)
    maxWidth = bounds.width
    maxHeight = bounds.height
  }

  def process(dt: Float): Unit = {
    oldt = t
    t += dt
  }

  def render(alpha: Float): Unit = {
    val now = t*alpha + oldt*(1-alpha)
    val currentY = startY + direction*displacement(now)*scale

    Gdx.gl20.glClear(GdxGL20.GL_COLOR_BUFFER_BIT)
    batch.get.begin()

    if (isWrapped) {
      drawText(currentY, slideFont.get.drawWrapped)
    } else {
      drawText(currentY, slideFont.get.drawMultiLine)
    }

    batch.get.end()
  }

  private def drawText(currentY: Float, f: DrawFun): GdxTextBounds = {
    f(batch.get, withLines, (1f-textWidth)/2*screenWidth, currentY, textWidth*screenWidth, alignement)
  }

  def dispose(): Unit = {
    if (font.isEmpty) {
      slideFont.get.dispose()
    } else {
      font.get.setColor(baseColor)
      font.get.setScale(baseScaleX, baseScaleY)
    }
    batch.get.dispose()
  }

  def resize(x: Int, y: Int): Unit = {
    if (screenWidth==0f) {
      baseWidthRatio = baseScaleX*min(maxWidth/(textWidth*x.toFloat), 1f)
      if (isWrapped) {
        val bounds = slideFont.get.getWrappedBounds(withLines, textWidth*x.toFloat)
        maxWidth = bounds.width
        maxHeight = bounds.height
      }
    } else {
      scale = scale*x.toFloat/screenWidth
    }

    screenWidth = x.toFloat
    screenHeight = y.toFloat

    val fontScale = baseWidthRatio*textWidth*screenWidth/maxWidth
    slideFont.get.setScale(fontScale, baseScaleY*fontScale)

    startY = if (flipY) y.toFloat + maxHeight*fontScale else 0f

    viewMatrix.setToOrtho2D(0f, 0f, screenWidth, screenHeight)
    batch.get.setProjectionMatrix(viewMatrix)
  }
}

/** Companion object for [[com.specdevs.specgine.states.gdx.ScrollingTextSlide `ScrollingTextSlide`]] class. */
object ScrollingTextSlide {
  /** Creates new [[com.specdevs.specgine.states.Slide `Slide`]] to display moving text loaded from file.
   *
   * @param textFileName Name of asset to be loaded, pointing to text file.
   * @param font Desired font - an optional parameter. If not given, the default font (Arial 15) will be used.
   * @param alignemetn Text alignement.
   * @param flipY Declares whether the text should be reversed and emerge from the top.
   * @param time Amount of time the text is visible.
   * @param displacement Displacement of moving text (time dependent function).
   * @param isBreakable Declares whether slide can be stopped before it ends.
   * @param textWidth Maxinmal portion of screen width used for setting up text width (0..1).
   * @param isWrapped Determines whether the text should be wrapped or squeezed into a line.
   * @param descriptor Asset descriptor used during loading, by default it is
   *        guessed using [[com.specdevs.specgine.assets.GuessAsset `GuessAsset`]].
   */
  def fromFile(
    textFileName: String,
    font: Option[GdxBitmapFont] = None,
    alignement: GdxBitmapFont.HAlignment = GdxBitmapFont.HAlignment.LEFT,
    flipY: Boolean = false,
    time: Float = 3f,
    displacement: (Float => Float) = _*20f,
    isBreakable: Boolean = true,
    textWidth: Float = 0.9f,
    isWrapped: Boolean = false,
    descriptor: AssetDescriptor = GuessAsset): ScrollingTextSlide = {
    val slide = new ScrollingTextSlide(
      Nil,
      font,
      alignement,
      flipY,
      time,
      displacement,
      isBreakable,
      textWidth,
      isWrapped
    )
    slide.needsLoading = true
    slide.assetsName = textFileName
    slide.assetDescriptor = Some(descriptor)
    slide
  }
}
