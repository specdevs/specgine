package com.specdevs.specgine.states
/** Represents different aspescts, attributes or features of [[com.specdevs.specgine.states.Entity `Entity`]] instances.
 *
 * Parent for all component types, from which each [[com.specdevs.specgine.states.Entity `Entity`]] can be composed.
 */
trait Component
