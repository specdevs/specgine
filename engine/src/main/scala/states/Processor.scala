package com.specdevs.specgine.states

/** Trait used to signalize, that [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * which should process entities.
 *
 * Only [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]s
 * mixning-in `Processor` will be informed
 * [[com.specdevs.specgine.states.World `World`]], that they should
 * process entities.
 */
trait Processor extends ProcessorImplementation
