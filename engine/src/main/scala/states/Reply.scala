package com.specdevs.specgine.states

/** Represents replies returned by queries.
 *
 * Replies can be given by [[com.specdevs.specgine.states.Responder `Responder`]]s.
 */
trait Reply
