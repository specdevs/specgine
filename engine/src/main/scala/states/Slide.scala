package com.specdevs.specgine.states

import com.specdevs.specgine.assets.AssetManagerUser
import com.specdevs.specgine.core.GetSetUser

/** Trait that is parent of all [[com.specdevs.specgine.states.Slide `Slide`]]s implementation.
 *
 * `Slide`s in SpecGine are very abstract. Technically, they can be pretty
 * anything that contains no interactivity othen than optional stopping, and
 * can be played in sequential order. Examples include all kinds of logos,
 * but also animated cutscenes, scrolling credits and lot more.
 *
 * To use it, user should implement the following abstract methods:
 *   - `initialize`,
 *   - `create`,
 *   - `enter`,
 *   - `leave`,
 *   - `dispose`.
 *
 * And:
 *   - `process`,
 *   - `render`,
 *   - `resize`.
 *
 * There is example implementation of `Slide` to display static images
 * using LibGDX framework inside [[com.specdevs.specgine.states.gdx]] sub-package.
 */
trait Slide extends AssetManagerUser with GetSetUser {
  /** Declares whether you can stop viewing slide before it ends.
   *
   * @return `true` if slide can be stopped.
   */
  def breakable: Boolean

  /** Slides duration.
   *
   * @return Duration of slide in seconds.
   */
  def duration: Float

  /** Declares whether slide is finished.
   *
   * @return `true` if slide has finished.
   */
  def isFinished: Boolean

  /** Method called, when Slide is initialized for the first time.
   *
   * This is perfect place to load your assets or set some configuration.
   */
  def initialize(): Unit

  /** Helper method correctly calling `initialize`, used by
   * [[com.specdevs.specgine.states.SlideShowState `SlideShowState`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doInitialize(): Unit = {
    initialize()
  }

  /** Method called, when Slide is loaded.
   *
   * This is perfect place store loaded assets or allocate
   * local data structures. You do not have guarantee, that `resize` was
   * called.
   */
  def create(): Unit

  /** Helper method correctly calling `create`, used by
   * [[com.specdevs.specgine.states.SlideShowState `SlideShowState`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doCreate(): Unit = {
    create()
  }

  /** Method called, when state is permamently removed from
   * [[com.specdevs.specgine.states.SlideShowState `SlideShowState`]].
   *
   * Remember to deallocate all resources you allocated yourself inside
   * `create` or `initialize`.
   */
  def dispose(): Unit

  /** Helper method correctly calling `dispose`, used by
   * [[com.specdevs.specgine.states.SlideShowState `SlideShowState`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doDispose(): Unit = {
    dispose()
    disposeAssets()
  }

  /** Method for processing slide.
   *
   * @param dt Time simulation step.
   */
  def process(dt: Float): Unit

  /** Method for rendering slide.
   *
   * @param alpha Render interpolation between two processing steps.
   */
  def render(alpha: Float): Unit

  /** Method called when size of screen changes.
   *
   * @param x Screen width.
   * @param y Screen height.
   */
  def resize(x: Int, y: Int): Unit
}
