package com.specdevs.specgine.states

/** Declaration of processing method implementation.
 *
 * `processAll` is common interface called by
 * [[com.specdevs.specgine.states.World `World`]] during its `process`
 * method for all [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]s
 * mixning-in [[com.specdevs.specgine.states.Processor `Processor`]]
 * trait.
 *
 * All default [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]s
 * already provide `ProcessorImplementation` with suitable `processAll`
 * method.
 */
trait ProcessorImplementation {
  /** Method processing all entities.
   *
   * @param dt Time of simulation step.
   */
  def processAll(dt: Float): Unit
}

/** Trait for with processor implementation for zero dimmensional
 * [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]s.
 *
 * This implementation is used for example in
 * [[com.specdevs.specgine.states.VoidEntitySystem `VoidEntitySystem`]].
 */
trait Processor0 extends ProcessorImplementation {
  /** Processing method called once every frame.
   *
   * @param dt Time of simulation step.
   */
  def process(dt: Float): Unit = {}

  def processAll(dt: Float): Unit = {
    process(dt)
  }
}

/** Trait for with processor implementation for one dimmensional
 * [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]s.
 *
 * To iterate, it uses `foreachEntity1` from
 * [[com.specdevs.specgine.states.EntityLoop1 `EntityLoop1`]].
 *
 * This implementation is used for example in
 * [[com.specdevs.specgine.states.SingleEntitySystem `SingleEntitySystem`]].
 */
trait Processor1 extends ProcessorImplementation with EntityLoop1 {
  /** Processing method called once every frame, before processing of entities start.
   *
   * @param dt Time of simulation step.
   */
  def begin(dt: Float): Unit = {}

  /** Processing method called once every frame, after processing of entities finish.
   *
   * @param dt Time of simulation step.
   */
  def end(dt: Float): Unit = {}

  /** Processing method called for every entity each frame.
   *
   * @param dt Time of simulation step.
   * @param e1 [[com.specdevs.specgine.states.Entity `Entity`]] to process.
   */
  def process(dt: Float, e1: Entity): Unit = {}

  def processAll(dt: Float): Unit = {
    begin(dt)
    foreachEntity1(e1 => process(dt, e1))
    end(dt)
  }
}

/** Trait for with processor implementation for two dimmensional
 * [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]s.
 *
 * To iterate, it uses `foreachEntity2` from
 * [[com.specdevs.specgine.states.EntityLoop2 `EntityLoop2`]].
 *
 * This implementation is used for example in
 * [[com.specdevs.specgine.states.PairEntitySystem `PairEntitySystem`]].
 */
trait Processor2 extends ProcessorImplementation with EntityLoop2 {
  /** Processing method called once every frame, before processing of entities start.
   *
   * @param dt Time of simulation step.
   */
  def begin(dt: Float): Unit = {}

  /** Processing method called once every frame, after processing of entities finish.
   *
   * @param dt Time of simulation step.
   */
  def end(dt: Float): Unit = {}

  /** Processing method called for every pair of entities each frame.
   *
   * @param dt Time of simulation step.
   * @param e1 [[com.specdevs.specgine.states.Entity `Entity`]] to process.
   * @param e2 [[com.specdevs.specgine.states.Entity `Entity`]] to process.
   */
  def process(dt: Float, e1: Entity, e2: Entity): Unit = {}

  def processAll(dt: Float): Unit = {
    begin(dt)
    foreachEntity2((e1, e2) => process(dt, e1, e2))
    end(dt)
  }
}
