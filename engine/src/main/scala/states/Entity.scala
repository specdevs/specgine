package com.specdevs.specgine.states

/** Class for entities in game.
 *
 * `Entity` according to its pure definition is represented only
 * by its `id`. They should be created via instace of
 * [[com.specdevs.specgine.states.World `World`]], which associates
 * them with sets of [[com.specdevs.specgine.states.Component `Component`]]s
 * they are built from. They can be seen as labels.
 *
 * @constructor Creates new `Entity` object with provided id.
 * @param id Id of `Entity`.
 * @see [[http://t-machine.org/index.php/2007/09/03/entity-systems-are-the-future-of-mmog-development-part-1/ ''
 *      t-machine.org web page'']], for introduction to Entity frameworks.
 */
case class Entity(val id: Long) extends AnyVal {
  /** Utility method used to create `Entity` successor.
   *
   * When method is used on `Entity` that lastly created, it
   * guarantees that the id will be uniqe, unless `Int` overflows
   * and comes back to `1`. Currenlty it is implemented by
   * increasing id by one, but users should not depend on it.
   *
   * @return New `Entity` instance.
   */
  def next: Entity = new Entity(id+1)
}
