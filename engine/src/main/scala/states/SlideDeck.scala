package com.specdevs.specgine.states

import com.specdevs.specgine.assets.{AbstractAssetManager,AssetManagerUser}

/** A container of [[com.specdevs.specgine.states.Slide `Slide`]]s.
 *
 * [[com.specdevs.specgine.states.Slide `Slide`]]s inside `SlideDeck` are
 * displayed one after another. It inherits most of its properties and
 * behaviour from currently displayed [[com.specdevs.specgine.states.Slide `Slide`]].
 * For example, it is `breakable` if there is no currently displayed
 * [[com.specdevs.specgine.states.Slide `Slide`]] or if current
 * [[com.specdevs.specgine.states.Slide `Slide`]] is breakable.
 *
 * @constructor Creates new deck of [[com.specdevs.specgine.states.Slide `Slide`]]s.
 * @param slides Vararg containing [[com.specdevs.specgine.states.Slide `Slide`]]s to group into deck.
 */
class SlideDeck(slides: Slide*) extends AssetManagerUser {
  private var toPlay: List[Slide] = slides.toList

  override def assetManager_=(manager: AbstractAssetManager): Unit = {
    super.assetManager = manager
    toPlay foreach (_.assetManager = manager)
  }

  /** Checks if you can stop viewing slide before it ends.
   *
   * SlideDeck is brakeable if it's empty or currently displayed slide is brakable.
   *
   * @return `true` if `SlideDeck` can be stopped.
   */
  def breakable: Boolean = {
    toPlay.isEmpty || toPlay.head.breakable
  }

  /** Checks whether `SlideDeck` is empty.
   *
   * @return `true` if `SlideDeck` contains no more
   *         [[com.specdevs.specgine.states.Slide `Slide`]]s to play.
   */
  def isEmpty: Boolean = {
    toPlay.isEmpty
  }

  /** Executes `process` method current [[com.specdevs.specgine.states.Slide `Slide`]].
   *
   * Does nothing if there is no current slide. Those that end during this
   * method call, are removed and new slide becomes active one.
   *
   * @param dt Time simulation step.
   */
  def process(dt: Float): Unit = {
    if (toPlay.nonEmpty) {
      toPlay.head.process(dt)
      if (toPlay.head.isFinished) {
        toPlay.head.doDispose()
        toPlay = toPlay.tail
      }
    }
  }

  /** Executes `render` method on current [[com.specdevs.specgine.states.Slide `Slide`]].
   *
   * Does nothing if there is no current slide.
   *
   * @param alpha Render interpolation between two processing steps.
   */
  def render(alpha: Float): Unit = {
    if (toPlay.nonEmpty) {
      toPlay.head.render(alpha)
    }
  }

  /** Passes `resize` request to all [[com.specdevs.specgine.states.Slide `Slide`]]s.
   *
   * @param x Screen width.
   * @param y Screen height.
   */
  def resize(x: Int, y: Int): Unit = {
    toPlay foreach (_.resize(x, y))
  }

  /** Helper method correctly calling `initialize`, used by
   * [[com.specdevs.specgine.states.SlideShowState `SlideShowState`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doInitialize(): Unit = {
    toPlay foreach (_.doInitialize())
  }

  /** Helper method correctly calling `create`, used by
   * [[com.specdevs.specgine.states.SlideShowState `SlideShowState`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doCreate(): Unit = {
    toPlay foreach (_.doCreate())
  }

  /** Helper method correctly calling `dispose`, used by
   * [[com.specdevs.specgine.states.SlideShowState `SlideShowState`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doDispose(): Unit = {
    toPlay foreach (_.doDispose())
    toPlay = Nil
    disposeAssets()
  }
}
