package com.specdevs.specgine.states

import scala.concurrent.Future

/** Trait mixed-in into classes that use [[com.specdevs.specgine.states.World `World`]]. */
trait WorldUser {
  private var theWorld: Option[World] = None

  /** Getter for instance of
   * [[com.specdevs.specgine.states.World `World`]].
   *
   * @return World instance.
   */
  def world: World = {
    if (theWorld.isEmpty) {
      throw new IllegalStateException("You haven't set world.")
    } else {
      theWorld.get
    }
  }

  /** Setter for instance of
   * [[com.specdevs.specgine.states.World `World`]].
   *
   * @param world World instance.
   */
  def world_=(world: World): Unit = {
    theWorld = Some(world)
  }

  /** Method used to create [[com.specdevs.specgine.states.Entity `Entity`]].
   *
   * @param components Set of [[com.specdevs.specgine.states.Component `Component`]]s for new
   *        [[com.specdevs.specgine.states.Entity `Entity`]].
   * @return [[com.specdevs.specgine.states.Entity `Entity`]] associated with
   *        given set of [[com.specdevs.specgine.states.Component `Component`]]s.
   */
  def createEntity(components: Component*): Entity = {
    world.createEntity(components: _*)
  }

  /** Method used to add new [[com.specdevs.specgine.states.Component `Component`]]s
   * to [[com.specdevs.specgine.states.Entity `Entity`]].
   *
   * @param e [[com.specdevs.specgine.states.Entity `Entity`]] to which components will be added.
   * @param components Set of [[com.specdevs.specgine.states.Component `Component`]]s to add.
   */
  def addComponents(e: Entity, components: Component*): Unit = {
    world.addComponents(e, components: _*)
  }

  /** Method used to filter out [[com.specdevs.specgine.states.Component `Component`]]s
   * from [[com.specdevs.specgine.states.Entity `Entity`]].
   *
   * @param e [[com.specdevs.specgine.states.Entity `Entity`]] to from which components will be filtered out.
   * @param rule A predicate saying if component should be removed.
   */
  def filterComponents(e: Entity, rule: Component => Boolean): Unit = {
    world.filterComponents(e, rule)
  }

  /** Method used to remove [[com.specdevs.specgine.states.Entity `Entity`]].
   *
   * @param e [[com.specdevs.specgine.states.Entity `Entity`]] to remove.
   */
  def removeEntity(e: Entity): Unit = {
    world.removeEntity(e)
  }

  /** Method used to send a message to receivers.
   *
   * @param message A message to be sent.
   */
  def send(message: Message): Unit = {
    world.send(message)
  }

  /** Method used to find replies for a given message.
   *
   * @param message A message to be replied to.
   * @return all replies for a given message.
   */
  def query(message: Message): Stream[Reply] = {
    world.query(message)
  }

  /** Method used to find replies for a given message asynchronolously.
   *
   * @param message A message to be replied to.
   * @return all replies for a given message.
   */
  def queryAsync(message: Message): Stream[Future[Reply]] = {
    world.queryAsync(message)
  }

  /** Method used to find first reply for a given message.
   *
   * @param message A message to be replied to.
   * @return first reply for a given message.
   */
  def queryFirst(message: Message): Option[Reply] = {
    world.queryFirst(message)
  }

  /** Method used to find first reply for a given message asynchronolously.
   *
   * @param message A message to be replied to.
   * @return first reply for a given message.
   */
  def queryFirstAsync(message: Message): Future[Reply] = {
    world.queryFirstAsync(message)
  }
}
