package com.specdevs.specgine.states

import com.specdevs.specgine.assets.AbstractAssetManager
import com.specdevs.specgine.core.{StateManager,AbstractConfigManager,State}
import com.specdevs.specgine.input.{AbstractInputManager,InputProxy}
import com.specdevs.specgine.animation.TweenManager

/** State responsible for game objects management using entity framework.
 *
 * This state is responsible for game object creation and management.
 * Each game object is modeled as [[com.specdevs.specgine.states.Entity `Entity`]],
 * and associated with it set of [[com.specdevs.specgine.states.Component `Component`]]s.
 * Each [[com.specdevs.specgine.states.Component `Component`]] repsresents some
 * kind of feature or characteristic of [[com.specdevs.specgine.states.Entity `Entity`]].
 * It can be position, velocity, body with associated model and animation set,
 * its collision bounding box, inventory, fuel, health or pretty anything.
 *
 * Processing of entities is done with
 * [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]s. They are responsible
 * for processing and rendering all instances of
 * [[com.specdevs.specgine.states.Entity `Entity`]].
 * One [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * can be responsible for moving game objects around, another for
 * rendering them. Systems can specify rules which must be met by
 * [[com.specdevs.specgine.states.Component `Component`]]s associated with
 * [[com.specdevs.specgine.states.Entity `Entity`]] to make it interesting
 * to them. For example, entity system that is moving objects around
 * might request to process only those objects, that have `Position` and
 * `Velocity`, and rendering system can request only objects with
 * visual representation. Because systems do not process nor keep track
 * of objects they are not interested in, processing is very effective.
 *
 * New instances of [[com.specdevs.specgine.states.Entity `Entity`]]
 * should be created inside `initialize` method using `createEntity`.
 * One also needs to add systems using `addSystem` method.
 *
 * `GameState` passes most of its logic to internally created instance of
 * [[com.specdevs.specgine.states.World `World`]] class. It keeps
 * track of entities and systems, and takes care to inform systems
 * about important changes to entities. It also distributes
 * all standard [[com.specdevs.specgine.core.State `State`]] events
 * like `initialize`, `create`, `enter`, `leave`, `dispose`, `resize`,
 * `render` and `process` down to each
 * [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]. Also
 * input handling is automatically propagated to them, if system extends
 * [[com.specdevs.specgine.input.InputReceiver `InputReceiver`]] or
 * [[com.specdevs.specgine.input.InputProxy `InputProxy`]], it will be automatically
 * registered in [[com.specdevs.specgine.input.AbstractInputManager `AbstractInputManager`]].
 *
 * @see [[http://t-machine.org/index.php/2007/09/03/entity-systems-are-the-future-of-mmog-development-part-1/ ''
 *      t-machine.org web page]], for introduction to Entity frameworks.
 */
trait GameState extends State with InputProxy with WorldUser {
  this.world = new World

  override def stateManager_=(manager: StateManager): Unit = {
    world.stateManager = manager
    super.stateManager = manager
  }

  override def configManager_=(manager: AbstractConfigManager): Unit = {
    world.configManager = manager
    super.configManager = manager
  }

  override def assetManager_=(manager: AbstractAssetManager): Unit = {
    world.assetManager = manager
    super.assetManager = manager
  }

  override def tweenManager_=(manager: TweenManager): Unit = {
    world.tweenManager = manager
    super.tweenManager = manager
  }

  /** Method adding new [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]] to internal `World`.
   *
   * This method adds new [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
   * and ensures, that it knows about all earlier registered instances of
   * [[com.specdevs.specgine.states.Entity `Entity`]], and all future ones.
   *
   * @param system [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]] to add.
   */
  protected def addSystem(system: EntitySystem): Unit = {
    world.addSystem(system)
  }

  def process(dt: Float): Unit = {
    world.process(dt)
  }

  def render(alpha: Float): Unit = {
    world.render(alpha)
  }

  override def resizeFrozen(x: Int, y: Int): Unit = {
    world.resizeFrozen(x, y)
  }

  def resize(x: Int, y: Int): Unit = {
    world.resize(x,y)
  }

  override def doInitialize(): Unit = {
    super.doInitialize()
    world.doInitialize()
  }

  def create(): Unit = {
    world.doCreate()
  }

  def enter(): Unit = {
    world.doEnter()
  }

  def leave(): Unit = {
    world.doLeave()
  }

  def dispose(): Unit = {
    world.doDispose()
  }

  def addReceivers(manager: AbstractInputManager): Unit = {
    world.addReceivers(manager)
  }
}
