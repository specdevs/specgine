package com.specdevs.specgine.states

import scala.collection.mutable.HashSet

/** A one dimmensional [[com.specdevs.specgine.states.EntitySystem `EntitySestem`]].
 *
 * This type of [[com.specdevs.specgine.states.EntitySystem `EntitySestem`]]
 * accepts entities and stores them in single entity container in protected
 * value `components`. This container should be modeled using
 * "`ComponentsSpec with ComponentsManager`". Logic to determine if
 * [[com.specdevs.specgine.states.Entity `Entity`]] is wanted, is passed directly
 * to implementation in [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]].
 *
 * `SingleEntitySystem` respects groups defined in
 * [[com.specdevs.specgine.states.ComponentsSpec `ComponentsSpec`]].
 * First, all unordered entities are processed, then entities
 * all entities ordered with increasing group value. For entities
 * with equal group number, processing order is undefined.
 *
 * `components` container is usually created using macro `withManager` from
 * [[com.specdevs.specgine.macros.states]] package.
 *
 * It is useful for example to move entities around or render them.
 *
 * @constructor Create new `SingleEntitySystem` with given priorities.
 * @param inputPriority Priority of input handling. Lower values comes first.
 * @param processPriority Processing priority. The smaller number, the sooner entity is processed.
 * @param renderPriority Rendering priority. The smaller number, the sooner entity is rendered.
 */
abstract class SingleEntitySystem(
    val inputPriority: Int = 0,
    val processPriority: Int = 0,
    val renderPriority: Int = 0)
    extends EntitySystem with Processor1 with Renderer1 {
  /** Container of entities and their associated components.
   *
   * It should mix its definition from [[com.specdevs.specgine.states.ComponentsSpec `ComponentsSpec`]]
   * with implementation of its functionality from
   * [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]].
   */
  protected val components: ComponentsManager with ComponentsSpec

  private val unordered = new HashSet[Entity]

  private val grouped = new EntitySetQueue

  def systemWantsEntity(cs: Set[Component]): Boolean = {
    components wants cs
  }

  def systemHasEntity(e: Entity): Boolean = {
    components has e
  }

  def addEntityToSystem(e: Entity, cs: Set[Component]): Unit = {
    if (components wants cs) {
      components.add(e, cs)
      components.group(e) match {
        case Some(group) => grouped(group) += e
        case None => unordered += e
      }
    }
    ()
  }

  def removeEntityFromSystem(e: Entity): Unit = {
    if (components has e) {
      components.remove(e)
      unordered -= e
      for (bag <- grouped) {
        bag.remove(e)
        ()
      }
    }
  }

  def foreachEntity1(f: Entity => Unit): Unit = {
    for (e <- unordered) {
      f(e)
    }
    for (bag <- grouped; e <- bag) {
      f(e)
    }
  }
}
