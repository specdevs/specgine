package com.specdevs.specgine.states

/** Exception class, used when there is no reply available.
 *
 * Raised when no [[com.specdevs.specgine.states.Responder `Responder`]] gave the reply.
 *
 * @constructor Creates new exception instance.
 */
class NoReplyAvailable extends Exception {
  override def toString: String = {
    "No reply available, looks like yr code isn't unassailable"
  }
}
