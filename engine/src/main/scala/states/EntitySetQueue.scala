package com.specdevs.specgine.states

import scala.collection.mutable.{ArrayBuffer,HashSet}

private[states] class EntitySetQueue {
  private class EntitySetNode(val set: HashSet[Entity], val priority: Int)

  private val data = ArrayBuffer.empty[EntitySetNode]

  def apply(priority: Int): HashSet[Entity] = {
    data find (_.priority == priority) match {
      case Some(node) => node.set
      case None => {
        val index = data.lastIndexWhere(_.priority <= priority)
        val set = new HashSet[Entity]
        data.insert(index+1, new EntitySetNode(set, priority))
        set
      }
    }
  }

  def foreach(f: HashSet[Entity] => Unit): Unit = {
    data foreach (node => f(node.set))
  }
}
