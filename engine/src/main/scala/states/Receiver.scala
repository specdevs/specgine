package com.specdevs.specgine.states

/** Trait used to signalize [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * that receives messages.
 */
trait Receiver {
  /** Method used to receive messages.
   *
   * @param message A message to receive.
   */
  def receive(message: Message): Unit
}
