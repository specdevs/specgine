package com.specdevs.specgine.states

/** Trait used for switching from one [[com.specdevs.specgine.states.AbstractMenuScreen `AbstractMenuScreen`]]
 * to another.
 */
trait MenuSwitcher {
  /** Changes current [[com.specdevs.specgine.states.AbstractMenuScreen `AbstractMenuScreen`]]
   * to the one with given name.
   *
   * @param name Name of the [[com.specdevs.specgine.states.AbstractMenuScreen `AbstractMenuScreen`]]
   *        screen to change to.
   */
  def changeMenu(name: String): Unit
}
