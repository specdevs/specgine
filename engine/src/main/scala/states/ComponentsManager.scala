package com.specdevs.specgine.states

/** Trait for components managment inside entity container.
 *
 * This trait is accompanied by
 * [[com.specdevs.specgine.states.ComponentsSpec `ComponentsSpec`]].
 * Normal use case include subclassing the
 * [[com.specdevs.specgine.states.ComponentsSpec `ComponentsSpec`]],
 * adding fields with maps from [[com.specdevs.specgine.states.Entity `Entity`]]
 * to some container of [[com.specdevs.specgine.states.Component `Component`]]s,
 * and them mixing-in `ComponentsManager` implementing functionality
 * to operate on all those fields at once.
 * It means, that well defined entity container is of type
 * "`ComponentsSpec with ComponentsManager`".
 *
 * All its methds should be implemented by containers that want to
 * manage set of components associated with entities. They are used for
 * example when new entities are added to
 * [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]].
 *
 * We provide a way to automatically generate `ComponentsManager`
 * based on [[com.specdevs.specgine.states.ComponentsSpec `ComponentsSpec`]]
 * using `withManager` macro from [[com.specdevs.specgine.macros.states]]
 * package. It is recommended way to create `ComponentsManager`s,
 * but not only way to do so.
 */
trait ComponentsManager {
  /** Describes whether a set of [[com.specdevs.specgine.states.Component `Component`]]s is interesting to class.
   *
   * This method should check if [[com.specdevs.specgine.states.Entity `Entity`]] is built from a set of
   * [[com.specdevs.specgine.states.Component `Component`]]s
   * that are interesting to given class, for example if it contains all
   * needed [[com.specdevs.specgine.states.Component `Component`]]s.
   *
   * @param cs Set of [[com.specdevs.specgine.states.Component `Component`]]s
   *        associated with [[com.specdevs.specgine.states.Entity `Entity`]].
   * @return `true` if set of [[com.specdevs.specgine.states.Component `Component`]]s is interesting.
   */
  def wants(cs: Set[Component]): Boolean

  /** Adds an [[com.specdevs.specgine.states.Entity `Entity`]] associated with set of
   * [[com.specdevs.specgine.states.Component `Component`]]s.
   *
   * @param e [[com.specdevs.specgine.states.Entity `Entity`]] to be added.
   * @param cs Set of [[com.specdevs.specgine.states.Component `Component`]]s associated with it.
   */
  def add(e: Entity, cs: Set[Component]): Unit

  /** Describes whether a class already has given [[com.specdevs.specgine.states.Entity `Entity`]].
   *
   * @param e [[com.specdevs.specgine.states.Entity `Entity`]] to be checked.
   * @return `true` if entity was regustered.
   */
  def has(e: Entity): Boolean

  /** Removes an [[com.specdevs.specgine.states.Entity `Entity`]] from class.
   *
   * @param e [[com.specdevs.specgine.states.Entity `Entity`]] to be removed.
   */
  def remove(e: Entity): Unit
}
