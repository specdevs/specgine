package com.specdevs.specgine.input.gdx

import com.specdevs.specgine.input._

import scala.collection.mutable.ArrayBuffer

private[gdx] class InputReceiverQueue[A <: InputReceiver] {
  private class ReceiverNode(val receiver: A, val priority: Int)

  private val data = ArrayBuffer.empty[ReceiverNode]

  def enqueue(receiver: A, priority: Int): Unit = {
    val index = data.lastIndexWhere(_.priority <= priority)
    data.insert(index+1, new ReceiverNode(receiver, priority))
  }

  def clear(): Unit = {
    data.clear()
  }

  def exists(f: A => Boolean): Boolean = {
    data exists (node => f(node.receiver))
  }
}
