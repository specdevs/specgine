package com.specdevs.specgine.input

/** Parent trait of all types of input receivers. */
trait InputReceiver
