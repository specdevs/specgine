package com.specdevs.specgine.input

// $COVERAGE-OFF$This trait provides no functionality to test, it is only for callbacks.

/** Trait receiving pointer events. */
trait PointerReceiver extends InputReceiver {
  /** Called when mouse button is pressed or finger placed on screen.
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @param pointer Number of finger, on mouse always 1.
   * @param button Number of button, on touchscreen always 0.
   * @return `true` if event is consumed.
   */
  def touchDown(x: Int, y: Int, pointer: Int, button: Int): Boolean = false

  /** Called when mouse button is released or finger lifted from screen.
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @param pointer Number of finger, on mouse always 1.
   * @param button Number of button, on touchscreen always 0.
   * @return `true` if event is consumed.
   */
  def touchUp(x: Int, y: Int, pointer: Int, button: Int): Boolean = false

  /** Called when mouse or finger is dragged.
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @param pointer Number of finger, on mouse always 1.
   * @return `true` if event is consumed.
   */
  def touchDragged(x: Int, y: Int, pointer: Int): Boolean = false

  /** Called when mouse is moved.
   *
   * Never called for touch-screen.
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @return `true` if event is consumed.
   */
  def mouseMoved(x: Int, y: Int): Boolean = false

  /** Called when mouse scroll is used.
   *
   * Never called for touch-screen.
   *
   * @param amount Amount of scroll.
   * @return `true` if event is consumed.
   */
  def scrolled(amount: Int): Boolean = false
}
