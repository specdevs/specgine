package com.specdevs.specgine.input

/** Trait representing all implementations of input manager.
 *
 * Implementation should guarantee, thet when `addReceiver` is called,
 * priority and receiver type is taken into account. Receivers should be
 * processed in following order:
 *   - [[com.specdevs.specgine.input.BackReceiver `BackReceiver`]],
 *   - [[com.specdevs.specgine.input.KeyReceiver `KeyReceiver`]],
 *   - [[com.specdevs.specgine.input.PointerReceiver `PointerReceiver`]],
 *   - [[com.specdevs.specgine.input.GestureReceiver `GestureReceiver`]].
 *
 * Inside same group, order of receivers is dictated by their priority. Those
 * with lower value of priority parameter should come first. If priorities are
 * equal, order of adding receivers should decide.
 *
 * In all implementations `reset` method should bring input manager to state
 * equal to how it was at startup.
 */
trait AbstractInputManager {
  /** Method adding receiver to input manager.
   *
   * @param what Input receiver to register.
   * @param priority Priority of receiver, lower values comes first.
   */
  def addReceiver(what: InputReceiver, priority: Int = 0): Unit

  /** Method removing all receivers from input manager. */
  def reset(): Unit
}
