package com.specdevs.specgine.input

// $COVERAGE-OFF$This trait provides no functionality to test, it is only for callbacks.

/** Trait receiving gesture events. */
trait GestureReceiver extends InputReceiver {
  /** Called when user drags pointer and then lifts it.
   *
   * Fling is sometimes called "swipe". It is quick gesture used to turn pages
   * or move content quickly.
   *
   * @param velX Y velocity in pixels per second of finger.
   * @param velY X velocity in pixels per second of finger.
   * @param button Button used for fling, on touchscreen always 0.
   * @return `true` if event is consumed.
   */
  def fling(velX: Float, velY: Float, button: Int): Boolean = false

  /** Called when mouse button is pressed or finger placed on screen.
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @param pointer Number of finger, on mouse always 1.
   * @param button Number of button, on touchscreen always 0.
   * @return `true` if event is consumed.
   */
  def touchDown(x: Float, y: Float, pointer: Int, button: Int): Boolean = false

  /** Called when user holds mouse button or finger in place for some time.
   *
   * Exact time depends on implementation of
   * [[com.specdevs.specgine.input.AbstractInputManager `AbstractInputManager`]].
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @return `true` if event is consumed.
   */
  def longPress(x: Float, y: Float): Boolean = false

  /** Called when user quickly presses and releases mouse button or finger without moving it outside of small area.
   *
   * It can be also used to register double-clicks or double-taps.
   *
   * Exact time and area depends on implementation of
   * [[com.specdevs.specgine.input.AbstractInputManager `AbstractInputManager`]].
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @param count Count of taps.
   * @param button Number of button, on touchscreen always 0.
   * @return `true` if event is consumed.
   */
  def tap(x: Float, y: Float, count: Int, button: Int): Boolean = false

  /** Called when user holds mouse button or finger and moves it without raising.
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @param dx Amount of pan in x direction.
   * @param dy Amount of pan in y direction.
   * @return `true` if event is consumed.
   */
  def pan(x: Float, y: Float, dx: Float, dy: Float): Boolean = false

  /** Called when user was panning and stopped.
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @param pointer Number of finger, on mouse always 1.
   * @param button Number of button, on touchscreen always 0.
   * @return `true` if event is consumed.
   */
  def panStop(x: Float, y: Float, pointer: Int, button: Int): Boolean = false

  /** Called when user places two fingers on screen and changes distance between them.
   *
   * @param initialDistance Distance between fingers when second finger
   *        touched the screen.
   * @param distance Current distance between fingers.
   * @return `true` if event is consumed.
   */
  def zoom(initialDistance: Float, distance: Float): Boolean = false

  /** Called when two fingers are placed on screen and moved around.
   *
   * It is universal two-finger gesture, that can be used to implement
   * rotations or mixed zoom/rotate/pan events.
   *
   * @param initialP1 Coordinates of first finger when it was placed.
   * @param initialP2 Coordinates of second finger when it was placed.
   * @param currentP1 Current coordinates of first finger.
   * @param currentP2 Current coordinates of second finger.
   * @return `true` if event is consumed.
   */
  def pinch(
    initialP1: (Float, Float),
    initialP2: (Float, Float),
    currentP1: (Float, Float),
    currentP2: (Float, Float)
  ): Boolean = false
}
