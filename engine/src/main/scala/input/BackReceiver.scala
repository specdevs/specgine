package com.specdevs.specgine.input

// $COVERAGE-OFF$This trait provides no functionality to test, it is only for callbacks.

/** Trait receiving back button events. */
trait BackReceiver extends InputReceiver {
  /** Called when back button is pressed down.
   *
   * @return `true` if event is consumed.
   */
  def backDown(): Boolean = false

  /** Called when back button is released.
   *
   * @return `true` if event is consumed.
   */
  def backUp(): Boolean = false
}

