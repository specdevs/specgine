package com.specdevs.specgine.input

// $COVERAGE-OFF$This trait provides no functionality to test, it is only for callbacks.

/** Trait receiving keyboard events. */
trait KeyReceiver extends InputReceiver {
  /** Called when key is pressed.
   *
   * @param keycode Key-code of button.
   * @return `true` if event is consumed.
   */
  def keyDown(keycode: Int): Boolean = false

  /** Called when key is released.
   *
   * @param keycode Key-code of button.
   * @return `true` if event is consumed.
   */
  def keyUp(keycode: Int): Boolean = false

  /** Called when character is typed.
   *
   * @param char Typed character.
   * @return `true` if event is consumed.
   */
  def keyTyped(char: Char): Boolean = false
}
