package com.specdevs.specgine.input

/** Trait implementing proxying and multiplexing of input handling.
 *
 * When [[com.specdevs.specgine.core.StateManager `StateManager`]] encounters
 * state implementing this trait, it calls `addReceivers` method of state with
 * [[com.specdevs.specgine.input.AbstractInputManager `AbstractInputManager`]]
 * as its parameter. It allows to implement easy input multiplexing, where
 * [[com.specdevs.specgine.core.State `State`]] uses received input manager to
 * add multiple [[com.specdevs.specgine.input.InputReceiver `InputReceiver`]]s.
 */
trait InputProxy {
  /** Method that adds [[com.specdevs.specgine.input.InputReceiver `InputReceiver`]]s
   * to input manager.
   *
   * @param manager Currently active implementation of input manager.
   */
  def addReceivers(manager: AbstractInputManager): Unit
}
