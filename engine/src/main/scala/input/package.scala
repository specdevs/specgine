package com.specdevs.specgine

/** Package providing input functionality in SpecGine.
 *
 * Classes in this package are resposible for input handling. At hearth of
 * input subsystem there is
 * [[com.specdevs.specgine.input.AbstractInputManager `AbstractInputManager`]]
 * and its LibGDX based implementation in [[com.specdevs.specgine.input.gdx]]
 * package. This class is created and managed by implementation of
 * [[com.specdevs.specgine.core.AbstractGame `AbstractGame`]]. When
 * [[com.specdevs.specgine.core.StateManager `StateManager`]] changes states,
 * it calls method `reset` on input manager and then checks if it implements
 * [[com.specdevs.specgine.input.InputProxy `InputProxy`]] or
 * [[com.specdevs.specgine.input.InputReceiver `InputReceiver`]], i.e. one of:
 *   - [[com.specdevs.specgine.input.BackReceiver `BackReceiver`]], to capture
 *     back button on Android devices.
 *   - [[com.specdevs.specgine.input.KeyReceiver `KeyReceiver`]], to capture
 *     keyboard events.
 *   - [[com.specdevs.specgine.input.PointerReceiver `PointerReceiver`]], to
 *     capture simple pointer devices like mouse or touch-screen.
 *   - [[com.specdevs.specgine.input.GestureReceiver `GestureReceiver`]], to
 *     capture complex gestures like pan, fling or zoom.
 *
 * When [[com.specdevs.specgine.core.StateManager `StateManager`]] enters
 * state that implements some receiver, it calls `addReceiver` method of input
 * manager with receiver as argument and optional priority. If state implements
 * [[com.specdevs.specgine.input.InputProxy `InputProxy`]], its `addReceivers`
 * method is called, with input manager as parameter. Such approach allows to
 * register multiple independent handlers, without user intervention other
 * than implementing correct interfaces.
 *
 * Each [[com.specdevs.specgine.input.InputReceiver `InputReceiver`]] contains
 * methods, that are called when event occurs. If method returns `true`, the
 * event is consumed. If it returns `false`, another receiver will be tried.
 * Receivers are ordered according to priority set during their registering in
 * input manager. For different types of receivers, all receivers of given
 * type are processed in order: all back buttons, keys, pointers and gestures.
 *
 * @note Currently receivers are based on LibGDX features, and their options
 *       maps directly to their LibGDX implementation. This is can change in
 *       future releases of SpecGine.
 */
package object input
