package com.specdevs.specgine.core

private[core] sealed trait StackCommand

private[core] case class PushStackCommand(val name: String) extends StackCommand

private[core] case class PopStackCommand() extends StackCommand

private[core] case class ChangeStackCommand(val name: String) extends StackCommand

private[core] case class QuitStackCommand() extends StackCommand
