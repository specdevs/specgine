package com.specdevs.specgine.core.gdx

import com.specdevs.specgine.core._
import com.specdevs.specgine.input.gdx.InputManager
import com.specdevs.specgine.assets.gdx.AssetManager

import com.badlogic.gdx.{ApplicationListener => GdxApplicationListener}
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.profiling.{GLProfiler => GdxGLProfiler}

import com.badlogic.gdx.utils.TimeUtils.{nanoTime => gdxNanoTime}
import com.badlogic.gdx.math.{FloatCounter => GdxFloatCounter}

import scala.math.{min,max}

/** Abstract main game class based on LibGDX.
 *
 * It is LibGDX `ApplicationListener`, user of SpecGine is currently
 * responsible for creating configured LibGDX application entry point and
 * passing instance of `Game` as `ApplicationListener`.
 *
 * There is a possibility of thorough profiling, both internally -
 * owing to [[com.specdevs.specgine.core.ProfilingData `ProfilingData`]],
 * and externally, by using Java Mission Control (desktop) or DDMS (Android).
 * Note that in order to be able to run the JMC it is necessary to define the environement variable:
 * `SBT_OPTS="-XX:+UnlockCommercialFeatures -XX:+FlightRecorder"`.
 * As for Android, you should run 'monitor' which is one of the available android tools.
 *
 * @param name Unique name of game, preferably package name.
 * @param dt Constant simulation step.
 * @param enableProfiling Specifies whether profiler should be turned on.
 * @constructor Creates instance of [[com.specdevs.specgine.core.AbstractGame `AbstractGame`]]
 *              that is also LibGDX `ApplicationListener`.
 * @see [[https://github.com/libgdx/libgdx/wiki/Starter-classes-%26-configuration ''LibGDX wiki'']],
 *      for starter classes and configuration.
 */
abstract class Game(name: String,
                    dt: Float = 0.01f,
                    enableProfiling: Boolean = false) extends GdxApplicationListener with AbstractGame {
  private lazy val assetManager = new AssetManager

  private lazy val configManager = new ConfigManager(name+".settings")

  private lazy val inputManager = new InputManager

  private lazy val stateManager = new StateManager

  private val defaultLoader: LoadingScreen = new DefaultLoader

  private var userLoader: LoadingScreen = defaultLoader

  private var accumulator = 0f

  private var profilingData: Option[ProfilingData] = None

  private val nano2seconds = 1f / 1000000000.0f

  private val animationCounter = new GdxFloatCounter(1)

  protected def addState(s: State, name: String, parentName: String = "", default: Boolean = false): Unit = {
    stateManager.addState(s, name, parentName, default)
  }

  protected def addGroup(group: StateGroup, name: String, parentName: String = ""): Unit = {
    stateManager.addGroup(group, name, parentName)
  }

  protected def setLoadingScreen(loader: LoadingScreen): Unit = {
    userLoader = loader
  }

  /** LibGDX `ApplicationListener` create method implementation.
   *
   * Creates all needed managers and initializes them.
   */
  def create(): Unit = {
    stateManager.assetManager = assetManager
    stateManager.configManager = configManager
    stateManager.inputManager = inputManager
    stateManager.defaultLoader = defaultLoader
    stateManager.initialize()
    stateManager.resize(Gdx.graphics.getWidth, Gdx.graphics.getHeight)
    initialize()
    stateManager.userLoader = userLoader
    stateManager.create()
    configManager.store()
    if (enableProfiling) {
      GdxGLProfiler.enable()
      stateManager.registerProfilerGetter(Unit=>profilingData)
    }
  }

  private def profileWhileRendering(): Unit = {
    val elapsed = Gdx.graphics.getDeltaTime
    val skippedFrames = max(0.0f, (elapsed-0.25f)/dt)
    accumulator += min(elapsed, 0.25f)

    while (accumulator >= dt) {
      val before = gdxNanoTime
      stateManager.process(dt)
      val after = gdxNanoTime
      animationCounter.put((after-before)*nano2seconds)
      accumulator -= dt
    }

    val beforeRendering = gdxNanoTime
    stateManager.render(accumulator/dt)
    val renderingTime = (gdxNanoTime-beforeRendering)*nano2seconds

    if (profilingData.isEmpty) {
      profilingData = Some(ProfilingData())
    }
    val profData = profilingData.get
    profData.calls = GdxGLProfiler.calls
    profData.drawCalls = GdxGLProfiler.drawCalls
    profData.textureBindings = GdxGLProfiler.textureBindings
    profData.shaderSwitches = GdxGLProfiler.shaderSwitches
    profData.minVertex = GdxGLProfiler.vertexCount.min
    profData.maxVertex = GdxGLProfiler.vertexCount.max
    profData.countVertex = GdxGLProfiler.vertexCount.count
    profData.totalVertex = GdxGLProfiler.vertexCount.total
    profData.fps = Gdx.graphics.getFramesPerSecond()
    profData.skippedFrames = skippedFrames
    profData.avgAnimFrameTime = animationCounter.average
    profData.renderingTime = renderingTime
    profData.animFrameCount = animationCounter.count
    profData.deltaTime = elapsed
    if (skippedFrames>=1) profData.stable = false
    GdxGLProfiler.reset()
    animationCounter.reset()
  }

  /** LibGDX `ApplicationListener` render method implementation.
   *
   * Calls `process` and `render` methods of
   * [[com.specdevs.specgine.core.StateManager `StateManager`]].
   * Quits game if needed. If `enableProfiling` is set to `true`,
   * it measures `OpenGL` and FPS statistics.
   */
  def render(): Unit = {
    if (stateManager.shouldQuit) {
      Gdx.app.exit()
    } else {
      if (enableProfiling) {
        profileWhileRendering()
      } else {
        accumulator += min(Gdx.graphics.getDeltaTime, 0.25f)
        while (accumulator >= dt) {
          stateManager.process(dt)
          accumulator -= dt
        }
        stateManager.render(accumulator/dt)
      }
    }
  }

  /** LibGDX `ApplicationListener` resize method implementation.
   *
   * Calls `resize` method of
   * [[com.specdevs.specgine.core.StateManager `StateManager`]].
   */
  def resize(x: Int, y: Int): Unit = {
    stateManager.resize(x, y)
  }

  /** LibGDX `ApplicationListener` pause method implementation.
   *
   * Currently does nothing.
   */
  def pause(): Unit = {}

  /** LibGDX `ApplicationListener` resume method implementation.
   *
   * Currently does nothing.
   */
  def resume(): Unit = {}

  /** LibGDX `ApplicationListener` dispose method implementation.
   *
   * Disposes all managers. Ensures, that configuration was stored before next
   * run.
   */
  def dispose(): Unit = {
    configManager.store()
    stateManager.dispose()
    assetManager.dispose()
    if (enableProfiling) {
      GdxGLProfiler.disable()
    }
  }

  /** Implementation of function which returns profiling data.
   *
   * Returns `None` when `enableProfiling` is set to false.
   */
  def getProfilingData: Option[ProfilingData] = {
    profilingData
  }
}
