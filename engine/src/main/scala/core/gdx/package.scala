package com.specdevs.specgine.core

/** Package implementing core classes using LibGDX framework. */
package object gdx
