package com.specdevs.specgine.core.gdx

import com.specdevs.specgine.core._

import com.badlogic.gdx.{Preferences => GdxPreferences}
import com.badlogic.gdx.Gdx

/** Configuration manager based on LibGDX.
 *
 * Configuration manager implemented using LibGDX `Preferences`. Stores as
 * `SharedPreferences` on Android, `NSDictionary` on iOS and in `xml` file in
 * standard Java preferences directory.
 *
 * @param name Name of preferences, should be unique to application.
 * @constructor Creates instance of [[com.specdevs.specgine.core.AbstractConfigManager `AbstractConfigManager`]].
 */
class ConfigManager(name: String) extends AbstractConfigManager {
  private val preferences: GdxPreferences = Gdx.app.getPreferences(name)

  private var needsStore: Boolean = false

  def get(key: String): Option[Config] = {
    if (preferences.contains(key+"_b")) {
      Some(BooleanConfig(preferences.getBoolean(key+"_b")))
    } else if (preferences.contains(key+"_f")) {
      Some(FloatConfig(preferences.getFloat(key+"_f")))
    } else if (preferences.contains(key+"_i")) {
      Some(IntConfig(preferences.getInteger(key+"_i")))
    } else if (preferences.contains(key+"_l")) {
      Some(LongConfig(preferences.getLong(key+"_l")))
    } else if (preferences.contains(key+"_s")) {
      Some(StringConfig(preferences.getString(key+"_s")))
    } else {
      None
    }
  }

  def set(key: String, value: Config): Unit = {
    needsStore = true
    value match {
      case BooleanConfig(theValue) => {
        preferences.putBoolean(key+"_b", theValue)
        ()
      }
      case FloatConfig(theValue) => {
        preferences.putFloat(key+"_f", theValue)
        ()
      }
      case IntConfig(theValue) => {
        preferences.putInteger(key+"_i", theValue)
        ()
      }
      case LongConfig(theValue) => {
        preferences.putLong(key+"_l", theValue)
        ()
      }
      case StringConfig(theValue) => {
        preferences.putString(key+"_s", theValue)
        ()
      }
      case _ => throw new IllegalArgumentException("Unsupported asset type.")
    }
  }

  def store(): Unit = {
    if (needsStore) {
      preferences.flush()
      needsStore = false
    }
  }
}
