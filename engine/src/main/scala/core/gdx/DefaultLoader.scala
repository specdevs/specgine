package com.specdevs.specgine.core.gdx

import com.specdevs.specgine.core._

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.{GL20 => GdxGL20}
import com.badlogic.gdx.graphics.{OrthographicCamera => GdxOrthographicCamera}
import com.badlogic.gdx.graphics.glutils.{ShapeRenderer => GdxShapeRenderer}

/** Default loading screen implementation based on LibGDX.
 *
 * Simple and fast loading screen implementation based on LibGDX, without
 * requirements for custom assets. Illustrates two statistics about loading:
 *   - loading process (draws fraction of circle proportional to loading
 *     completness),
 *   - loading time (circle fraction rotates with constant velocity).
 *
 * Because it does not require custom resources, it is used to load resources
 * needed by other loaders.
 *
 * @constructor Creates instance of [[com.specdevs.specgine.core.LoadingScreen `LoadingScreen`]].
 */
class DefaultLoader extends LoadingScreen {
  private var oldt = 0f

  private var t = 0f

  private lazy val camera = new GdxOrthographicCamera

  private lazy val renderer = new GdxShapeRenderer

  def reset(): Unit = {
    Gdx.gl20.glClearColor(0f, 0f, 0f, 0f)
    oldt = 0f
    t = 0f
  }

  def resize(x: Int, y: Int): Unit = {
    val aspect = x.toFloat/y
    camera.setToOrtho(false, 2f*aspect, 2f)
    camera.translate(-aspect, -1f)
  }

  def processLoading(dt: Float): Unit = {
    oldt = t
    t += dt
  }

  def renderLoading(alpha: Float, progress: Float): Unit = {
    val rotation = 90-(t*alpha+oldt*(1-alpha))*30f
    val fill = -360*progress
    val segments = 1024
    camera.update()
    renderer.setProjectionMatrix(camera.combined)
    Gdx.gl20.glClear(GdxGL20.GL_COLOR_BUFFER_BIT)
    renderer.begin(GdxShapeRenderer.ShapeType.Filled)
    renderer.setColor(1f, 1f, 1f, 1f)
    renderer.arc(0f, 0f, 0.3f, rotation, fill, segments)
    renderer.end()
  }
}
