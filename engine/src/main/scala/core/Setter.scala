package com.specdevs.specgine.core

/** Trait used for storing values.
 *
 * @tparam A Type of stored values.
 */
trait Setter[A] {
  /** Method storing value.
   *
   * @param key Key pointing to value.
   * @param value Value to store.
   */
  def set(key: String, value: A): Unit
}
