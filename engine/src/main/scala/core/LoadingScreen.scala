package com.specdevs.specgine.core

/** Trait representing loading screen. */
trait LoadingScreen {
  /** Method called just before new loading screen. */
  def reset(): Unit

  /** Method called when size of screen changes.
   *
   * It is guaranteed to be called at least once, before first call to
   * `processLoading` or `renderLoading`.
   *
   * @param x Screen width.
   * @param y Screen height.
   */
  def resize(x: Int, y: Int): Unit

  /** Method processing loading animation.
   *
   * @param dt Time of simulation step.
   */
  def processLoading(dt: Float): Unit

  /** Method rendering loading screen.
   *
   * @param alpha Render interpolation between two last simulation steps (0..1).
   * @param progress Percentage of loaded resources (0..1).
   */
  def renderLoading(alpha: Float, progress: Float): Unit
}
