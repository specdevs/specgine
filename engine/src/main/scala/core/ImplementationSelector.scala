package com.specdevs.specgine.core

/** Selector of implementation for type `T`.
 *
 * @tparam T Type to validate.
 */
trait ImplementationSelector[+T] {
  /** Check if implementation is valid.
   *
   * @return True if implementation can be used, false otherwise.
   */
  def isValid: Boolean

  /** Get instance of `T` with implementation.
   *
   * @note This method may fail if implementation is not valid.
   * @return Instance of `T` implemented with selector.
   */
  def getInstance: T
}
