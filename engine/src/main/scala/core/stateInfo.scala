package com.specdevs.specgine.core

/** Trait used for values stored by [[com.specdevs.specgine.core.StateManager `StateManager`]]. */
trait StateInfo extends Any

/** `Boolean` implementation for [[com.specdevs.specgine.core.StateInfo `StateInfo`]].
 *
 * @param self `Boolean` value.
 */
case class BooleanStateInfo(val self: Boolean) extends AnyVal with StateInfo

/** `Float` implementation for [[com.specdevs.specgine.core.StateInfo `StateInfo`]].
 *
 * @param self `Float` value.
 */
case class FloatStateInfo(val self: Float) extends AnyVal with StateInfo

/** `Int` implementation for [[com.specdevs.specgine.core.StateInfo `StateInfo`]].
 *
 * @param self `Int` value.
 */
case class IntStateInfo(val self: Int) extends AnyVal with StateInfo

/** `Long` implementation for [[com.specdevs.specgine.core.StateInfo `StateInfo`]].
 *
 * @param self `Long` value.
 */
case class LongStateInfo(val self: Long) extends AnyVal with StateInfo

/** `String` implementation for [[com.specdevs.specgine.core.StateInfo `StateInfo`]].
 *
 * @param self `String` value.
 */
case class StringStateInfo(val self: String) extends AnyVal with StateInfo
