package com.specdevs.specgine

/** Package providing core functionality of SpecGine.
 *
 * Classes in this package can be divided into following groups.
 *
 * == Generic Game Code ==
 *
 * This includes the
 * [[com.specdevs.specgine.core.AbstractGame `AbstractGame`]] itself, class
 * that is entry point of every game made with SpecGine. It does not provide
 * implementation though, example of which can be found in framework
 * sub-package, [[com.specdevs.specgine.core.gdx]].
 * [[com.specdevs.specgine.core.AbstractGame `AbstractGame`]] class is
 * responsible for creating and setting managers for various parts of game
 * engine, and calling processing routines.
 *
 * Every manager in SpecGame is represented by two classes: `...Manager` and
 * `...ManagerUser`. Managers are classes that implement various functionality,
 * and manager users are traits simplifying access. Mixing in `...ManagerUser`
 * trait requires that given manager is set before usage. If `...Manager` is
 * responsible for providing access to some resources, it inherits from one or
 * more of traits:
 *   - [[com.specdevs.specgine.core.Getter `Getter`]],
 *   - [[com.specdevs.specgine.core.Setter `Setter`]],
 *   - [[com.specdevs.specgine.core.GetterTarget `GetterTarget`]],
 *   - [[com.specdevs.specgine.core.SetterTarget `SetterTarget`]].
 *
 * Method to get and set values in managers are available trough
 * [[com.specdevs.specgine.core.GetSetUser `GetSetUser`]]. This trait provides
 * methods relying on implicit values to pick right manager and correct
 * [[com.specdevs.specgine.core.Provider `Provider`]], class that allows to
 * convert between `A` and `B` if there exists some `C` such, that `C(B) <: A`.
 * Approach we took allows us to efficiently store potentially unrelated values,
 * like assets, in common storage and without using reflection.
 *
 * == State Management ==
 *
 * Games in LibGDX are made of one or more
 * [[com.specdevs.specgine.core.State `State`]]s, potentially grouped into
 * [[com.specdevs.specgine.core.StateGroup `StateGroup`]]s.
 * [[com.specdevs.specgine.core.StateGroup `StateGroup`]] can contain other
 * [[com.specdevs.specgine.core.StateGroup `StateGroup`]]s or
 * [[com.specdevs.specgine.core.State `State`]]s. If
 * [[com.specdevs.specgine.core.StateGroup `StateGroup`]]'s parent name is
 * same as its own name, it is considered root of separate state tree. There
 * is one default root group, called `""` (empty string), that is default
 * parent for every new [[com.specdevs.specgine.core.State `State`]] or
 * [[com.specdevs.specgine.core.StateGroup `StateGroup`]].
 *
 * [[com.specdevs.specgine.core.StateManager `StateManager`]] is responsible
 * for distributing proper calls from
 * [[com.specdevs.specgine.core.AbstractGame `AbstractGame`]] down to
 * [[com.specdevs.specgine.core.State `State`]].
 * [[com.specdevs.specgine.core.StateManagerUser `StateManagerUser`]] is trait
 * that exposes functionality related to state transitions. Internally
 * [[com.specdevs.specgine.core.StateManager `StateManager`]] is implemented
 * as stack of [[com.specdevs.specgine.core.State `State`]]s, with `push` and
 * `pop` (and optimized `change`) operations.
 * [[com.specdevs.specgine.core.State `State`]] on top of stack is the one that
 * is currently active, all states below are frozen.
 *
 * Each [[com.specdevs.specgine.core.StateGroup `StateGroup`]] can have its own
 * initialization and de-initialization code, and
 * [[com.specdevs.specgine.core.StateManager `StateManager`]] ensures, that
 * all groups containing active state, directly or trough other groups, were
 * also activated or deactivated when needed. Such functionality can be used
 * to easily and automatically manage resources shared between states (like
 * bitmaps or fonts), without need to manually decide when they are no longer
 * needed.
 *
 * All [[com.specdevs.specgine.core.StateGroup `StateGroup`]]s can also store
 * other data shared between states, and this is only supported way to
 * exchange information between them.
 * [[com.specdevs.specgine.core.StateManager `StateManager`]] implements
 * [[com.specdevs.specgine.core.GetterTarget `GetterTarget`]] and
 * [[com.specdevs.specgine.core.SetterTarget `SetterTarget`]], which allows to
 * set or get [[com.specdevs.specgine.core.StateInfo `StateInfo`]], universal
 * trait aimed to be extended by user as he pleases. There are few default
 * implementations of [[com.specdevs.specgine.core.StateInfo `StateInfo`]]
 * with example implicit [[com.specdevs.specgine.core.Provider `Provider`]]s
 * to get you started. Such data while shared, is not persistent between game
 * runs. Targets for getter and setter are names of groups.
 *
 * == Configuration Management ==
 *
 * [[com.specdevs.specgine.core.Config `Config`]] is similar to
 * [[com.specdevs.specgine.core.StateInfo `StateInfo`]], but is persistent
 * between runs, and there is only one global target for it. It is managed by
 * [[com.specdevs.specgine.core.AbstractConfigManager `AbstractConfigManager`]]
 * and accessible trough
 * [[com.specdevs.specgine.core.ConfigManagerUser `ConfigManagerUser`]].
 * Default implementation based on LibGDX is present in
 * [[com.specdevs.specgine.core.gdx]].
 *
 * == Implementation Selection ==
 *
 * If multiple implementations for single class are present, one can use
 * `selectImplementation` method from `core` package. It takes instances of
 * [[com.specdevs.specgine.core.ImplementationSelector `ImplementationSelector`]],
 * abstract class implemented by [[com.specdevs.specgine.core.EagerSelector `EagerSelector`]]
 * and [[com.specdevs.specgine.core.LazySelector `LazySelector`]]. Eager variant should be
 * mixed-in into implementation and is easier to use or override without creating custom
 * helper classes. Lazy variant is used as wrapper and allows to select implementations
 * when some variants cannot be created on some platforms.
 *
 * In [[com.specdevs.specgine.core.gdx `gdx`]] subpackage there are implementations of
 * selectors that restrict classes to certain platforms. They are:
 *   - [[com.specdevs.specgine.core.gdx.DesktopImplementation `DesktopImplementation`]] eager mixin and
 *     [[com.specdevs.specgine.core.gdx.IfDesktop `IfDesktop`]] lazy class,
 *   - [[com.specdevs.specgine.core.gdx.AndroidImplementation `AndroidImplementation`]] eager mixin and
 *     [[com.specdevs.specgine.core.gdx.IfAndroid `IfAndroid`]] lazy class,
 *   - [[com.specdevs.specgine.core.gdx.IOSImplementation `IOSImplementation`]] eager mixing and
 *     [[com.specdevs.specgine.core.gdx.IfIOS `IfIOS`]] lazy class.
 *
 * Example usage include selecting platform specific input code:
 * {{{
 *   trait InputSystem // ...
 *   class AndroidInputSystem extends InputSystem // ...
 *   class DesktopInputSystem extends InputSystem with DesktopImplementation[InputSystem] // ...
 *
 *   val inputSystem = selectImplementation[InputSystem](
 *     IfAndroid(new AndroidInputSystem), // lazy, AndroidInputSystem created only when valid
 *     new DesktopInputSystem // eager, DesktopInputSystem created always
 *   )
 * }}}
 */
package object core {
  /** Implicit object translating between `Boolean` and [[com.specdevs.specgine.core.Config `Config`]]. */
  implicit object ImplicitBooleanConfig extends Provider[Config,Boolean] {
    def unpack(value: Option[Config]): Option[Boolean] = {
      value match {
        case Some(BooleanConfig(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: Boolean): Config = BooleanConfig(value)
  }

  /** Implicit object translating between `Float` and [[com.specdevs.specgine.core.Config `Config`]]. */
  implicit object ImplicitFloatConfig extends Provider[Config,Float] {
    def unpack(value: Option[Config]): Option[Float] = {
      value match {
        case Some(FloatConfig(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: Float): Config = FloatConfig(value)
  }

  /** Implicit object translating between `Int` and [[com.specdevs.specgine.core.Config `Config`]]. */
  implicit object ImplicitIntConfig extends Provider[Config,Int] {
    def unpack(value: Option[Config]): Option[Int] = {
      value match {
        case Some(IntConfig(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: Int): Config = IntConfig(value)
  }

  /** Implicit object translating between `Long` and [[com.specdevs.specgine.core.Config `Config`]]. */
  implicit object ImplicitLongConfig extends Provider[Config,Long] {
    def unpack(value: Option[Config]): Option[Long] = {
      value match {
        case Some(LongConfig(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: Long): Config = LongConfig(value)
  }

  /** Implicit object translating between `String` and [[com.specdevs.specgine.core.Config `Config`]]. */
  implicit object ImplicitStringConfig extends Provider[Config,String] {
    def unpack(value: Option[Config]): Option[String] = {
      value match {
        case Some(StringConfig(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: String): Config = StringConfig(value)
  }

  /** Implicit object translating between `Boolean` and [[com.specdevs.specgine.core.StateInfo `StateInfo`]]. */
  implicit object ImplicitBooleanStateInfo extends Provider[StateInfo,Boolean] {
    def unpack(value: Option[StateInfo]): Option[Boolean] = {
      value match {
        case Some(BooleanStateInfo(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: Boolean): StateInfo = BooleanStateInfo(value)
  }

  /** Implicit object translating between `Float` and [[com.specdevs.specgine.core.StateInfo `StateInfo`]]. */
  implicit object ImplicitFloatStateInfo extends Provider[StateInfo,Float] {
    def unpack(value: Option[StateInfo]): Option[Float] = {
      value match {
        case Some(FloatStateInfo(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: Float): StateInfo = FloatStateInfo(value)
  }

  /** Implicit object translating between `Int` and [[com.specdevs.specgine.core.StateInfo `StateInfo`]]. */
  implicit object ImplicitIntStateInfo extends Provider[StateInfo,Int] {
    def unpack(value: Option[StateInfo]): Option[Int] = {
      value match {
        case Some(IntStateInfo(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: Int): StateInfo = IntStateInfo(value)
  }

  /** Implicit object translating between `Long` and [[com.specdevs.specgine.core.StateInfo `StateInfo`]]. */
  implicit object ImplicitLongStateInfo extends Provider[StateInfo,Long] {
    def unpack(value: Option[StateInfo]): Option[Long] = {
      value match {
        case Some(LongStateInfo(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: Long): StateInfo = LongStateInfo(value)
  }

  /** Implicit object translating between `String` and [[com.specdevs.specgine.core.StateInfo `StateInfo`]]. */
  implicit object ImplicitStringStateInfo extends Provider[StateInfo,String] {
    def unpack(value: Option[StateInfo]): Option[String] = {
      value match {
        case Some(StringStateInfo(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: String): StateInfo = StringStateInfo(value)
  }

  /** Method selecting one of multiple implementations.
   *
   * When multiple implementations of single class `T` are present, this method will
   * return the first implementation, whose selector is marked as valid.
   *
   * @tparam T Type with multiple implementations.
   * @param implementations Vararg or multiple implementations wrapped in
   *        [[com.specdevs.specgine.core.ImplementationSelector `ImplementationSelector`]]s.
   * @return Some implementation if found.
   */
  def selectImplementation[T](implementations: ImplementationSelector[T]*): Option[T] = {
    implementations find {
      case implementation: ImplementationSelector[T] => implementation.isValid
    } map { _.getInstance }
  }
}
