package com.specdevs.specgine.core

import com.specdevs.specgine.input.{AbstractInputManager,InputReceiver,InputProxy}
import com.specdevs.specgine.assets.AbstractAssetManager

import scala.collection.mutable.{HashMap,Stack,Queue}

/** Main class managing states.
 *
 * `StateManager` keeps track of all [[com.specdevs.specgine.core.StateGroup `StateGroup`]]s
 * and [[com.specdevs.specgine.core.State `State`]]s and relations between
 * them. It also keeps information what [[com.specdevs.specgine.core.State `State`]]
 * is currently active and which is frozen. Internally this is implemented
 * using classic stack of states, where top state is active and all below
 * states are frozen.
 *
 * Key operations on stack are:
 *   - `push` - Results in new state being put on top of stack.
 *   - `pop` - Results in currently active state to be removed from stack.
 *   - `change` - It is `pop` and `push` merged, without activating and deactivating
 *     state just behind current state.
 *   - `quit` - Requests closing of game and `pop` on all states on stack.
 *
 * `StateManager` is responsible for delegating processing and rendering to
 * currently active state, and taking care for initialization or displaying loading
 * screen during state transitions. Finally, it is responsible for exposing
 * interface to exchange information between states.
 *
 * @see [[http://guff.tigris.org/docs/SBGames06-en.pdf ''"An Architecture for Game State Management based on State
 *      Hierarchies"]], by Luis Valente, Aura Conci and Bruno Feij.
 * @constructor Should be called from inside of [[com.specdevs.specgine.core.AbstractGame `AbstractGame`]].
 */
class StateManager extends GetterTarget[StateInfo] with SetterTarget[StateInfo] {
  private var theAssetManager: Option[AbstractAssetManager] = None

  private var profilerGetter: (Unit => Option[ProfilingData]) = (Unit => None)

  /** Getter for instance of
   * [[com.specdevs.specgine.assets.AbstractAssetManager `AbstractAssetManager`]].
   *
   * @return Instance of [[com.specdevs.specgine.assets.AbstractAssetManager `AbstractAssetManager`]].
   */
  def assetManager: AbstractAssetManager = {
    if (theAssetManager.isEmpty) {
      throw new IllegalStateException("You haven't set assetManager.")
    } else {
      theAssetManager.get
    }
  }

  /** Setter for instance of
   * [[com.specdevs.specgine.assets.AbstractAssetManager `AbstractAssetManager`]].
   *
   * @param manager Instance of [[com.specdevs.specgine.assets.AbstractAssetManager `AbstractAssetManager`]].
   */
  def assetManager_=(manager: AbstractAssetManager): Unit = {
    theAssetManager = Some(manager)
  }

  private var theConfigManager: Option[AbstractConfigManager] = None

  /** Getter for instance of
   * [[com.specdevs.specgine.core.AbstractConfigManager `AbstractConfigManager`]].
   *
   * @return Instance of [[com.specdevs.specgine.core.AbstractConfigManager `AbstractConfigManager`]].
   */
  def configManager: AbstractConfigManager = {
    if (theConfigManager.isEmpty) {
      throw new IllegalStateException("You haven't set configManager.")
    } else {
      theConfigManager.get
    }
  }

  /** Setter for instance of
   * [[com.specdevs.specgine.core.AbstractConfigManager `AbstractConfigManager`]].
   *
   * @param manager Instance of [[com.specdevs.specgine.core.AbstractConfigManager `AbstractConfigManager`]].
   */
  def configManager_=(manager: AbstractConfigManager): Unit = {
    theConfigManager = Some(manager)
  }

  private var theInputManager: Option[AbstractInputManager] = None

  /** Getter for instance of
   * [[com.specdevs.specgine.input.AbstractInputManager `AbstractInputManager`]].
   *
   * @return Instance of [[com.specdevs.specgine.input.AbstractInputManager `AbstractInputManager`]].
   */
  def inputManager: AbstractInputManager = {
    if (theInputManager.isEmpty) {
      throw new IllegalStateException("You haven't set inputManager.")
    } else {
      theInputManager.get
    }
  }

  /** Setter for instance of
   * [[com.specdevs.specgine.input.AbstractInputManager `AbstractInputManager`]].
   *
   * @param manager Instance of [[com.specdevs.specgine.input.AbstractInputManager `AbstractInputManager`]].
   */
  def inputManager_=(manager: AbstractInputManager): Unit = {
    theInputManager = Some(manager)
  }

  private var theDefaultLoader: Option[LoadingScreen] = None

  /** Getter for instance of
   * [[com.specdevs.specgine.core.LoadingScreen `LoadingScreen`]].
   *
   * Used as loading screen when there are no states on stack, for example to
   * load resources needed by another loading screens.
   *
   * @return Instance of [[com.specdevs.specgine.core.LoadingScreen `LoadingScreen`]].
   */
  def defaultLoader: LoadingScreen = {
    if (theDefaultLoader.isEmpty) {
      throw new IllegalStateException("You haven't set defaultLoader.")
    } else {
      theDefaultLoader.get
    }
  }

  /** Setter for instance of
   * [[com.specdevs.specgine.core.LoadingScreen `LoadingScreen`]].
   *
   * Used as loading screen when there are no states on stack, for example to
   * load resources needed by another loading screens.
   *
   * @param loadingScreen Instance of [[com.specdevs.specgine.core.LoadingScreen `LoadingScreen`]].
   */
  def defaultLoader_=(loadingScreen: LoadingScreen): Unit = {
    theDefaultLoader = Some(loadingScreen)
  }

  private var theUserLoader: Option[LoadingScreen] = None

  /** Getter for instance of
   * [[com.specdevs.specgine.core.LoadingScreen `LoadingScreen`]].
   *
   * Used as loading screen when active state does not overload
   * `resizeLoading`, `processLoading` or `renderLoading` methods. If unset is
   * equal to default loader.
   *
   * @return Instance of [[com.specdevs.specgine.core.LoadingScreen `LoadingScreen`]].
   */
  def userLoader: LoadingScreen = {
    if (theUserLoader.isEmpty) {
      defaultLoader
    } else {
      theUserLoader.get
    }
  }

  /** Setter for instance of
   * [[com.specdevs.specgine.core.LoadingScreen `LoadingScreen`]].
   *
   * Used as loading screen when active state does not overload
   * `resizeLoading`, `processLoading` or `renderLoading` methods.
   *
   * @param loadingScreen Instance of [[com.specdevs.specgine.core.LoadingScreen `LoadingScreen`]].
   */
  def userLoader_=(loadingScreen: LoadingScreen): Unit = {
    theUserLoader = Some(loadingScreen)
  }

  private val stack = new Stack[StateNode]

  private val stackCommands = new Queue[StackCommand]

  private val groups = new HashMap[String, GroupNode]

  private val states = new HashMap[String, StateNode]

  private var defaultState: Option[String] = None

  private var loading = false

  private var width = 0

  private var height = 0

  private var forceQuit = true

  /** Returns `true` if and only if application should quit. */
  def shouldQuit: Boolean = forceQuit

  /** Register group with given name.
   *
   * If `parentName` will equal `name`, group will be considered root of new
   * separate state hierarchy.
   *
   * @param group [[com.specdevs.specgine.core.StateGroup `StateGroup`]] to add.
   * @param name Name of [[com.specdevs.specgine.core.StateGroup `StateGroup`]].
   * @param parentName Name of [[com.specdevs.specgine.core.StateGroup `StateGroup`]].
   * @throws IllegalArgumentException If parent group is missing or name already used.
   */
  def addGroup(group: StateGroup, name: String, parentName: String = ""): Unit = {
    if (groups.isDefinedAt(name)) {
      throw new IllegalArgumentException(s"Group $name is already added.")
    }
    if (groups.isDefinedAt(parentName)) {
      groups += name -> new GroupNode(group, groups(parentName).level+1, parentName)
    } else if (name == parentName) {
      groups += name -> new GroupNode(group, 0, name)
    } else {
      throw new IllegalArgumentException(s"Group $parentName doesn't exist.")
    }
    group.assetManager = assetManager
    group.configManager = configManager
  }

  /** Register state with given name.
   *
   * @param state [[com.specdevs.specgine.core.State `State`]] to add.
   * @param name Name of [[com.specdevs.specgine.core.State `State`]].
   * @param parentName Name of [[com.specdevs.specgine.core.StateGroup `StateGroup`]].
   * @param default Set to `true` if you want to start game from this state.
   * @throws IllegalArgumentException If parent group is missing or name already used.
   */
  def addState(state: State, name: String, parentName: String = "", default: Boolean = false): Unit = {
    if (states.isDefinedAt(name)) {
      throw new IllegalArgumentException(s"State $name is already added.")
    }
    if (!groups.isDefinedAt(parentName)) {
      throw new IllegalArgumentException(s"Group $parentName doesn't exist.")
    }
    state.stateManager = this
    state.configManager = configManager
    state.assetManager = assetManager
    states += name -> new StateNode(state, parentName)
    if (default || defaultState.isEmpty) {
      defaultState = Some(name)
      forceQuit = false
    }
  }

  /** Method requesting push of new state.
   *
   * If new state is already on stack, all states between current and this
   * state will be disposed. If requested state is not on stack, current state
   * will be frozen and new state will be added on top.
   *
   * @param name Name of [[com.specdevs.specgine.core.State `State`]].
   * @throws IllegalArgumentException If name does not exists.
   */
  def push(name: String): Unit = {
    if (!states.isDefinedAt(name)) {
      throw new IllegalArgumentException(s"State $name doesn't exist.")
    }
    stackCommands.enqueue(PushStackCommand(name))
  }

  /** Method requesting pop of current state.
   *
   * Current state will be disposed, and top frozen state will become active
   * again.
   */
  def pop(): Unit = {
    stackCommands.enqueue(PopStackCommand())
  }

  /** Method requesting change of current state.
   *
   * Current state will be disposed, and replaced with given state. If new
   * state is already on stack, `changeState` is equal to `pushState`.
   *
   * @param name Name of [[com.specdevs.specgine.core.State `State`]].
   * @throws IllegalArgumentException If name does not exists.
   */
  def change(name: String): Unit = {
    if (!states.isDefinedAt(name)) {
      throw new IllegalArgumentException(s"State $name doesn't exist.")
    }
    stackCommands.enqueue(ChangeStackCommand(name))
  }

  /** Method requesting close of game. */
  def quit(): Unit = {
    stackCommands.enqueue(QuitStackCommand())
  }

  /** Method called by [[com.specdevs.specgine.core.AbstractGame `AbstractGame`]]
   * when size of screen changee.
   *
   * @param x Screen width.
   * @param y Screen height.
   */
  def resize(x: Int, y: Int): Unit = {
    width = x
    height = y
    doResize()
  }

  /** Method called by [[com.specdevs.specgine.core.AbstractGame `AbstractGame`]]
   * requests processing of game.
   *
   * @param dt Time of simulation step.
   */
  def process(dt: Float): Unit = {
    if (loading) {
      assetManager.update()
      if (stack.isEmpty) {
        defaultLoader.processLoading(dt)
      } else {
        stack.head.state.processLoading(dt)
      }
    } else {
      if (stack.nonEmpty) {
        stack.head.state.doProcess(dt)
      }
      issueCommands()
    }
  }

  /** Method called by [[com.specdevs.specgine.core.AbstractGame `AbstractGame`]]
   * requests rendering of game.
   *
   * @param alpha Render interpolation between two last simulation steps (0..1).
   */
  def render(alpha: Float): Unit = {
    if (loading) {
      val progress = assetManager.getPercentage
      if (stack.isEmpty) {
        defaultLoader.renderLoading(alpha, progress)
      } else {
        stack.head.state.renderLoading(alpha, progress)
      }
      if (!assetManager.needsLoading) {
        loading = false
        if (stack.isEmpty) {
          doPush(defaultState.get)
        } else {
          doCreate(stack.head)
        }
      }
    } else {
      if (stack.nonEmpty) {
        stack.head.alpha = alpha
        stack.head.state.render(alpha)
      }
    }
  }

  /** Method calling `processFrozen` on frozen state.
   *
   * @param dt Time since last simulation step.
   */
  def processFrozen(dt: Float): Unit = {
    val superState = stack.tail.headOption
    if (superState.isDefined) {
      superState.get.state.processFrozen(dt)
    }
  }

  /** Method calling `renderFrozen` of frozen state.
   *
   * @param alpha Render interpolation between two last simulation steps (0..1).
   */
  def renderFrozen(alpha: Float): Unit = {
    val superState = stack.tail.headOption
    if (superState.isDefined) {
      superState.get.state.renderFrozen(alpha, superState.get.alpha)
    }
  }

  /** Method used to get state info from manager.
   *
   * @param key Key poinint to value.
   * @param target Name of group to look for value, or `None` for direct parent.
   * @return Value associated with key, or `None` if not found.
   * @throws IllegalArgumentException If target is wrong.
   */
  def getFrom(key: String, target: Option[String] = None): Option[StateInfo] = {
    def getInfoIn(group: String): Option[StateInfo] = {
      groups(group).group.getLocalInfo(key) match {
        case None => {
          val parent = groups(group).parent
          if (group == parent) {
            None
          } else {
            getInfoIn(parent)
          }
        }
        case info: Option[StateInfo] => info
      }
    }
    getInfoIn(getGroupFor(target))
  }

  /** Method used to set state info in manager.
   *
   * @param key Key poinint to value.
   * @param value Value to set.
   * @param target Name of group to look for value, or `None` for direct parent.
   * @throws IllegalArgumentException If target is wrong.
   */
  def setIn(key: String, value: StateInfo, target: Option[String] = None): Unit = {
    groups(getGroupFor(target)).group.setLocalInfo(key, value)
  }

  /** Method called by [[com.specdevs.specgine.core.AbstractGame `AbstractGame`]]
   * before game is initialized.
   *
   * It creates and registers default group named `""` (empty string).
   */
  def initialize(): Unit = {
    addGroup(new StateGroup {
      def initialize(): Unit = {}
      def create(): Unit = {}
      def dispose(): Unit = {}
    }, "", "")
  }

  /** Method called by [[com.specdevs.specgine.core.AbstractGame `AbstractGame`]]
   * after game is initialized.
   */
  def create(): Unit = {
    if (assetManager.needsLoading) {
      loading = true
      defaultLoader.reset()
    } else {
      if (defaultState.nonEmpty) {
        doPush(defaultState.get)
      }
    }
  }

  /** Method called by [[com.specdevs.specgine.core.AbstractGame `AbstractGame`]]
   * before game is closed.
   */
  def dispose(): Unit = {
    def performDispose(): Unit = {
      doDispose(stack.head)
      if (stack.nonEmpty) performDispose()
    }
    if (stack.nonEmpty) {
      doLeave(stack.head)
      performDispose()
    }
  }

  private def doForGroups(name: String, pre: GroupNode => Unit, post: GroupNode => Unit): Unit = {
    val group = groups(name)
    val parentName = group.parent
    pre(group)
    if (parentName != name) {
      doForGroups(parentName, pre, post)
    }
    post(group)
  }

  private def doInitialize(node: StateNode): Unit = {
    stack.push(node)
    doForGroups(node.parent, n => (), _.initialize())
    node.initialize()
    configManager.store()
    if (assetManager.needsLoading) {
      loading = true
      userLoader.reset()
      doResize()
    } else {
      doCreate(node)
    }
  }

  private def doCreate(node: StateNode): Unit = {
    doForGroups(node.parent, n => (), _.create())
    node.state.doCreate()
    doEnter(node)
  }

  private def doEnter(node: StateNode): Unit = {
    inputManager.reset()
    doResize()
    val state = node.state
    state.doEnter()
    state match {
      case s: InputReceiver => inputManager.addReceiver(s)
      case _ => ()
    }
    state match {
      case p: InputProxy => p.addReceivers(inputManager)
      case _ => ()
    }
  }

  private def doLeave(node: StateNode): Unit = {
    inputManager.reset()
    node.state.doLeave()
  }

  private def doDispose(node: StateNode): Unit = {
    node.dispose()
    doForGroups(node.parent, _.dispose(), n => ())
    stack.pop()
    ()
  }

  private def doPush(name: String): Unit = {
    val node = states(name)
    if (node.isOnStack) {
      if (node != stack.head) {
        def findState(): Unit = {
          doDispose(stack.head)
          if (node != stack.head) {
            findState()
          }
        }
        doLeave(stack.head)
        findState()
        doEnter(stack.head)
      }
    } else {
      if (stack.nonEmpty) {
        doLeave(stack.head)
      } else {
        forceQuit = false
      }
      doInitialize(node)
    }
  }

  private def doPop(): Unit = {
    if (stack.nonEmpty) {
      val node = stack.head
      doLeave(node)
      doDispose(node)
      if (stack.nonEmpty) {
        doEnter(stack.head)
      } else {
        doQuit()
      }
    }
  }

  private def doChange(name: String): Unit = {
    val node = states(name)
    if (node.isOnStack || stack.isEmpty) {
      doPush(name)
    } else {
      doLeave(stack.head)
      doDispose(stack.head)
      doInitialize(node)
    }
  }

  private def doQuit(): Unit = {
    forceQuit = true
  }

  private def issueCommands(): Unit = {
    if (stackCommands.nonEmpty) {
      stackCommands.dequeue() match {
        case PushStackCommand(name) => doPush(name)
        case PopStackCommand() => doPop()
        case ChangeStackCommand(name) => doChange(name)
        case QuitStackCommand() => doQuit()
      }
      issueCommands()
    }
  }

  private def doResize(): Unit = {
    if (stack.nonEmpty) {
      if (loading) {
        stack.head.state.resizeLoading(width, height)
      } else {
        stack.head.state.resize(width, height)
      }
      val parent = stack.tail.headOption
      if (parent.isDefined) {
        parent.get.state.resizeFrozen(width, height)
      }
    } else if (loading) {
      defaultLoader.resize(width, height)
    }
  }

  private def getGroupFor(target: Option[String]): String = {
    def findGroupFor(target: String, now: String): String = {
      if (now == target) {
        now
      } else {
        val parent = groups(now).parent
        if (parent == now) {
          throw new IllegalArgumentException(s"Group $target isn't parent of currently active state.")
        } else {
          findGroupFor(target, parent)
        }
      }
    }
    if (stack.isEmpty) {
      throw new IllegalStateException("Cannot get nor set StateInfo when stack is empty.")
    } else {
      target match {
        case None => stack.head.parent
        case Some(name) => findGroupFor(name, stack.head.parent)
      }
    }
  }

  /** Setter for instance of profiling data extractor function.
   *
   * @param f Function which takes `Unit` and returns an `Option` of
   *   [[com.specdevs.specgine.core.ProfilingData `ProfilingData`]].
   */
  def registerProfilerGetter(f: Unit => Option[ProfilingData]): Unit = {
    profilerGetter = f
  }

  /** Getter for instance of profiling data extractor function.
   *
   * @return Function which takes `Unit` and returns an `Option` of
   *   [[com.specdevs.specgine.core.ProfilingData `ProfilingData`]].
   */
  def getProfilingData: Option[ProfilingData] = {
    profilerGetter(())
  }
}
