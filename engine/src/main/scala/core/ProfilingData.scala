package com.specdevs.specgine.core

/** Class which stores profiling statistics.
 *
 * Contains statistics about `OpenGL` and FPS performance.
 * Note that for the first second `fps` will be equal to zero and while loading assets
 * it can be smaller than expected. This is due to the fact that first couple of frames can take longer, because
 * [[com.specdevs.specgine.assets.gdx.AssetManager `AssetManager`]] might need to finalize loading assets.
 * This may require long-lasting actions e.g. texture binding.
 * After loading all assets, `fps` metric should stabilize after 1 second.
 * Instance of ProfilingData is mutable but it changes only after calling `render` method of
 * [[com.specdevs.specgine.core.gdx.Game `Game`]] class. User can safely use it inside `process` and `render`
 * methods as it will not change.
 *
 * @param calls Amount of total OpenGL calls.
 * @param drawCalls Amount of draw calls.
 * @param textureBindings Amount of texture bindings.
 * @param shaderSwitches Amount of shader switches.
 * @param minVertex The smallest value of used vertices.
 * @param maxVertex The largest value of used vertices.
 * @param countVertex The amount of vertices additions.
 * @param totalVertex Total amount of used vertices.
 * @param fps Amount of current average frame per second.
 * @param skippedFrames Estimated number of skipped frames.
 * @param stable Whether there were skipped frames or not.
 * @param avgAnimFrameTime Average animation frame time.
 * @param renderingTime Time of rendering.
 * @param animFramCount Number of animation frames.
 * @param deltaTime Time span between beginning of last and current frame.
 */
case class ProfilingData(
  var calls: Int,
  var drawCalls: Int,
  var textureBindings: Int,
  var shaderSwitches: Int,
  var minVertex: Float,
  var maxVertex: Float,
  var countVertex: Int,
  var totalVertex: Float,
  var fps: Int,
  var skippedFrames: Float,
  var stable: Boolean,
  var avgAnimFrameTime: Float,
  var renderingTime: Float,
  var animFrameCount: Int,
  var deltaTime: Float
) {
  /** Calculates time difference between previous frame time and time for rendering and animation.
   *
   * Note that if everything is ok and there is no aggravating loading at the same time,
   * it should be positive and relatively constant.
   * @return Time difference between previous frame time and time for rendering and animation.
   */
  def timeDiff: Float = deltaTime - renderingTime - avgAnimFrameTime*animFrameCount
}

/** Companion object for [[com.specdevs.specgine.core.ProfilingData `ProfilingData`]] class. */
object ProfilingData {
  def apply(): ProfilingData = ProfilingData(0, 0, 0, 0, 0.0f, 0.0f, 0, 0.0f, 0, 0.0f, true, 0.0f, 0.0f, 0, 0.0f)
}
