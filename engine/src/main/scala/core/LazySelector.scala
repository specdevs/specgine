package com.specdevs.specgine.core

/** Lazy selector of implementation wrapper for type `T`.
 *
 * @tparam T Type to validate.
 * @param implementation Implementation of type `T`, passed by name.
 * @constructor Create lazy wrapper for type `T`.
 */
abstract class LazySelector[T](implementation: => T) extends ImplementationSelector[T] {
  /** The instance of `T` with implementation.
   *
   * @note This method may fail if implementation is not valid.
   * @return Instance of `T` implemented with selector.
   */
  lazy val getInstance: T = implementation
}
