package com.specdevs.specgine.core

import scala.concurrent.Future

/** Trait used for accessing future values.
 *
 * @tparam A Type of stored values.
 */
trait GetterFuture[A] {
  /** Method returning value.
   *
   * @param key Key pointing to value.
   * @return Value associated with `key`, `None` if not found.
   */
  def getFuture(key: String): Future[A]
}
