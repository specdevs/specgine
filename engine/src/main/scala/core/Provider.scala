package com.specdevs.specgine.core

/** Trait with methods for packaging and unpackaging of values.
 *
 * This trait allows to convert between `A` and `B` if there exists some `C`
 * such, that `C(B) <: A`. Usually `C` will be value case class with `B` as
 * its only parameter, `A` will be universal trait, and `B` some type, unrelated
 * to `A`. Instances of `Provider[A,B]` are used as implicit parameters to `get`
 * and `set` family of methods.
 *
 * @tparam A Common type of stored values.
 * @tparam B Type of value, potentially unrelated to `A`.
 */
trait Provider[A,B] {
  /** Method that unpacks value.
   *
   * It returns `None` if unpackaging is not possible, or when value to unpack
   * is already `None`.
   *
   * @param value Value to unpack.
   * @return Unpacked value.
   */
  def unpack(value: Option[A]): Option[B]

  /** Method that packs value.
   *
   * This method should always succeed.
   * @param value Value to pack.
   * @return Packed value.
   */
  def pack(value: B): A
}
