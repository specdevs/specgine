package com.specdevs.specgine.core

/** Trait used for storing values in optional target.
 *
 * @tparam A type of stored values.
 */
trait SetterTarget[A] {
  /** Method storing value.
   *
   * @param key Key pointing to value.
   * @param target Where the value should be set, `None` for default target.
   * @param value Value to store.
   * @throws IllegalArgumentException If target is wrong.
   */
  def setIn(key: String, value: A, target: Option[String] = None): Unit
}
