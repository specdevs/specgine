package com.specdevs.specgine.animation

import scala.math.pow
import scala.language.implicitConversions

/** Package that gathers interpolators definitions.
 *
 * User can define as fancy interpolators as he/she wishes,
 * as long as they are continuous, with domain [0, 1]
 * and for counterdomain equals to Double or Float their value at 0 is 0
 * and their value at 1 is 1, for example:
 * {{{
 *   case object FancyInterpolator extends Interpolator[Float] { def apply(x: Float): Float = x*x/4 + sin(x*pi/2)*3/4 }
 * }}}
 */
package interpolators {
  /** Linear easing function. */
  case object Linear extends Interpolator[Float] {
    def apply(x: Float): Float = x
  }

  /** Quadratic easing function. */
  case object Quad extends Interpolator[Float] {
    def apply(x: Float): Float = x*x
  }

  /** Power easing function.
   *
   * @constructor Creates new power interpolator.
   * @param power Power of a chosen interpolator - a power function.
   */
  case class Power(val power: Float) extends Interpolator[Float] {
    def apply(x: Float): Float = pow(x.toDouble, power.toDouble).toFloat
  }
}

package object interpolators {
  /** Default function conversion to [[com.specdevs.specgine.animation.Interpolator `Interpolator`]].
   * Enables using functions instead of defining interpolators.
   */
  implicit def functionToInterpolator[T](f: Float => T): Interpolator[T] = new Interpolator[T] {
    def apply(x: Float): T = f(x)
  }
}
