package com.specdevs.specgine

/** Package providing tween mechanism in SpecGine.
 *
 * Classes in this package are responsible for parameters' animation handling. All tweens
 * should be subtypes of [[com.specdevs.specgine.animation.TweenInterface `TweenInterface`]].
 * Easing functions for animations are supposed to be subtypes of
 * [[com.specdevs.specgine.animation.Interpolator `Interpolator`]].
 * Some exemplary interpolators can be found in [[com.specdevs.specgine.animation.interpolators `interpolators`]].
 *
 * Below we present a simple example of how to use tweens.
 * Inside classes implementing TweenManagerUser it is possible to use code similar to the following:
 * {{{
 *   case class Ball(var radius: Float)
 *   val ball = Ball(0.05f)
 *   val animation = new Tween(access(ball.radius), 0.5f, 2f)
 *   addTween(animation) // Ball.radius now changes with time, after 1second it will be equal to 0.275
 * }}}
 */
package object animation
