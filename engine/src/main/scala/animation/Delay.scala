package com.specdevs.specgine.animation

/** Class for animation delay. It can be used to delay other animations.
 *
 * @constructor Creates new [[com.specdevs.specgine.animation.Delay `Delay`]].
 * @param duration Amount of time in seconds devoted for delay.
 */
class Delay(val duration: Float) extends TweenInterface {

  private var t = 0f

  def update(dt: Float): Boolean = {
    t += dt
    t >= duration
  }
}
