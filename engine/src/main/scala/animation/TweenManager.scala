package com.specdevs.specgine.animation

import scala.collection.mutable.{HashMap,Queue}

/** Class for managing tweens.
 *
 * Tweens can be called in the desired order.
 * It can be achieved through specifying the tween after which currently added one will be used.
 *
 * @constructor Creates instance of [[com.specdevs.specgine.animation.TweenManager `TweenManager`]].
 */
class TweenManager {
  private case class TweenNode(
    val tween: TweenInterface,
    val after: Queue[(TweenId, TweenInterface)] = new Queue)

  private var current = TweenId(0)

  private val tweens = new HashMap[TweenId, TweenNode]

  /** Method used to update all managed tweens. If a tween has already finished, it's removed from the map.
   *
   * @param dt Current time.
   */
  def update(dt: Float): Unit = {
    val removable = tweens filter { case (_, TweenNode(tween, _)) => tween.update(dt) }
    removable foreach { case (id, _) => remove(id) }
  }

  /** Method used to add a tween.
   *
   * @param tween A tween to be added.
   * @param after A tween after which base tween will be triggered.
   *   'None' means that base tween will start immediately.
   * @return [[com.specdevs.specgine.animation.TweenId `TweenId`]] of a newly added tween.
   */
  def add(tween: TweenInterface, after: Option[TweenId] = None): TweenId = {
    current = current.next
    after match {
      case Some(tweenId) =>
        if (tweens.isDefinedAt(tweenId)) {
          tweens.get(tweenId).get.after += (current -> tween)
        } else {
          throw new IllegalArgumentException(s"There is no tween defined for $tweenId")
        }
      case None => tweens += (current -> TweenNode(tween))
    }
    current
  }

  /** Method used to remove a tween with given TweenId.
   *
   * @param id A TweenId to be removed.
   */
  def remove(id: TweenId): Unit = {
    val tween = tweens remove id
    tween match {
      case Some(tweenNode) =>
        for ((tweenId, tweenAfter) <- tweenNode.after) tweens += (tweenId -> TweenNode(tweenAfter))
      case None => ()
    }
  }
}
