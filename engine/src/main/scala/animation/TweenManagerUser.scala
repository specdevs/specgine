package com.specdevs.specgine.animation

/** Trait mixed-in into classes that use [[com.specdevs.specgine.animation.TweenManager `TweenManager`]]. */
trait TweenManagerUser {
  private var addedTweens: List[TweenId] = Nil

  private var theTweenManager: Option[TweenManager] = None
  /** Getter for instance of
   * [[com.specdevs.specgine.animation.TweenManager `TweenManager`]].
   *
   * @return Tween manager instance.
   */
  implicit def tweenManager: TweenManager = {
    if (theTweenManager.isEmpty) {
      throw new IllegalStateException("You haven't set tweenManager.")
    } else {
      theTweenManager.get
    }
  }

  /** Setter for instance of
   * [[com.specdevs.specgine.animation.TweenManager `TweenManager`]].
   *
   * @param manager Tween manager instance.
   */
  def tweenManager_=(manager: TweenManager): Unit = {
    theTweenManager = Some(manager)
  }

  /** Method used to add a tween.
   *
   * @param tween A tween to be added.
   * @param after A tween after which base tween will be triggered.
   */
  def addTween(tween: TweenInterface, after: Option[TweenId] = None): TweenId = {
    val tweenId = theTweenManager.get.add(tween, after)
    addedTweens = tweenId :: addedTweens
    tweenId
  }

  /** Method used to remove a tween with given TweenId.
   *
   * @param id A TweenId to be removed.
   */
  def removeTween(id: TweenId): Unit = {
    theTweenManager.get.remove(id)
  }

  /** Method removing all tweens added through `TweenManager`. */
  def disposeTweens(): Unit = {
    addedTweens foreach {removeTween(_)}
  }
}
