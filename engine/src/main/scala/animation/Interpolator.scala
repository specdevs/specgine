package com.specdevs.specgine.animation

/** Represents easing functions for [[com.specdevs.specgine.animation.Tween `Tween`]]s. */
trait Interpolator[+T] extends Function1[Float,T]
