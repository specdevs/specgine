package com.specdevs.specgine.animation

/** A common interface for all Tweenable objects. */
trait TweenInterface {

  /** Method used to update the desired value.
   *
   * @param dt Curent time.
   * @return flag that indicates whether the animation is finished or not.
   */
  def update(dt: Float): Boolean
}
