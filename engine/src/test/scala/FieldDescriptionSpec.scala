package com.specdevs.specgine.test

import com.specdevs.specgine.macros.states.{Needed,Optional,All}
import com.specdevs.specgine.states.{Component,Entity}

import org.scalatest.{FunSpec,Matchers}

class FieldDescriptionSpec extends FunSpec with Matchers {
  val pos = new Optional[Position]
  val vel = new Needed[Velocity]
  val all = new All
  val entity1 = Entity(1)
  val entity2 = Entity(2)

  describe("Needed") {
    it("should have type like Map[Entity,T]") {
      assertCompiles("vel(entity1): Velocity")
    }
  }

  describe("Optional") {
    it("should have type like Map[Entity,Option[T]]") {
      assertCompiles("pos(entity1): Option[Position]")
    }
  }

  describe("All") {
    it("should have type like Map[Entity,Set[Component]]") {
      assertCompiles("all(entity1): Set[Component]")
    }
  }

  describe("FieldDescription") {
    it("should be possible to add new elements") {
      vel isDefinedAt entity1 should equal (false)
      vel.update(entity1, Velocity(0, 0))
      vel isDefinedAt entity1 should equal (true)
    }

    it("should be possible to get elements") {
      vel.get(entity1) should equal (Some(Velocity(0, 0)))
      vel(entity1) should equal (Velocity(0, 0))
      vel.get(entity2) should equal (None)
      val thrown = intercept[NoSuchElementException] {
        vel(entity2)
      }
      assert(thrown.getMessage === entity2.id.toString)
    }

    it("should be possible to update elements") {
      val value1 = Velocity(0, 0)
      val value2 = Velocity(1, 1)
      vel.update(entity1, value1)
      vel.update(entity2, value2)
      vel(entity1) should equal (value1)
      vel(entity2) should equal (value2)
      vel.update(entity1, value2)
      vel.update(entity2, value1)
      vel(entity1) should equal (value2)
      vel(entity2) should equal (value1)
    }

    it("should be possible to iterate over elements") {
      val value1 = Velocity(0, 0)
      val value2 = Velocity(1, 1)
      vel.update(entity1, value1)
      vel.update(entity2, value2)
      def change(e: Entity, v: Velocity): Unit = {v.x += 1; ()}
      vel foreach change
      value1.x should equal (1)
      value2.x should equal (2)
    }

    it("should be possible to remove elements") {
      vel isDefinedAt entity1 should equal (true)
      vel.remove(entity1)
      vel isDefinedAt entity1 should equal (false)
    }
  }
}
