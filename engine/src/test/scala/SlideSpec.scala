package com.specdevs.specgine.test

import com.specdevs.specgine.states._

import org.scalatest.{FunSpec,Matchers}

class SlideSpec extends FunSpec with Matchers {
  class DummySlide extends Slide with DummyCallbacksImplementation {
    var processed = false
    var rendered = false
    var resized = false
    def process(dt: Float): Unit = {
      processed = true
    }
    def render(alpha: Float): Unit = {
      rendered = true
    }
    def resize(x: Int, y: Int): Unit = {
      resized = true
    }
    def breakable: Boolean = false
    def duration: Float = 1.0f
    def isFinished: Boolean = true
  }

  class DummySlideShowState extends SlideShowState() {
    var created = false
    var disposed = false
    var initialized = false
    def create(): Unit = {
      created = true
    }
    def dispose(): Unit = {
      disposed = true
    }
    def initialize(): Unit = {
      initialized = true
    }
  }

  val dummyAssetManager = new DummyAssetManager

  describe("Slide") {
    it("should check basic methods") {
      val dummySlide = new DummySlide
      dummySlide.doCreate()
      dummySlide.created should equal (true)
      dummySlide.doInitialize()
      dummySlide.initialized should equal (true)
      val thrownAsset = intercept[IllegalStateException] {
        dummySlide.doDispose()
      }
      assert(thrownAsset.getMessage === "You haven't set assetManager.")
      dummySlide.assetManager = dummyAssetManager
      dummySlide.process(0.1f)
      dummySlide.processed should equal (true)
      dummySlide.render(0.1f)
      dummySlide.rendered should equal (true)
      dummySlide.resize(4, 2)
      dummySlide.resized should equal (true)
      dummySlide.doDispose()
      dummySlide.disposed should equal (true)
    }
  }

  describe("SlideDeck") {
    it("should check assetManager") {
      val dummySlideDeck = new SlideDeck()
      val thrownAsset = intercept[IllegalStateException] {
        dummySlideDeck.doDispose()
      }
      assert(thrownAsset.getMessage === "You haven't set assetManager.")
      dummySlideDeck.assetManager = dummyAssetManager
      dummySlideDeck.doDispose()
    }

    it("should return true if empty") {
      val dummySlideDeck = new SlideDeck()
      dummySlideDeck.isEmpty should equal (true)
      dummySlideDeck.breakable should equal (true)
    }

    it("should do nothing when processing ") {
      val dummySlideDeck = new SlideDeck()
      dummySlideDeck.process(0.1f)
      dummySlideDeck.render(0.1f)
    }

    it("should process not finished slide") {
      class DummyNonFinishedSlide extends DummySlide {
        override def isFinished: Boolean = false
      }
      val dummySlideDeck = new SlideDeck(new DummyNonFinishedSlide)
      dummySlideDeck.process(0.1f)
    }

    it("should return false if empty") {
      val dummySlide = new DummySlide
      val dummySlideDeck = new SlideDeck(dummySlide)
      dummySlideDeck.isEmpty should equal (false)
      dummySlideDeck.breakable should equal (false)
    }

    it("should check basic methods") {
      val dummySlide = new DummySlide
      val dummySlide2 = new DummySlide
      val dummySlideDeck = new SlideDeck(dummySlide,dummySlide2)
      dummySlideDeck.assetManager = dummyAssetManager
      dummySlideDeck.doCreate()
      dummySlideDeck.doInitialize()
      dummySlideDeck.process(0.1f)
      dummySlideDeck.render(0.1f)
      dummySlideDeck.resize(1, 1)
      dummySlideDeck.doDispose()
    }
  }

  describe("SlideShowState") {
    it("should check assetManager") {
      val dummySlideShowState = new DummySlideShowState
      val thrownAsset = intercept[IllegalStateException] {
        dummySlideShowState.doDispose()
      }
      assert(thrownAsset.getMessage === "You haven't set assetManager.")
      dummySlideShowState.assetManager = dummyAssetManager
      dummySlideShowState.doDispose()
      dummySlideShowState.disposed should equal (true)
    }

    it("should initialize and add Slides") {
      val dummySlide = new DummySlide
      class TempDummySlideShowState extends DummySlideShowState {
        override def initialize(): Unit = {
          super.initialize()
          addSlides(dummySlide)
        }
      }
      val dummySlideShowState = new TempDummySlideShowState
      dummySlideShowState.assetManager = dummyAssetManager
      dummySlideShowState.doCreate()
      dummySlideShowState.created should equal (true)
      dummySlideShowState.doInitialize()
      dummySlideShowState.initialized should equal (true)
      dummySlideShowState.doDispose()
      dummySlideShowState.disposed should equal (true)
    }

    it("should check basic methods on slide") {
      val dummySlide = new DummySlide
      val dummySlide2 = new DummySlide
      class TempDummySlideShowState extends DummySlideShowState {
        override def initialize(): Unit = {
          super.initialize()
          addSlideDeck(dummySlide,dummySlide2)
        }
      }
      val dummySlideShowState = new TempDummySlideShowState
      dummySlideShowState.assetManager = dummyAssetManager
      dummySlideShowState.doInitialize()
      dummySlideShowState.doCreate()
      dummySlideShowState.process(0.1f)
      dummySlideShowState.render(0.1f)
      dummySlideShowState.resize(1, 1)
      dummySlideShowState.enter()
      dummySlideShowState.leave()
      dummySlideShowState.keyUp(42) should equal (false)
      dummySlideShowState.touchUp(1, 9, 8, 8) should equal (false)
      ()
    }

    it("should brake slide on keyUp") {
      class DummyBrakeableSlide extends DummySlide {
        override def breakable: Boolean = true
      }
      val dummyBrakeableSlide = new DummyBrakeableSlide
      val dummStateManager = new DummyStateManager
      class TempDummySlideShowState extends DummySlideShowState {
        override def initialize(): Unit = {
          super.initialize()
          addSlideDeck(dummyBrakeableSlide)
        }
      }
      val dummySlideShowState = new TempDummySlideShowState
      dummySlideShowState.assetManager = dummyAssetManager
      dummySlideShowState.stateManager = dummStateManager
      dummySlideShowState.doInitialize()
      dummySlideShowState.keyUp(42) should equal (true)
    }

    it("should brake slide on touchUp") {
      class DummyBrakeableSlide extends DummySlide {
        override def breakable: Boolean = true
      }
      val dummyBrakeableSlide = new DummyBrakeableSlide
      val dummStateManager = new DummyStateManager
      class TempDummySlideShowState extends SlideShowState(nextState = "nextState") {
        var created = false
        var disposed = false
        var initialized = false
        def create(): Unit = {
          created = true
        }
        def dispose(): Unit = {
          disposed = true
        }
        def initialize(): Unit = {
          initialized = true
          addSlideDeck(dummyBrakeableSlide)
        }
      }
      val dummySlideShowState = new TempDummySlideShowState
      dummySlideShowState.assetManager = dummyAssetManager
      dummySlideShowState.stateManager = dummStateManager
      dummySlideShowState.doInitialize()
      val thrownAsset = intercept[IllegalArgumentException] {
        dummySlideShowState.touchUp(1, 9, 8, 8) should equal (true)
      }
      assert(thrownAsset.getMessage === "State nextState doesn't exist.")
    }

    it("should update played decks") {
      val dummySlide = new DummySlide
      val dummySlideDeck = new SlideDeck(dummySlide)
      val dummStateManager = new DummyStateManager
      class TempDummySlideShowState extends DummySlideShowState {
        override def initialize(): Unit = {
          super.initialize()
          addSlideDecks(List(dummySlideDeck,dummySlideDeck): _*)
        }
      }
      val dummySlideShowState = new TempDummySlideShowState
      dummySlideShowState.assetManager = dummyAssetManager
      dummySlideShowState.stateManager = dummStateManager
      dummySlideShowState.doInitialize()
      dummySlideShowState.process(0.1f)
      ()
    }

    it("should do nothing when rendering empty deck") {
      val dummySlideShowState = new DummySlideShowState
      dummySlideShowState.render(0.1f)
      ()
    }
  }
}
