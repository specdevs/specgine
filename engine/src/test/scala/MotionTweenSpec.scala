package com.specdevs.specgine.test

import com.specdevs.specgine.core.Accessor
import com.specdevs.specgine.animation._

import org.scalatest.{FunSpec,Matchers}
import scala.collection.mutable.HashMap

class DelaydSpec extends FunSpec with Matchers {
  describe("Delay") {
    it("duration should equal to 0.1f") {
      val delay = new Delay(0.1f)
      delay.duration should equal (0.1f)
    }
    it("animation should be delayed") {
      val delay = new Delay(0.1f)
      delay.update(0.05f) should equal (false)
    }
    it("animation should not be delayed") {
      val delay = new Delay(0.1f)
      delay.update(0.1f) should equal (true)
    }
    it("animation should be delayed and then it should not be") {
      val delay = new Delay(0.1f)
      delay.update(0.05f) should equal (false)
      delay.update(0.1f) should equal (true)
    }
  }
}

class TweenSpec extends FunSpec with Matchers {
  class MyAccessor extends Accessor[Float] {
    private var temp = 1f
    def value: Float = temp
    def value_=(what: Float): Unit= {
      temp = what
    }
  }
  val myAccessor = new MyAccessor
  class MyTweenMenagerUser extends TweenManagerUser {
    val tweensAdded = new HashMap[TweenId, Boolean]
    override def addTween(tween: TweenInterface, after: Option[TweenId] = None): TweenId = {
      val id = super.addTween(tween, after)
      tweensAdded += (id->true)
      id
    }
    override def removeTween(id: TweenId): Unit = {
      super.removeTween(id)
      if (tweensAdded.isDefinedAt(id)) {
        tweensAdded += (id->false)
      }
      ()
    }
  }
  val dummyTweenMenagerUser = new MyTweenMenagerUser
  val dummyTweenMenager = new DummyTweenManager
  val dummyTweenId = TweenId(42)
  val dummyTween = new Tween(acc = myAccessor, target = 1.0f, duration = 1f, stopAfter = None)
  val dummyTween2 = new Tween(acc = myAccessor, target = 1.0f, duration = 0.05f, stopAfter = Some(1))
  val dummyTween3 = new Tween(acc = myAccessor, target = 1.0f, duration = 0.1f, stopAfter = Some(2), yoyo = true)

  describe("TweenId") {
    it("id should equal to 42") {
      dummyTweenId.id should equal (42)
    }
    it("next's id should be 43") {
      val nextTweenId = dummyTweenId.next
      nextTweenId.id should equal (43)
    }
  }

  describe("Tween") {
    it("with empty stopAfter should return false") {
      dummyTween.update(0.1f) should equal (false)
    }

    it("with non empty stopAfter should return true in the second update") {
      dummyTween2.update(0.1f) should equal (false)
      dummyTween2.update(0.1f) should equal (true)
    }

    it("with yoyo set to true should return true in the third update") {
      dummyTween3.update(0.2f) should equal (false)
      dummyTween3.update(0.2f) should equal (false)
      dummyTween3.update(0.2f) should equal (true)
    }
  }

  describe("TweenManagerUser") {
    it("should check for setting TweenManager") {
      val thrown = intercept[IllegalStateException] {
        dummyTweenMenagerUser.tweenManager
      }
      assert(thrown.getMessage === "You haven't set tweenManager.")
      dummyTweenMenagerUser.tweenManager = dummyTweenMenager
      dummyTweenMenagerUser.tweenManager should equal (dummyTweenMenager)
    }

    it("should check for adding Tweens") {
      dummyTweenMenagerUser.tweenManager = dummyTweenMenager
      val thrown = intercept[IllegalArgumentException] {
        dummyTweenMenagerUser.addTween(dummyTween, Some(dummyTweenId.next))
      }
      assert(thrown.getMessage === "There is no tween defined for TweenId(43)")
      dummyTweenMenagerUser.addTween(dummyTween)
      dummyTweenMenagerUser.addTween(dummyTween, Some(TweenId(2)))
      ()
    }

    it("should check for removing Tweens") {
      val dummyTweenMenagerUser = new MyTweenMenagerUser
      val dummyTweenMenager = new DummyTweenManager
      dummyTweenMenagerUser.tweenManager = dummyTweenMenager
      dummyTweenMenagerUser.addTween(dummyTween)
      dummyTweenMenagerUser.addTween(dummyTween2, Some(TweenId(1)))
      dummyTweenMenagerUser.tweensAdded(TweenId(1)) should equal (true)
      dummyTweenMenagerUser.removeTween(TweenId(1))
      dummyTweenMenagerUser.tweensAdded(TweenId(1)) should equal (false)
      dummyTweenMenagerUser.removeTween(TweenId(42))
    }

    it("should check for disposing Tweens") {
      val dummyTweenMenagerUser = new MyTweenMenagerUser
      val dummyTweenMenager = new DummyTweenManager
      dummyTweenMenagerUser.tweenManager = dummyTweenMenager
      dummyTweenMenagerUser.addTween(dummyTween)
      dummyTweenMenagerUser.tweensAdded(TweenId(1)) should equal (true)
      dummyTweenMenagerUser.addTween(dummyTween2)
      dummyTweenMenagerUser.tweensAdded(TweenId(2)) should equal (true)
      dummyTweenMenagerUser.addTween(dummyTween3)
      dummyTweenMenagerUser.tweensAdded(TweenId(3)) should equal (true)
      dummyTweenMenagerUser.disposeTweens()
      dummyTweenMenagerUser.tweensAdded(TweenId(1)) should equal (false)
      dummyTweenMenagerUser.tweensAdded(TweenId(2)) should equal (false)
      dummyTweenMenagerUser.tweensAdded(TweenId(3)) should equal (false)
    }

    it("should check for update Tweens via TweenManager") {
      val dummyTweenMenagerUser = new MyTweenMenagerUser
      val dummyTweenMenager = new DummyTweenManager
      dummyTweenMenagerUser.tweenManager = dummyTweenMenager
      dummyTweenMenagerUser.addTween(dummyTween)
      dummyTweenMenagerUser.addTween(dummyTween2)
      dummyTweenMenager.updated should equal (false)
      dummyTweenMenagerUser.tweenManager.update(0.2f)
      dummyTweenMenager.updated should equal (true)
    }
  }
}
