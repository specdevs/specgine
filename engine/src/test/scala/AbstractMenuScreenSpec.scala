package com.specdevs.specgine.test

import com.specdevs.specgine.states._

import org.scalatest.{FunSpec,Matchers}

class AbstractMenuScreenSpec extends FunSpec with Matchers {
  class DummyAbstractMenuScreen extends AbstractMenuScreen with DummyCallbacksImplementation {
    var processed = false
    var processedBackground = false
    var rendered = false
    var resized = false
    var renderedBackground = false
    def process(dt: Float): Unit = {
      processed = true
    }
    def render(alpha: Float): Unit = {
      rendered = true
    }
    def resize(x: Int, y: Int): Unit = {
      resized = true
    }
    def processBackground(dt: Float): Unit = {
      processedBackground = true
    }
    def renderBackground(alpha: Float): Unit = {
      renderedBackground = true
    }
  }

  class DummyMenuSwitcher extends MenuSwitcher {
    def changeMenu(name: String): Unit = {}
  }

  val dummyAbstractMenuScreen = new DummyAbstractMenuScreen
  val dummyMenuSwitcher = new DummyMenuSwitcher
  val dummyAssetManager = new DummyAssetManager

  describe("AbstractMenuScreen") {
    it("should check MenuSwitcher setting") {
      val thrownAsset = intercept[IllegalStateException] {
        dummyAbstractMenuScreen.changeMenu("")
      }
      assert(thrownAsset.getMessage === "You haven't set menuSwitcher.")
      dummyAbstractMenuScreen.menuSwitcher = dummyMenuSwitcher
      dummyAbstractMenuScreen.changeMenu("")
      ()
    }

    it("should check AssetManager setting") {
      val thrownAsset = intercept[IllegalStateException] {
        dummyAbstractMenuScreen.doDispose()
      }
      assert(thrownAsset.getMessage === "You haven't set assetManager.")
      dummyAbstractMenuScreen.assetManager = dummyAssetManager
      dummyAbstractMenuScreen.doDispose()
      dummyAbstractMenuScreen.disposed should equal (true)
    }

    it("should check basic methods") {
      dummyAbstractMenuScreen.doCreate()
      dummyAbstractMenuScreen.created should equal (true)
      dummyAbstractMenuScreen.doEnter()
      dummyAbstractMenuScreen.inside should equal (true)
      dummyAbstractMenuScreen.doInitialize()
      dummyAbstractMenuScreen.initialized should equal (true)
      dummyAbstractMenuScreen.doLeave()
      dummyAbstractMenuScreen.inside should equal (false)
    }
  }
}
