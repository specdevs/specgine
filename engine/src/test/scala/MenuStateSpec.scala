package com.specdevs.specgine.test

import com.specdevs.specgine.states._

import org.scalatest.{FunSpec,Matchers}

class MenuStateSpec extends FunSpec with Matchers {
  class DummyMenuState extends MenuState {
    this.assetManager = new DummyAssetManager
    this.stateManager = new DummyStateManager
    var created = false
    var initialized = false
    var disposed = false
    var processed = false
    var rendered = false
    var resized = false
    var inside = false
    def create(): Unit = {
      created = true
    }

    def initialize(): Unit = {
      initialized = true
    }

    def dispose(): Unit = {
      disposed = true
    }

    override def enter(): Unit = {
      super.enter()
      inside = true
    }

    override def leave(): Unit = {
      super.leave()
      inside = false
    }

    override def process(dt: Float): Unit = {
      super.process(dt)
      processed = true
    }

    override def render(alpha: Float): Unit = {
      super.render(alpha)
      rendered = true
    }

    override def resize(x: Int, y: Int): Unit = {
      super.resize(x,y)
      resized = true
    }
  }

  class DummyProperMenuState extends DummyMenuState {
    override def initialize(): Unit = {
      super.initialize()
      this.addMenuScreen(name = "four", screen = new DummyAbstractMenuScreen, true)
      this.addMenuScreen(name = "two", screen = new DummyAbstractMenuScreen)
    }
  }
  describe("MenuState") {
    it("should error when changing to non-existing screen") {
      val dummyMenuState = new DummyMenuState
      val thrownAsset = intercept[IllegalArgumentException] {
        dummyMenuState.changeMenu("non-existing")
      }
      assert(thrownAsset.getMessage === "MenuScreen non-existing doesn't exist.")
    }

    it("should error when adding twice the same screen") {
      class DummyAddTwiceMenuState extends DummyMenuState {
        override def initialize(): Unit = {
          super.initialize()
          this.addMenuScreen(name = "the same", screen = new DummyAbstractMenuScreen)
          this.addMenuScreen(name = "the same", screen = new DummyAbstractMenuScreen)
        }
      }
      val dummyMenuState = new DummyAddTwiceMenuState

      val thrownAsset = intercept[IllegalArgumentException] {
        dummyMenuState.doInitialize()
      }
      assert(thrownAsset.getMessage === "MenuScreen the same is already added.")
    }

    it("should initialize and change menu screen") {
      val dummyMenuState = new DummyProperMenuState
      dummyMenuState.initialized should equal (false)
      dummyMenuState.doInitialize()
      dummyMenuState.initialized should equal (true)
      dummyMenuState.changeMenu("two")
    }

    it("should initialize and process") {
      val dummyMenuState = new DummyProperMenuState
      dummyMenuState.initialized should equal (false)
      dummyMenuState.doInitialize()
      dummyMenuState.initialized should equal (true)
      dummyMenuState.processed should equal (false)
      dummyMenuState.process(0.1f)
      dummyMenuState.processed should equal (true)
    }

    it("should initialize and render") {
      val dummyMenuState = new DummyProperMenuState
      dummyMenuState.initialized should equal (false)
      dummyMenuState.doInitialize()
      dummyMenuState.initialized should equal (true)
      dummyMenuState.rendered should equal (false)
      dummyMenuState.render(0.1f)
      dummyMenuState.rendered should equal (true)
    }

    it("should initialize and create") {
      val dummyMenuState = new DummyProperMenuState
      dummyMenuState.initialized should equal (false)
      dummyMenuState.doInitialize()
      dummyMenuState.initialized should equal (true)
      dummyMenuState.created should equal (false)
      dummyMenuState.doCreate()
      dummyMenuState.created should equal (true)
    }

    it("should initialize and resize") {
      val dummyMenuState = new DummyProperMenuState
      dummyMenuState.initialized should equal (false)
      dummyMenuState.doInitialize()
      dummyMenuState.initialized should equal (true)
      dummyMenuState.resized should equal (false)
      dummyMenuState.resize(4, 2)
      dummyMenuState.resized should equal (true)
    }

    it("should initialize, enter and leave") {
      val dummyMenuState = new DummyProperMenuState
      dummyMenuState.initialized should equal (false)
      dummyMenuState.doInitialize()
      dummyMenuState.initialized should equal (true)
      dummyMenuState.inside should equal (false)
      dummyMenuState.enter()
      dummyMenuState.inside should equal (true)
      dummyMenuState.leave()
      dummyMenuState.inside should equal (false)
    }

    it("should initialize and dispose") {
      val dummyMenuState = new DummyProperMenuState
      dummyMenuState.initialized should equal (false)
      dummyMenuState.doInitialize()
      dummyMenuState.initialized should equal (true)
      dummyMenuState.disposed should equal (false)
      dummyMenuState.doDispose()
      dummyMenuState.disposed should equal (true)
    }

    it("should initialize and execute input methods") {
      val dummyMenuState = new DummyProperMenuState
      dummyMenuState.initialized should equal (false)
      dummyMenuState.doInitialize()
      dummyMenuState.initialized should equal (true)
      dummyMenuState.keyDown(42) should equal (false)
      dummyMenuState.keyUp(42) should equal (false)
      dummyMenuState.keyTyped('a') should equal (false)
      dummyMenuState.touchDown(1, 9, 8, 8) should equal (false)
      dummyMenuState.touchUp(1, 9, 8, 8) should equal (false)
      dummyMenuState.touchDragged(1, 9, 8) should equal (false)
      dummyMenuState.mouseMoved(4, 2) should equal (false)
      dummyMenuState.scrolled(42) should equal (false)
    }
  }
}
