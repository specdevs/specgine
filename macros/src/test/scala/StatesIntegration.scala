package com.specdevs.specgine.macros.test

import com.specdevs.specgine.assets.{AbstractAssetManager,Asset,AssetDescriptor}
import com.specdevs.specgine.core.{AbstractConfigManager,Config,StateManager}
import com.specdevs.specgine.macros.states.{No,OneOf2,The,Needed,Optional,All,withManager}
import com.specdevs.specgine.states.{Component,ComponentsSpec,Entity,SingleEntitySystem,VoidEntitySystem,World}
import com.specdevs.specgine.test._

import org.scalatest.{FunSpec,Matchers}

@No[TeamB]
@The[TeamA]
class MovementSpec extends ComponentsSpec {
  val pos = new Needed[Position]
  val vel = new Needed[Velocity]
  val acc = new Optional[Acceleration]
}

class MovementSystem extends SingleEntitySystem  with DummyCallbacksImplementation {
  val components = withManager[MovementSpec]
}

@OneOf2[TeamA,TeamB]
class PositionSpec extends ComponentsSpec {
  val pos = new Needed[Position]
  val all = new All
}

class BadTwoAllSpec extends ComponentsSpec {
  val all = new All
  val error = new All
}

class BadNoFields extends ComponentsSpec

@No[TeamA]
class BadExcludeNeeded extends ComponentsSpec {
  val field = new Needed[TeamA]
}

@The[TeamA]
class BadNotOptimal extends ComponentsSpec {
  val field = new Optional[TeamA]
}

@No[TeamB]
@No[TeamC]
class TeamSpec extends ComponentsSpec {
  val inTeamA = new Optional[TeamA]
}

class PositionSystem extends SingleEntitySystem with DummyCallbacksImplementation {
  val components = withManager[PositionSpec]
}

class TeamCheckSystem extends SingleEntitySystem with DummyCallbacksImplementation {
  val components = withManager[TeamSpec]
}

class DummySystem extends VoidEntitySystem with DummyCallbacksImplementation

class StatesIntegration extends FunSpec with Matchers {
  val world = new World
  val dummyStateManager = new StateManager
  val dummyConfigManager = new DummyConfigManager
  val dummyAssetManager = new DummyAssetManager
  val dummyTweenManager = new DummyTweenManager
  val ms = new MovementSystem
  val ds = new DummySystem
  val ps = new PositionSystem
  val ts = new TeamCheckSystem

  world.stateManager = dummyStateManager
  world.configManager = dummyConfigManager
  world.assetManager = dummyAssetManager
  world.tweenManager = dummyTweenManager
  world.addSystem(ms)
  world.addSystem(ds)
  world.addSystem(ps)
  world.addSystem(ts)

  val player = world.createEntity(Position(0, 1), Velocity(1, 2), TeamA())
  val player2 = world.createEntity(Position(1, 1), Velocity(2, 2), Acceleration(0, 1), TeamA())
  val player3 = world.createEntity(Position(1, 1), Velocity(2, 2), Acceleration(0, 1), TeamB(), TeamC())
  val player4 = world.createEntity(Position(1, 1), TeamA(), TeamB())
  val player5 = world.createEntity(Position(3, 1))

  describe("TeamCheck System") {
    it("should have players in only TeamA, or in no team") {
      ts systemHasEntity player should equal (true)
      ts systemHasEntity player2 should equal (true)
      ts systemHasEntity player3 should equal (false)
      ts systemHasEntity player4 should equal (false)
      ts systemHasEntity player5 should equal (true)
    }

    it("should have optional team set only for TeamA") {
      ts.components.inTeamA(player) should equal (Some(TeamA()))
      ts.components.inTeamA(player2) should equal (Some(TeamA()))
      ts.components.inTeamA(player5) should equal (None)
    }
  }

  describe("Movement System") {
    it("should have player with Position(0, 1) and Velocity(1, 2) and no Acceleration") {
      ms systemHasEntity player should equal (true)
      ms.components.pos(player) should equal (Position(0, 1))
      ms.components.vel(player) should equal (Velocity(1, 2))
      ms.components.acc(player).isDefined should equal (false)
    }

    it("should have player2 with Position(1, 1), Velocity(2, 2) and Acceleration(0, 1)") {
      ms systemHasEntity player2 should equal (true)
      ms.components.pos(player2) should equal (Position(1, 1))
      ms.components.vel(player2) should equal (Velocity(2, 2))
      ms.components.acc(player2).get should equal (Acceleration(0, 1))
    }

    it("should not have player3") {
      ms systemHasEntity player3 should equal (false)
    }

    it("shoud have Entity(1) equal to player") {
      ms systemHasEntity Entity(1) should equal (true)
      ms.components.pos(Entity(1)) should equal (ms.components.pos(player))
      ms.components.vel(Entity(1)) should equal (ms.components.vel(player))
      ms.components.acc(Entity(1)).isDefined should equal (ms.components.acc(player).isDefined)
    }

    it("shoud have Entity(2) equal to player2") {
      ms systemHasEntity Entity(2) should equal (true)
      ms.components.pos(Entity(2)) should equal (ms.components.pos(player2))
      ms.components.vel(Entity(2)) should equal (ms.components.vel(player2))
      ms.components.acc(Entity(2)) should equal (ms.components.acc(player2))
    }

    it("should not have Entity(0)") {
      ms systemHasEntity Entity(0) should equal (false)
    }

    it("should not have Entity(3)") {
      ms systemHasEntity Entity(3) should equal (false)
    }

    it("should not have Entity(4)") {
      ms systemHasEntity Entity(4) should equal (false)
    }

    it("should not have Entity(5)") {
      ms systemHasEntity Entity(5) should equal (false)
    }

    it("should want set of Position, Velocity and TeamA components") {
      ms systemWantsEntity Set(Position(0, 1), Velocity(1, 2), TeamA()) should equal (true)
    }

    it("should want set of Position, Velocity, Acceleration and TeamA components") {
      ms systemWantsEntity Set(Position(0, 1), Velocity(1, 2), TeamA(), Acceleration(0, 1)) should equal (true)
    }

    it("should not want TeamB component") {
      ms systemWantsEntity Set(Position(0, 1), Velocity(1, 2), TeamB()) should equal (false)
      ms systemWantsEntity Set(Position(0, 1), Velocity(1, 2), TeamB(), Acceleration(0, 1)) should equal (false)
    }

    it("should not want only Position component") {
      ms systemWantsEntity Set(Position(0, 1)) should equal (false)
    }

    it("should not want only Velocity component") {
      ms systemWantsEntity Set(Velocity(0, 1)) should equal (false)
    }

    it("should not want only TeamA component") {
      ms systemWantsEntity Set(TeamA()) should equal (false)
    }

    it("should not want only Position and Velocity components") {
      ms systemWantsEntity Set(Position(0, 1), Velocity(0, 1)) should equal (false)
    }

    it("should not want only Velocity and TeamA components") {
      ms systemWantsEntity Set(Velocity(0, 1), TeamA()) should equal (false)
    }

    it("should not want only Position and TeamA components") {
      ms systemWantsEntity Set(Position(0, 1), TeamA()) should equal (false)
    }

    it("should remove Entity") {
      ms removeEntityFromSystem Entity(1)
      ms removeEntityFromSystem Entity(2)
      ms systemHasEntity Entity(1) should equal (false)
      ms systemHasEntity Entity(2) should equal (false)
    }

    it("should remove Entity if TeamB component is added") {
      val tempPlayer = world.createEntity(Position(0, 1), Velocity(1, 2), TeamA())
      ms systemHasEntity tempPlayer should equal (true)
      world.addComponents(tempPlayer, TeamB())
      ms systemHasEntity tempPlayer should equal (false)
    }
  }

  describe("Dummy System") {
    it("should not want player nor player2 nor player3") {
      ds systemWantsEntity Set(Position(0, 1), Velocity(1, 2), TeamA()) should equal (false)
      ds systemWantsEntity Set(Position(1, 1), Velocity(2, 2), Acceleration(0, 1), TeamA()) should equal (false)
      ds systemWantsEntity Set(Position(1, 1), Velocity(2, 2), Acceleration(0, 1), TeamB()) should equal (false)
    }

    it("should not have any Entity") {
      ds systemHasEntity Entity(1) should equal (false)
      ds systemHasEntity Entity(2) should equal (false)
      ds systemHasEntity Entity(3) should equal (false)
    }
  }

  describe("Position System") {
    it("should have player with Position(0, 1), Velocity(1, 2) and TeamA() components") {
      ps systemHasEntity player should equal (true)
      ps.components.all(player) should equal (Set(Position(0, 1), Velocity(1, 2), TeamA()))
    }

    it("should have player2 with Position(1, 1), Velocity(2, 2), Acceleration(0, 1) and TeamA() components") {
      ps systemHasEntity player2 should equal (true)
      ps.components.all(player2) should equal (Set(Position(1, 1), Velocity(2, 2), Acceleration(0, 1), TeamA()))
    }

    it("should have player3 with Position(1, 1), Velocity(2, 2), Acceleration(0, 1), TeamB() and TeamC() components") {
      ps systemHasEntity player3 should equal (true)
      ps.components.all(player3) should equal (
        Set(Position(1, 1), Velocity(2, 2), Acceleration(0, 1), TeamB(), TeamC())
      )
    }

    it("should have player4 with Position(1, 1), TeamA() and TeamB() components") {
      ps systemHasEntity player4 should equal (true)
      ps.components.all(player4) should equal (Set(Position(1, 1), TeamA(), TeamB()))
    }

    it("should not have player5") {
      ps systemHasEntity player5 should equal (false)
      ps.components.all.isDefinedAt(player5) should equal (false)
    }
  }

  describe("Bad Systems") {
    it("should fail for two All descriptors") {
      assertDoesNotCompile("withManager[BadTwoAllSpec]")
    }

    it("should fail for descriptors with no fields") {
      assertDoesNotCompile("withManager[BadNoFields]")
    }

    it("should fail when needed fields is excluded") {
      assertDoesNotCompile("withManager[BadExcludeNeeded]")
    }

    it("should fail with warning when is not optimal") {
      assertDoesNotCompile("withManager[BadNotOptimal]")
    }
  }
}
