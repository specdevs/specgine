package com.specdevs.specgine.macros.test

import com.specdevs.specgine.macros.core.access

import com.specdevs.specgine.test._

import org.scalatest.{FunSpec,Matchers}

class CoreIntegration extends FunSpec with Matchers {
  var number = 42
  val pos = Position(1, 1)

  describe("Access macro") {
    it("should compile") {
      assertCompiles("access(number)")
      assertCompiles("access(pos.x)")
    }

    it("should fail for local variable") {
      assertDoesNotCompile("""
        def failed() = {
          var x = 42;
          access(x)
        }
      """)
    }

    it("should fail for constants") {
      assertDoesNotCompile("access(42)")
    }

    it("should create accessor for class variable") {
      val accessor = access(number)
      accessor.value should equal (42)
      accessor.value = 6
      accessor.value should equal (number)
    }

    it("should create accessor for case class constructor") {
      val accessor = access(pos.x)
      accessor.value should equal (1)
      accessor.value = 6
      accessor.value should equal (pos.x)
    }
  }
}
