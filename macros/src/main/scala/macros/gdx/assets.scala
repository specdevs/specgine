package com.specdevs.specgine.macros.gdx

import com.specdevs.specgine.core.Provider
import com.specdevs.specgine.assets.Asset
import com.specdevs.specgine.assets.gdx.JsonAsset

import scala.language.experimental.macros
import scala.reflect.macros.blackbox.Context

import spray.json.JsonFormat

// $COVERAGE-OFF$Disable coverage reports of macro invocation.

/** Macro package for [[com.specdevs.specgine.assets.gdx]].
 *
 * This package provides macro `handleJson` to automate creation of
 * [[com.specdevs.specgine.core.Provider `Provider`]] trait from
 * [[com.specdevs.specgine.assets.Asset `Asset`]] to generic type `T`.
 * It implements `pack` and `unpack` methods.
 *
 * For basic types one should import `JsonFormat[T]` from `DefaultJsonProtocol` or define custom one,
 * declaring it as implicit, so macro definition can pick it up.
 * Please refer to [[https://github.com/spray/spray-json ''Spray-JSON Home Page'']] for guidance with
 * `JsonFormst[T]` implementation.
 *
 * Below we present a simple example of how to use `handleJson` macro for a case class type:
 * {{{
 *   import spray.json.JsonParser
 *   import DefaultJsonProtocol._
 *   case class Position(val x: Float, val y: Float)
 *   object PositionProtocole extends DefaultJsonProtocol {
 *     implicit val positionFormat = jsonFormat2(Position)
 *   }
 *   val handler = handleJson[Position]
 *   val unpackedJson = handler.unpack(Some(JsonAsset(JsonParser("""{"x":3,"y":14}""")))).get
 *   unpackedJson == Position(3, 14)
 * }}}
 */
object assets {
  import AssetsImpl._

  /** Macro automatically providing instance of [[com.specdevs.specgine.core.Provider `Provider`]] trait from
   * [[com.specdevs.specgine.assets.Asset `Asset`]] to generic type `T`.
   *
   * Implements `pack` and `unpack` methods of [[com.specdevs.specgine.core.Provider `Provider`]] trait.
   *
   * @param reader Implicit instance of `JsonFormat[T]` for type `T`.
   * @tparam T generic type.
   * @return instance of `Provider[Asset,T]`.
   */
  def handleJson[T](implicit reader: JsonFormat[T]): Provider[Asset, T] = macro handleJsonImpl[T]
}

// $COVERAGE-ON$

private object AssetsImpl {
  class Helper[C <: Context](val c: C) {
    import c.universe._

    private val com = Ident(TermName("com"))

    private val specdevs = Select(com, TermName("specdevs"))

    private val specgine = Select(specdevs, TermName("specgine"))

    private val core = Select(specgine, TermName("core"))

    private val assets = Select(specgine, TermName("assets"))

    private val assetsGdx = Select(assets, TermName("gdx"))

    private val provider = Select(core, TypeName("Provider"))

    private val asset = Select(assets, TypeName("Asset"))

    private val jsonAsset = Select(assetsGdx, TypeName("JsonAsset"))

    private val className = TypeName("$anon")

    private val classInit: Tree = Apply(Select(New(Ident(className)), termNames.CONSTRUCTOR), Nil)

    private def typeTree(theType: Type): Tree = {
      theType.dealias.typeArgs match {
        case Nil => Ident(theType.typeSymbol.name)
        case some: Any => AppliedTypeTree(Ident(theType.typeSymbol.name), some map typeTree)
      }
    }

    private def parents(theType: Type): List[Tree] = {
      List(AppliedTypeTree(provider, List(asset, typeTree(theType))))
    }

    private val constructor: Tree = {
      val superConstructor = Select(Super(This(typeNames.EMPTY), typeNames.EMPTY), termNames.CONSTRUCTOR)
      val constructorBody = Block(List(Apply(superConstructor, Nil)), Literal(Constant(())))
      DefDef(NoMods, termNames.CONSTRUCTOR, Nil, List(Nil), TypeTree(), constructorBody)
    }

    private def pack(theType: Type, format: Tree): Tree = {
      val packParams = List(List(
        ValDef(Modifiers(Flag.PARAM), TermName("value"), typeTree(theType), EmptyTree)
      ))
      val value = Apply(Select(format, TermName("write")), List(Ident(TermName("value"))))
      val packBody = Apply(Select(New(jsonAsset), termNames.CONSTRUCTOR), List(value))
      DefDef(NoMods, TermName("pack"), Nil, packParams, asset, packBody)
    }

    private def unpack(theType: Type, format: Tree): Tree = {
      val unpackParams = List(List(ValDef(
        Modifiers(Flag.PARAM),
        TermName("asset"),
        AppliedTypeTree(Ident(TypeName("Option")), List(asset)),
        EmptyTree
      )))
      val unpackPattern = Apply(Ident(TermName("Some")), List(
        Bind(TermName("json"), Typed(Ident(termNames.WILDCARD), jsonAsset))
      ))
      val unpackRead = Apply(Select(format, TermName("read")), List(Select(Ident(TermName("json")),TermName("self"))))
      val unpackTry = Select(Select(Ident(TermName("util")), TermName("Try")), TermName("apply"))
      val unpackBody = Match(Ident(TermName("asset")), List(
        CaseDef(unpackPattern, EmptyTree, Select(Apply(unpackTry, List(unpackRead)), TermName("toOption"))),
        CaseDef(Ident(termNames.WILDCARD), EmptyTree, Ident(TermName("None")))
      ))
      DefDef(
        NoMods,
        TermName("unpack"),
        Nil,
        unpackParams,
        AppliedTypeTree(Ident(TypeName("Option")), List(typeTree(theType))),
        unpackBody
      )
    }

    private def methods(theType: Type, format: Tree): List[Tree] = {
      List(constructor, pack(theType, format), unpack(theType, format))
    }

    private def classTemplate(theType: Type, format: Tree): Template = {
      Template(parents(theType), noSelfType, methods(theType, format))
    }

    private def classDef(theType: Type, format: Tree): Tree = {
      ClassDef(Modifiers(Flag.FINAL), className, Nil, classTemplate(theType, format))
    }

    def generateHandler(theType: Type, format: Tree): Tree = {
      Block(List(classDef(theType, format)), classInit)
    }
  }

  def handleJsonImpl[T: c.WeakTypeTag](c: Context)(reader: c.Expr[JsonFormat[T]]): c.Expr[Provider[Asset,T]] = {
    import c.universe._
    val helper = new Helper[c.type](c)
    val theType: Type = weakTypeOf[T]
    val format: Tree = reader.tree
    c.Expr[Provider[Asset,T]](helper.generateHandler(theType, format))
  }
}
